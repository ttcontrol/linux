/*
 * Generic PWM backlight driver data - see drivers/video/backlight/pwm_bl.c
 */
#ifndef __LINUX_PWM_BACKLIGHT_H
#define __LINUX_PWM_BACKLIGHT_H

struct platform_pwm_backlight_data {
	int pwm_id;
	unsigned int max_brightness;
	unsigned int dft_brightness;
	unsigned int pwm_period_ns;
	unsigned int fade_steps_per_sec;
	int (*init)(struct device *dev);
	int (*notify)(struct device *dev, int brightness);
	void (*exit)(struct device *dev);
};

struct pwm_bl_data {
	struct pwm_device	*pwm;
	unsigned int		period;
	unsigned int 		fade_increment;
	int					fade_value;
	unsigned int		fade_target;
	int					fade_buf;
	unsigned char 		on_off;
	int			(*notify)(int brightness);
};

#endif
