/*
 * MAX9526 platform data
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef __LINUX_I2C_MAX9526_H__
#define __LINUX_I2C_MAX9526_H__

struct max9526_platform_data {
	unsigned int csi;
};

#endif
