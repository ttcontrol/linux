
#ifndef _SX86XX_REG_STRUCT_H_
#define _SX86XX_REG_STRUCT_H_


struct sx86xx_reg
{
  u8 reg;
  u8 data;
} typedef sx86xx_reg_t, *psx86xx_reg_t;

#endif // _SX86XX_REG_STRUCT_H_

