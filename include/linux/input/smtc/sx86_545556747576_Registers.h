
#define SMTC_CMD_PENTRG		  0xe0
#define SMTC_CMD_MANUAL      0xc0
#define SMTC_CMD_SELECT      0xA0
#define SMTC_CMD_CONVERT     0xB0

#define SMTC_CMD_READ        0x40


#define SMTC_REGRESET_VALUE	0xde



/*********************************************************/
/***** Touchscreen Portion *****/
#define SMTC_REGTOUCH0  0x00
#define SMTC_REGTOUCH1  0x01
#define SMTC_REGTOUCH2  0x02
#define SMTC_REGTOUCH3  0x03
#define SMTC_REGCHANMSK 0x04
/***** Haptic and Temp Portion *****/
#define SMTC_REGHAPT0   0x05
#define SMTC_REGHAPT1   0x06
#define SMTC_REGHAPT2   0x07
#define SMTC_REGHAPT3   0x08
#define SMTC_REGHAPT4   0x09
#define SMTC_REGHAPT5   0x0A
/***** Proximity Portion *****/
#define SMTC_REGPROX0   0x0B
#define SMTC_REGPROX1   0x0C
#define SMTC_REGPROX2   0x0D
#define SMTC_REGPROX3   0x0E
#define SMTC_REGPROX4   0x0F
#define SMTC_REGPROX5   0x10
#define SMTC_REGPROX6   0x11
#define SMTC_REGPROX7   0x12
#define SMTC_REGPROX8   0x13
#define SMTC_REGPROX9   0x14
#define SMTC_REGPROX10  0x15
#define SMTC_REGPROX11  0x16
#define SMTC_REGPROX12  0x17
#define SMTC_REGPROX13  0x18
#define SMTC_REGPROX14  0x19
#define SMTC_REGPROX15  0x1A
#define SMTC_REGPROX16  0x1B
#define SMTC_REGPROX17  0x1C
#define SMTC_REGPROX18  0x1D
#define SMTC_REGPROX19  0x1E
#define SMTC_REGPROX20  0x1F
#define SMTC_REGPROX21  0x20
#define SMTC_REGPROX22  0x21
/***** Status Portion *****/
#define SMTC_REGIRQMSK  0x22
#define SMTC_REGIRQSRC  0x23
#define SMTC_REGSTAT    0x24
/***** Aux Portion *****/
#define SMTC_REGAUX0    0x25
#define SMTC_REGAUX1    0x25
/***** Software Reset *****/
#define SMTC_REGRESET   0x3F
/*********************************************************/
/*                  RegTouch0                 */
#define SMTC_TOUCHRATE_OFF    0x00
#define SMTC_TOUCHRATE_10CPS  0x10
#define SMTC_TOUCHRATE_20CPS  0x20
#define SMTC_TOUCHRATE_40CPS  0x30
#define SMTC_TOUCHRATE_60CPS  0x40
#define SMTC_TOUCHRATE_80CPS  0x50
#define SMTC_TOUCHRATE_100CPS 0x60
#define SMTC_TOUCHRATE_200CPS 0x70
#define SMTC_TOUCHRATE_300CPS 0x80
#define SMTC_TOUCHRATE_400CPS 0x90
#define SMTC_TOUCHRATE_500CPS 0xA0
#define SMTC_TOUCHRATE_1KCPS  0xB0
#define SMTC_TOUCHRATE_2KCPS  0xC0
#define SMTC_TOUCHRATE_3KCPS  0xD0
#define SMTC_TOUCHRATE_4KCPS  0xE0
#define SMTC_TOUCHRATE_5KCPS  0xF0

#define SMTC_POWDLY_0_5US     0b00000000
#define SMTC_POWDLY_1_1US     0b00000001
#define SMTC_POWDLY_2_2US     0b00000010
#define SMTC_POWDLY_4_4US     0b00000011
#define SMTC_POWDLY_8_9US     0b00000100
#define SMTC_POWDLY_17_8US    0b00000101
#define SMTC_POWDLY_35_5US    0b00000110
#define SMTC_POWDLY_71_0US    0b00000111
#define SMTC_POWDLY_142US     0b00001000
#define SMTC_POWDLY_284US     0b00001001
#define SMTC_POWDLY_568US     0b00001010
#define SMTC_POWDLY_1_14MS    0b00001011
#define SMTC_POWDLY_2_27MS    0b00001100
#define SMTC_POWDLY_4_55MS    0b00001101
#define SMTC_POWDLY_9_09MS    0b00001110
#define SMTC_POWDLY_18_19MS   0b00001111
/*********************************************************/
/*                  RegTouch1                */

#define SMTC_REGTOUCH1_DEFAULT          0b00100000

#define SMTC_TSTYPE_4WIRE               0b00000000
#define SMTC_TSTYPE_5WIRE               0b00010000

#define SMTC_RPNDT_114KOHM              0b00000000
#define SMTC_RPNDT_228KOHM              0b00000100
#define SMTC_RPNDT_57KOHM               0b00001000
#define SMTC_RPNDT_28KOHM               0b00001100

#define SMTC_FILT_OFF                  0b00000000
#define SMTC_FILT_3SAMPLE              0b00000001
#define SMTC_FILT_5SAMPLE              0b00000010
#define SMTC_FILT_7SAMPLE              0b00000011
/*********************************************************/
/*                  RegTouch2               */
#define SMTC_SETDLY_0_5US               0b00000000
#define SMTC_SETDLY_1_1US               0b00000001
#define SMTC_SETDLY_2_2US               0b00000010
#define SMTC_SETDLY_4_4US               0b00000011
#define SMTC_SETDLY_8_9US               0b00000100
#define SMTC_SETDLY_17_8US              0b00000101
#define SMTC_SETDLY_35_5US              0b00000110
#define SMTC_SETDLY_71_0US              0b00000111
#define SMTC_SETDLY_142US               0b00001000
#define SMTC_SETDLY_284US               0b00001001
#define SMTC_SETDLY_568US               0b00001010
#define SMTC_SETDLY_1_14MS              0b00001011
#define SMTC_SETDLY_2_27MS              0b00001100
#define SMTC_SETDLY_4_55MS              0b00001101
#define SMTC_SETDLY_9_09MS              0b00001110
#define SMTC_SETDLY_18_19MS             0b00001111
/*********************************************************/
/*                  RegChanMsk              */

#define SMTC_CONV_X	  	0x80
#define SMTC_CONV_Y	  	0x40
#define SMTC_CONV_Z1		0x20
#define SMTC_CONV_Z2		0x10
#define SMTC_CONV_AUX   0x08
#define SMTC_CONV_RX		0x04
#define SMTC_CONV_RY		0x02

/****************************************************************************/
/*                  RegHapt0                */
#define SMTC_HAPTMODE_PWM               0b00000000
#define SMTC_HAPTMODE_I2C               0b10000000
#define SMTC_HAPTTYPEEN_OFF             0b00000000
#define SMTC_HAPTTYPEEN_LRA             0b00100000
#define SMTC_HAPTTYPEEN_ERM             0b01000000
#define SMTC_HAPTTYPEEN_RESERVED        0b01100000
#define SMTC_HAPTRANGE_128              0b00000000
#define SMTC_HAPTRANGE_256              0b00010000
#define SMTC_HAPTGAIN_1                 0b00000000
#define SMTC_HAPTGAIN_1_17              0b00000001
#define SMTC_HAPTGAIN_1_35              0b00000010
#define SMTC_HAPTGAIN_1_52              0b00000011
#define SMTC_HAPTGAIN_1_69              0b00000100
#define SMTC_HAPTGAIN_1_87              0b00000101
#define SMTC_HAPTGAIN_2_04              0b00000110
#define SMTC_HAPTGAIN_2_21              0b00000111
#define SMTC_HAPTGAIN_2_39              0b00001000
#define SMTC_HAPTGAIN_2_56              0b00001001
#define SMTC_HAPTGAIN_2_73              0b00001010
#define SMTC_HAPTGAIN_2_91              0b00001011
#define SMTC_HAPTGAIN_3_08              0b00001100
#define SMTC_HAPTGAIN_3_25              0b00001101
#define SMTC_HAPTGAIN_3_43              0b00001110
#define SMTC_HAPTGAIN_3_6               0b00001111
/*************************                */
#define SMTC_HAPTSQUELCH                0b00111000

/*-------------------------------------------*/
/****************************************************************************/
/*                  RegHapt2                */
#define SMTC_HAPTAMP_NO_SIGN            0b01111111
#define SMTC_HAPTPOLARITY_NEG           0b00000000
#define SMTC_HAPTPOLARITY_POS           0b10000000
/*-------------------------------------------*/
/****************************************************************************/
/*                  RegHapt3                */
#define SMTC_HAPTBW_OFF                     0b00000000
#define SMTC_HAPTBW_265HZ                   0b00100000
#define SMTC_HAPTBW_530HZ                   0b01000000
#define SMTC_HAPTBW_795HZ                   0b01100000
#define SMTC_HAPTHZ_NORMAL                  0b00000000
#define SMTC_HAPTHZ_HZ                      0b00010000
#define SMTC_HAPTSHORTPROT_ON               0b00000000
#define SMTC_HAPTSHORTPROT_OFF              0b00001000
#define SMTC_HAPTTIMERMSB                   0b00000111
/*-------------------------------------------*/
/****************************************************************************/
/*                  RegHapt4                */
#define SMTC_HAPTTIMERLSB                   0b11111111
/*-------------------------------------------*/
/****************************************************************************/


/****************************************************************************/
/*  RegIrqMsk   /   RegIrqSrc -Values        */
#define SMTC_SHORTIRQ                        0b10000000
#define SMTC_PROXCLOSEIRQ                    0b01000000
#define SMTC_PROXFARIRQ_PROXCONVDONEIRQ      0b00100000
#define SMTC_PROXCOMPDONE                    0b00010000
#define SMTC_PENTOUCHIRQ_TOUCHCONVDONEIRQ    0b00001000
#define SMTC_PENRELEASEIRQ                   0b00000100
#define SMTC_TEMPWARNINGIRQ                  0b00000010
#define SMTC_TEMPALARMIRQ                    0b00000001
/****************************************************************************/
/*                  RegStat                 */
#define SMTC_HAPTSHORTSTAT                   0b10000000
#define SMTC_RESETSTAT                       0b01000000
#define SMTC_PROXSTAT                        0b00100000
#define SMTC_PROXCOMPSTAT                    0b00010000
#define SMTC_CONVSTAT                        0b00001000
#define SMTC_PENSTAT                         0b00000100
#define SMTC_TEMPWARNINGSTAT                 0b00000010
#define SMTC_TEMPALARMSTAT                   0b00000001
/****************************************************************************/

/****************************************************************************/
/*                  RegProx0                */
#define SMTC_PRXRAWFILTSEL_FAST            0b00000000
#define SMTC_PRXRAWFILTSEL_SLOW            0b10000000

#define SMTC_PRXIRQSEL                     0b01000000

#define SMTC_PRXHYST_0                     0b00000000
#define SMTC_PRXHYST_32                    0b00010000
#define SMTC_PRXHYST_128                   0b00100000
#define SMTC_PRXHYST_512                   0b00110000

#define SMTC_PRXSCANPERIOD_OFF             0b00000000
#define SMTC_PRXSCANPERIOD_EVERY_TS        0b00000001
#define SMTC_PRXSCANPERIOD_2_TS            0b00000010
#define SMTC_PRXSCANPERIOD_4_TS            0b00000011
#define SMTC_PRXSCANPERIOD_8_TS            0b00000100
#define SMTC_PRXSCANPERIOD_16_TS           0b00000101
#define SMTC_PRXSCANPERIOD_32_TS           0b00000110
#define SMTC_PRXSCANPERIOD_64_TS           0b00000111
#define SMTC_PRXSCANPERIOD_128_TS          0b00001000
#define SMTC_PRXSCANPERIOD_256_TS          0b00001001
#define SMTC_PRXSCANPERIOD_512_TS          0b00001010
#define SMTC_PRXSCANPERIOD_1024_TS         0b00001011
#define SMTC_PRXSCANPERIOD_2048_TS         0b00001100
#define SMTC_PRXSCANPERIOD_4096_TS         0b00001101
#define SMTC_PRXSCANPERIOD_8192_TS         0b00001110
#define SMTC_PRXSCANPERIOD_16384_TS        0b00001111

/****************************************************************************/
/*                  RegProx1                */
#define SMTC_PRXTRSHLD                     0b11111111
/****************************************************************************/
/*                  RegProx2                */
#define SMTC_PROXCLOSEDEB_OFF              0b00000000
#define SMTC_PROXCLOSEDEB_2                0b01000000
#define SMTC_PROXCLOSEDEB_4                0b10000000
#define SMTC_PROXCLOSEDEB_8                0b11000000
#define SMTC_PROXFARDEB_OFF                0b00000000
#define SMTC_PROXFARDEB_2                  0b00010000
#define SMTC_PROXFARDEB_4                  0b00100000
#define SMTC_PROXFARDEB_8                  0b00110000
#define SMTC_PROXCOMPPRD                   0b00001111
#define SMTC_PROXCOMPPRD_OFF               0b00000000
/*----- Else -> ProxCompPeriod x 128 samples -----*/
/****************************************************************************/
/*                  RegProx3                */
#define SMTC_PRXSTCK_OFF                   0b00000000
/*---- Else -> ProxStuckAtTimeout x 16 samples ---*/
/****************************************************************************/
/*                  RegProx4                */
#define SMTC_PRXPOSTRSHLD                  0b11111111
/*--- Threshold = 8 x register value (for comp) --*/
/****************************************************************************/
/*                  RegProx5                */
#define SMTC_PRXPOSDEB_OFF                 0b00000000
#define SMTC_PRXPOSDEB_2                   0b00100000
#define SMTC_PRXPOSDEB_4                   0b01000000
#define SMTC_PRXPOSDEB_8                   0b01100000
#define SMTC_PRXPOSDEB_16                  0b10000000
#define SMTC_PRXPOSDEB_32                  0b10100000
#define SMTC_PRXPOSDEB_64                  0b11000000
#define SMTC_PRXPOSDEB_128                 0b11100000

#define SMTC_PRXPOSFILT_OFF                0b00000000
#define SMTC_PRXPOSFILT_1_1_2              0b00000100
#define SMTC_PRXPOSFILT_1_1_4              0b00001000
#define SMTC_PRXPOSFILT_1_1_8              0b00001100
#define SMTC_PRXPOSFILT_1_1_16             0b00010000
#define SMTC_PRXPOSFILT_1_1_32             0b00010100
#define SMTC_PRXPOSFILT_1_1_64             0b00011000
#define SMTC_PRXPOSFILT_1_1_128            0b00011100

/* Depending on PRXRAWFILTSEL 0__1 */
#define SMTC_PRXRAWFILT_OFF__1_1_16        0b00000000
#define SMTC_PRXRAWFILT_1_1_2__1_1_32      0b00000001
#define SMTC_PRXRAWFILT_1_1_4__1_1_64      0b00000010
#define SMTC_PRXRAWFILT_1_1_8__1_1_128     0b00000011
/****************************************************************************/
/*                  RegProx6                */
#define SMTC_PRXNEGTRSHLD                  0b11111111
/*--- Threshold = 8 x register value (for comp) --*/
/****************************************************************************/
/*                  RegProx7                */
#define SMTC_PRXNEGDEB_OFF                 0b00000000
#define SMTC_PRXNEGDEB_2                   0b00100000
#define SMTC_PRXNEGDEB_4                   0b01000000
#define SMTC_PRXNEGDEB_8                   0b01100000
#define SMTC_PRXNEGDEB_16                  0b10000000
#define SMTC_PRXNEGDEB_32                  0b10100000
#define SMTC_PRXNEGDEB_64                  0b11000000
#define SMTC_PRXNEGDEB_128                 0b11100000

#define SMTC_PRXNEGFILT_OFF                0b00000000
#define SMTC_PRXNEGFILT_1_1_2              0b00000100
#define SMTC_PRXNEGFILT_1_1_4              0b00001000
#define SMTC_PRXNEGFILT_1_1_8              0b00001100
#define SMTC_PRXNEGFILT_1_1_16             0b00010000
#define SMTC_PRXNEGFILT_1_1_32             0b00010100
#define SMTC_PRXNEGFILT_1_1_64             0b00011000
#define SMTC_PRXNEGFILT_1_1_128            0b00011100

#define SMTC_PROXHIGHIM_OFF                 0b00000000
#define SMTC_PROXHIGHIM_ON                  0b00000010 /* uses more power */
/****************************************************************************/
/*                  RegProx8                */
#define SMTC_PRXRAWMSB                  0b00001111
/*-------- Signed, 2's complement format ------*/
/****************************************************************************/
/*                  RegProx9                */
#define SMTC_PRXRAWLSB                  0b11111111
/*-------- Signed, 2's complement format ------*/
/****************************************************************************/
/*                  RegProx10               */
#define SMTC_PRXUSEFULMSB              0b00001111
/*-------- Signed, 2's complement format ------*/
/****************************************************************************/
/*                  RegProx11               */
#define SMTC_PRXUSEFULLSB              0b11111111
/*-------- Signed, 2's complement format ------*/
/****************************************************************************/
/*                  RegProx12               */
#define SMTC_PRXAVGMSB                  0b00001111
/*-------- Signed, 2's complement format ------*/
/****************************************************************************/
/*                  RegProx13               */
#define SMTC_PRXAVGLSB                  0b11111111
/*-------- Signed, 2's complement format ------*/
/****************************************************************************/
/*                  RegProx14               */
#define SMTC_PRXDIFFMSB                 0b00011111
/*-------- Signed, 2's complement format ------*/
/****************************************************************************/
/*                  RegProx15               */
#define SMTC_PRXDIFFLSB                 0b11111111
/*-------- Signed, 2's complement format ------*/
/****************************************************************************/
/*                  RegProx16               */
#define SMTC_OFFSETCOMPMSB              0b11111111
/****************************************************************************/
/*                  RegProx17               */
#define SMTC_OFFSETCOMPLSB              0b11111111
/****************************************************************************/
/*                  RegProx18               */
#define SMTC_PRXSENSITIVITY_0                  0b00000000
#define SMTC_PRXSENSITIVITY_1                  0b00010000
#define SMTC_PRXSENSITIVITY_2                  0b00100000
#define SMTC_PRXSENSITIVITY_3                  0b00110000
#define SMTC_PRXSENSITIVITY_4                  0b01000000
#define SMTC_PRXSENSITIVITY_5                  0b01010000
#define SMTC_PRXSENSITIVITY_6                  0b01100000
#define SMTC_PRXSENSITIVITY_7                  0b01110000


#define SMTC_PRXFREQ_64KHZ              0b00000100
#define SMTC_PRXFREQ_90KHZ              0b00000110
#define SMTC_PRXFREQ_112KHZ             0b00001000
#define SMTC_PRXFREQ_150KHZ             0b00001010 /*recommended*/
/****************************************************************************/
/*                  RegProx19                     */
/* Top Plate */
#define SMTC_PRXSENSORCON_NONE          0b00000000
#define SMTC_PRXSENSORCON_AUX1          0b00010000
#define SMTC_PRXSENSORCON_AUX2          0b00100000
#define SMTC_PRXSENSORCON_X_STANDARD    0b10000000
#define SMTC_PRXSENSORCON_Y_INVERTED    0b10010000
/* Bottom Plate */
#define SMTC_PRXSHIELDCON_NONE          0b00000000
#define SMTC_PRXSHIELDCON_AUX3          0b00000011
#define SMTC_PRXSHIELDCON_X_INVERTED    0b00001000
#define SMTC_PRXSHIELDCON_Y_STANDARD    0b00001001
/****************************************************************************/
/*                  RegProx20                     */

#define SMTC_PRXREG20_DEFAULT           0b00000100

#define SMTC_PRXBOOST_ON                  0b00100000
#define SMTC_PRXBOOST_OFF                 0b00110000
/****************************************************************************/
/*                  RegProx21                     */
#define SMTC_PRXREG21_DEFAULT           0b10000001
/****************************************************************************/



