
#ifndef _SMTC_RESSINGLETOUCH_PLATFORM_DATA_H_
#define _SMTC_RESSINGLETOUCH_PLATFORM_DATA_H_

#include <linux/input/smtc/sx86xx_reg_struct.h>

#define _INITIAL_WIDTH_THRESHOLD 4095 /*	770 / *3000 // rx*/
#define _INITIAL_HEIGHT_THRESHOLD	4095 /*1320 / *3000 // ry*/
#define WIDTH_DIFF_LIMIT	30
#define HEIGHT_DIFF_LIMIT	30
#define HEIGHT_NOISE 25 /*66*/
#define  WIDTH_NOISE 15 /*88*/
#define X_MULTIPLIER 8 
#define Y_MULTIPLIER 9
#define TOUCH_SIZE_SUM_SHIFT  2	/* 2=4 samples, 3=8 samples, 4=16 etc */
#define TOUCH_SIZE_SUM_COUNT		(1 << TOUCH_SIZE_SUM_SHIFT)


struct smtc_ResSingleTouch_axis_platform_data {
  u8 window; /* allocance of variance in numbres for st vs dt */
  u16 threshold; /* optional: set initial threshold, if not set, uses 4095 */
  u16 max_diff; /* ignore large changes (when varying pressure) */
  u16 max;      /* max value on axis, determined by screen */
  u16 min;      /* min value on axis, determined by screen */
  u16 plate_ohms; /* total resistiance on axis, for pressure calc */
} typedef smtc_ResSingleTouch_axis_platform_data_t, *psmtc_ResSingleTouch_axis_platform_data_t;

struct smtc_ResSingleTouch_platform_data {
  struct smtc_ResSingleTouch_axis_platform_data x_axis;
  struct smtc_ResSingleTouch_axis_platform_data y_axis;
  
  u32 rt_max;
} typedef smtc_ResSingleTouch_platform_data_t, *psmtc_ResSingleTouch_platform_data_t;

#endif // _SMTC_RESSINGLETOUCH_PLATFORM_DATA_H_

