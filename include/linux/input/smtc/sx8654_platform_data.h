
#ifndef _SX8654_PLATFORM_DATA_H_
#define _SX8654_PLATFORM_DATA_H_

#include <linux/input/smtc/sx86xx_reg_struct.h>

#include "smtc_ResSingleTouch_platform_data.h"

struct sx8654_platform_data {

  int     (*get_pendown_state)(void);
  
  int     (*init_platform_hw)(void);
  void    (*exit_platform_hw)(void);

  psx86xx_reg_t init_array;
  int size_init_array;
  int numTouchChannelsEnabled;

  smtc_ResSingleTouch_platform_data_t  touchPlatData;
};

#endif // _SX8654_PLATFORM_DATA_H_

