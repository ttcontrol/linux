

#ifndef _SMTC_RESPROXIMITYPROCESS_H_
#define _SMTC_RESPROXIMITYPROCESS_H_



#include <linux/slab.h>
#include <linux/input.h>

#define SMTC_PROX_MAX_DISTANCE 1

struct smtc_resProxData
{
  u8 inProx;
  u16 data;
};

struct smtc_ResProximity
{
  struct input_dev *input;
  /* 
   * Proximity returns more data BUT they are used
   * more for debugging purposes..
   */
  struct smtc_resProxData currentData;
} typedef smtc_ResProximity_t, *psmtc_ResProximity_t;


/* Currently we cannot determine position with proximity, so we want
 * to set this up as a separate input so we don't get confused
 * with X/Y
 */
int smtc_ResProximity_initInputEx(psmtc_ResProximity_t this,
                struct device *parent,int bustype, u8 registerDevice);
int smtc_ResProximity_initInput(psmtc_ResProximity_t this,
                struct device *parent,int bustype);

void smtc_ResProximity_processOutProx(psmtc_ResProximity_t this,s16 data);

void smtc_ResProximity_processInProx(psmtc_ResProximity_t this,s16 data);

void smtc_ResProximity_remove(psmtc_ResProximity_t this);


#endif // _SMTC_RESPROXIMITYPROCESS_H_

