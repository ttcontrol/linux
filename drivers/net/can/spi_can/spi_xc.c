/*******************************************************************************
 *** Network device driver for transfering CAN-message using SPI to the XC2000
 *** CPU.
 ***
 *** File:          spi_xc.can
 *** Author:        Markus Ferringer
 *** Created Date:  11/02/2010
 *** Last Modified: 11/02/2010
 ******************************************************************************/
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <linux/sched.h>
#include <linux/spi/spi.h>
#include <linux/syscalls.h>
#include <linux/irqreturn.h>
#include <linux/signal.h>
#include <linux/interrupt.h>
#include <linux/errno.h>
#include <asm/irq.h>
#include <linux/spinlock.h>
#include <linux/irq.h>
#include <linux/workqueue.h>
#include <asm/atomic.h>

#include "protocol.h"
#include "spi_prot.h"

#undef MOD_NAME
#define MOD_NAME "spi_xc: "

#define TX_BUFFER_SIZE 256
#define MSG_PER_TRANSFER 16
#define BITS_PER_MSG (8 * sizeof(struct spi_frame_t))

/*******************************************************************************/
/***                                                                         ***/
/*** DEFINES AND GENERAL DECLARATIONS/DIRECTIVES                             ***/
/***                                                                         ***/
/*******************************************************************************/
int spi_xc_read_write(struct spi_frame_t *_frame);
static void work_handler(struct work_struct *work);

/*******************************************************************************/
/***                                                                         ***/
/*** VARIABLE DECLARATIONS                                                   ***/
/***                                                                         ***/
/*******************************************************************************/
static struct spi_frame_t tx_buffer[TX_BUFFER_SIZE];
static ubyte2 tx_buffer_rd;
static ubyte2 tx_buffer_wr;
static atomic_t tx_fill = ATOMIC_INIT(0);
static struct spi_message message;
static struct spi_transfer transfer;

static struct work_struct worker;
static struct workqueue_struct *work_queue;

static struct mutex *spi_lock;                         // Mutex for this module
static struct spi_device *spi_xc_device;               // Corresponding SPI-device structure
static int spi_xc_irq = 0;                             // IRQ-handle
volatile struct spi_statistics_t spi_stats;                     // Statistics structure


/*******************************************************************************/
/***                                                                         ***/
/*** "MODULE" FUNCTIONS                                                      ***/
/***                                                                         ***/
/*******************************************************************************/

int spi_xc_read_write(struct spi_frame_t *_frame) {
    static spinlock_t mr_lock = SPIN_LOCK_UNLOCKED;
    unsigned long flags;
    unsigned int wr_ptr_next;
    
    PRINT_DBG("spi_xc_read_write() called...\n");

    // Sometimes, this function is called in non-IRQ-mode, and could thus be
    // interrupted by another spi_xc_read_write() call. This must be avoided, as
    // the tx_buffer_wr variable must be consistent for all calls.
    spin_lock_irqsave(&mr_lock, flags);
    
    wr_ptr_next = (tx_buffer_wr + 1) % TX_BUFFER_SIZE;
    // Check for overflow (notice that the very last element is always empty)
    //if (wr_ptr_next == tx_buffer_rd) {
    if (atomic_read(&tx_fill) >= TX_BUFFER_SIZE) {
        spi_stats.spi_tx_overflow++;
    } else {
        // There is still memory free in the buffer
        _frame->status |= SPI_STATUS_MAGIC;
        memcpy((uint8_t *)&tx_buffer[tx_buffer_wr], (uint8_t *)_frame, sizeof(struct spi_frame_t));
        tx_buffer_wr = wr_ptr_next;
        atomic_inc(&tx_fill);
    }
    
    spin_unlock_irqrestore(&mr_lock, flags);
    return 0;
}
EXPORT_SYMBOL(spi_xc_read_write);

static void work_handler(struct work_struct *work) {
    unsigned int buf_used, i, idx;
    struct spi_frame_t *frame;
    
    mutex_lock(spi_lock);
    
    buf_used = atomic_read(&tx_fill);
    if (buf_used > MSG_PER_TRANSFER) {
        spi_stats.tx_buf_warning++;
        // Limit the number to the maximum of one transfer
        buf_used = MSG_PER_TRANSFER;
    }
    
    // Copy all messages to the respective transfer-buffer
    for (i = 0; i < buf_used; ++i) {
        idx = (tx_buffer_rd + i) % TX_BUFFER_SIZE;
        memcpy((uint8_t *)((uint8_t *)transfer.tx_buf + i * sizeof(struct spi_frame_t)), 
               (uint8_t *)&tx_buffer[idx], 
               sizeof(struct spi_frame_t));
    }
    tx_buffer_rd = (tx_buffer_rd + buf_used) % TX_BUFFER_SIZE;
    atomic_sub(buf_used, &tx_fill);

    // If necessary, write dmy-messages to all the remaining entries
    for (i = buf_used; i < MSG_PER_TRANSFER; ++i) {
        spi_stats.tx_dmy++;
        // Just use a IDLE command to transmit a dummy message (instead of setting everything to 0)
        frame = (struct spi_frame_t *)((uint8_t *)transfer.tx_buf + i * sizeof(struct spi_frame_t));
        frame->command = SPI_CMD_IDLE;
    }
    
    spi_stats.tx_all += MSG_PER_TRANSFER;
    spi_stats.rx_all += MSG_PER_TRANSFER;
    spi_stats.messages++;
    spi_stats.transfers++;
    
    spi_message_add_tail(&transfer, &message);
    spi_sync(spi_xc_device, &message);
    
    SET_DBG_LED(TRUE);
    
    // Now process all received messages
    for (i = 0; i < MSG_PER_TRANSFER; ++i) {
        frame = (struct spi_frame_t *)((uint8_t *)transfer.rx_buf + i * sizeof(struct spi_frame_t));
        // We totally ignore IDLEs (=dmy messages)
        if (frame->command != SPI_CMD_IDLE) {
            spi_prot_processMessage(frame); 
        } else {
            spi_stats.rx_dmy++;
        }
    }
    spi_transfer_del(&transfer);

    SET_DBG_LED(FALSE);
    mutex_unlock(spi_lock);
}

enum irqreturn XC_IRQ_Handler(int irq, void *dev_id) {
//    PRINT_DBG("XC_IRQ_Handler() called...\n");
    
    // The work_handler must be finished (ie, free) before it can be rescheduled
    if (mutex_is_locked(spi_lock) == 1) {
        spi_stats.spi_lock_error++;
    } else {
        queue_work(work_queue, &worker);
    }
    
    spi_stats.xc_irq++;
    return IRQ_HANDLED;
}

/*******************************************************************************/
/***                                                                         ***/
/*** MODULE FUNCTIONS                                                        ***/
/***                                                                         ***/
/*******************************************************************************/

struct spi_xc_t {
    char name[24];
    struct spi_frame_t frame;
    struct mutex lock;
    struct spi_device *spi;
};

static int xc_probe(struct spi_device *spi)
{
    struct spi_xc_t *priv;
    int ret = 0;

    PRINT_DBG("xc_probe() called...\n");

    priv = kmalloc(sizeof(struct spi_xc_t), GFP_KERNEL);
   
    if (!priv) {
        PRINT_ERR("xc_probe(): kmalloc(priv) failed!\n");
        return -ENOMEM;
    }
    
    mutex_init(&priv->lock);
    spi_lock = &priv->lock;
    priv->spi = spi;
    sprintf(priv->name, "spidev%d.%d-%s", spi->master->bus_num, spi->chip_select, "xc");
    dev_set_drvdata(&spi->dev, priv);
    spi->max_speed_hz = 6000000;
    spi->bits_per_word = BITS_PER_MSG * MSG_PER_TRANSFER;
    spi->chip_select = 2;
    spi->mode = SPI_MODE_1;

    spi_setup(spi);

    spi_xc_device = spi;
    
    // Initialize the buffer
    tx_buffer_rd = 0;
    tx_buffer_wr = 0;
    
    // Initialize the message
    memset(&message, 0, sizeof(struct spi_message));
    spi_message_init(&message);
    message.complete = 0;
    message.context = 0;
    message.spi = spi_xc_device;
    message.is_dma_mapped = 0;
    
    // Initialize the transfer
    memset(&transfer, 0, sizeof(struct spi_transfer));
    transfer.tx_buf = kmalloc(sizeof(struct spi_frame_t) * MSG_PER_TRANSFER, GFP_KERNEL);
    transfer.rx_buf = kmalloc(sizeof(struct spi_frame_t) * MSG_PER_TRANSFER, GFP_KERNEL);
    transfer.delay_usecs = 0;
    transfer.cs_change = 0;
    transfer.bits_per_word = BITS_PER_MSG * MSG_PER_TRANSFER;
    transfer.len = MSG_PER_TRANSFER * sizeof(struct spi_frame_t);
    
    work_queue = create_workqueue("SPI_XC_QUEUE");
    
    // Initialize worker
    INIT_WORK(&worker, work_handler);
    
    // Request XC-Interrupt
    spi_xc_irq = IOMUX_TO_IRQ(MX51_PIN_EIM_A23);
    gpio_direction_input(MX51_PIN_EIM_A23);
    set_irq_type(spi_xc_irq, IRQF_TRIGGER_FALLING);
    ret = request_irq(spi_xc_irq, XC_IRQ_Handler, 0, "xc_irq", NULL);
    if (ret) {
        PRINT_ERR("request_irq(EIM_A23) failed!\n");
        return -1;
    }

    spi_prot_probe();

    return ret;
}

static int xc_remove(struct spi_device *spi)
{
    struct spi_xc_t *priv = dev_get_drvdata(&spi->dev);

    PRINT_DBG("xc_remove(%s) called...\n", dev_name(&spi->dev));

    spi_prot_remove();
    if (spi_xc_irq != 0) {
        free_irq(spi_xc_irq, NULL);
    }

    kfree(priv);
    kfree(transfer.tx_buf);
    kfree(transfer.rx_buf);
    
    flush_workqueue(work_queue);
    destroy_workqueue(work_queue);
    
    return 0;
}

#ifdef CONFIG_PM
static int xc_suspend(struct spi_device *spi, pm_message_t mesg)
{
    struct spi_xc_t *priv = dev_get_drvdata(&spi->dev);

    PRINT_DBG("xc_suspend(%s) called...\n", dev_name(&spi->dev));
    disable_irq(spi_xc_irq);
    gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_UART3_TXD),1);
    return 0;
}


static int xc_resume(struct spi_device *spi)
{
    struct spi_xc_t *priv = dev_get_drvdata(&spi->dev);

    PRINT_DBG("xc_resume(%s) called...\n", dev_name(&spi->dev));
    enable_irq(spi_xc_irq);
    gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_UART3_TXD),0);
    // Wait some time for the XC to start
    msleep(10);
    spi_prot_resume();

    return 0;
}
#else
#define xc_suspend NULL
#define xc_resume  NULL
#endif


static struct spi_driver xc_driver = {
    .driver = {
        .name = "spi_xc",
        .bus = &spi_bus_type,
        .owner = THIS_MODULE,
    },
    .probe = xc_probe,
    .remove = xc_remove,
    .suspend = xc_suspend,
    .resume = xc_resume,
};

int spi_xc_init(void)
{
    PRINT_DBG("spi_xc_init() called...\n");
    
    memset((void *)&spi_stats, 0, sizeof(struct spi_statistics_t));
    spi_stats.imxVer.major = SPI_DRIVER_VERSION_MAJOR;
    spi_stats.imxVer.minor = SPI_DRIVER_VERSION_MINOR;
    spi_stats.imxVer.patch = SPI_DRIVER_VERSION_PATCH;
    
    return spi_register_driver(&xc_driver);
}

void spi_xc_exit(void)
{
    PRINT_DBG("spi_xc_exit() called...\n");

    spi_unregister_driver(&xc_driver);
}
