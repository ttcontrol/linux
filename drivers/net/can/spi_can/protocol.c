/*******************************************************************************
 *** Functions for encoding/decoding SPI-messages. Shared file for Linux and
 *** XC2000 CPU.
 ***
 *** File:          protocol.c
 *** Author:        Markus Ferringer
 *** Created Date:  30/10/2009
 *** Last Modified: 29/01/2010
 ******************************************************************************/
#ifndef XC2000_FIRMWARE
    #include <linux/module.h>
#endif

#include "protocol.h"

#ifdef XC2000_FIRMWARE
    #pragma section code=RAMFUNCS
#endif

/*******************************************************************************/
/*******************************************************************************/
/*** COMMON PROTOCOL FUNCTIONS *************************************************/
/*******************************************************************************/
/*******************************************************************************/
inline ubyte4 Ptr2Long(const ubyte1 *const _ptr)
{
    return (((ubyte4)_ptr[0] << 24) |
            ((ubyte4)_ptr[1] << 16) |
            ((ubyte4)_ptr[2] <<  8) |
            ((ubyte4)_ptr[3] <<  0));
}

inline void Long2Ptr(ubyte4 _val, ubyte1 *const _ptr)
{
    _ptr[0] = (ubyte1)(_val >> 24);
    _ptr[1] = (ubyte1)(_val >> 16);
    _ptr[2] = (ubyte1)(_val >>  8);
    _ptr[3] = (ubyte1)(_val >>  0);
}

inline ubyte2 Ptr2Int(const ubyte1 *const _ptr)
{
    return ((ubyte2)_ptr[0] << 8) | ((ubyte2)_ptr[1]);
}

inline void Int2Ptr(ubyte2 _val, ubyte1 *const _ptr)
{
    _ptr[0] = (ubyte1)(_val >> 8);
    _ptr[1] = (ubyte1)(_val >> 0);
}
/*******************************************************************************/
/*******************************************************************************/
/*** ASSEMBLING/DISASSEMBLING FRAMES *******************************************/
/*******************************************************************************/
/*******************************************************************************/
void protocol_txCANMsg(ubyte1 _channel,
                       ubyte4 _id,
                       ubyte1 _dlc,
                       const ubyte1 *const _data,
                       struct spi_frame_t *const _spiFrame)
{
    ubyte1 i;
    // Assemble message
    _spiFrame->command = SPI_CMD_DATA;
    _spiFrame->address = SPI_ADDR_CAN;
    _spiFrame->endpoint = _channel;
    _spiFrame->status = 0;
    Long2Ptr(_id, &_spiFrame->data[0]);
    _spiFrame->data[4] = _dlc;
    for (i = 0; i < 8; ++i) {
        _spiFrame->data[5 + i] = 0;
        if (i < _dlc) {
            _spiFrame->data[5 + i] = _data[i];
        }
    }
}

void protocol_rxCANMsg(const struct spi_frame_t *const _spiFrame,
                       ubyte1 *const _channel,
                       ubyte4 *const _id,
                       ubyte1 *const _dlc,
                       ubyte1 *const _data)
{
    ubyte1 i;
    *_channel = _spiFrame->endpoint;
    *_id = Ptr2Long(&_spiFrame->data[0]);
    *_dlc = _spiFrame->data[4];

    for (i = 0; i < *_dlc; ++i) {
        _data[i] = _spiFrame->data[i + 5];
    }
}

void protocol_txCANConfig0(ubyte1 _channel,
                           ubyte4 _baudrate,
                           ubyte1 _tseg1,
                           ubyte1 _tseg2,
                           ubyte1 _sjw,
                           bool _enabled,
                           bool _configured,
                           struct spi_frame_t *const _spiFrame)
{
    // Assemble message
    _spiFrame->command = SPI_CMD_CONFIG0;
    _spiFrame->address = SPI_ADDR_CAN;
    _spiFrame->endpoint = _channel;
    _spiFrame->status = 0;
    Long2Ptr(_baudrate, &_spiFrame->data[0]);
    _spiFrame->data[4]  = _tseg1;
    _spiFrame->data[5]  = _tseg2;
    _spiFrame->data[6]  = _sjw;
    _spiFrame->data[7]  = _enabled;    // Device enabled?
    _spiFrame->data[8]  = _configured; // STATUS only: device configured?
    memset(&_spiFrame->data[9], 0, SPI_DATA_MAX - 9);
}

void protocol_rxCANConfig0(const struct spi_frame_t *const _spiFrame,
                           ubyte1 *const _channel,
                           ubyte4 *const _baudrate,
                           ubyte1 *const _tseg1,
                           ubyte1 *const _tseg2,
                           ubyte1 *const _sjw,
                           bool *const _enabled,
                           bool *const _configured)
{
    *_channel = _spiFrame->endpoint;
    *_baudrate = Ptr2Long(&_spiFrame->data[0]);
    *_tseg1 = _spiFrame->data[4];
    *_tseg2 = _spiFrame->data[5];
    *_sjw = _spiFrame->data[6];
    *_enabled = (bool)_spiFrame->data[7];
    *_configured = (bool)_spiFrame->data[8];
}

void protocol_txCANStatus1(ubyte1 _channel, 
                           ubyte2 _state, 
                           ubyte2 _txCnt, 
                           ubyte2 _rxCnt, 
                           struct spi_frame_t *const _spiFrame)
{
    _spiFrame->command = SPI_CMD_STATUS1;
    _spiFrame->address = SPI_ADDR_CAN;
    _spiFrame->endpoint = _channel;
    _spiFrame->status = 0;
    Int2Ptr(_state, &_spiFrame->data[0]);
    Int2Ptr(_txCnt, &_spiFrame->data[2]);
    Int2Ptr(_rxCnt, &_spiFrame->data[4]);
    memset(&_spiFrame->data[6], 0, SPI_DATA_MAX - 6);
}

void protocol_rxCANStatus1(const struct spi_frame_t *const _spiFrame, 
                           ubyte1 *const _channel, 
                           ubyte2 *const _state, 
                           ubyte2 *const _txCnt, 
                           ubyte2 *const _rxCnt)
{
    *_channel = _spiFrame->endpoint;
    *_state = Ptr2Int(&_spiFrame->data[0]);
    *_txCnt = Ptr2Int(&_spiFrame->data[2]);
    *_rxCnt = Ptr2Int(&_spiFrame->data[4]);
}


void protocol_txError(ubyte1 _addr,
                      ubyte1 _endpoint,
                      ubyte1 _cmd,
                      ubyte4 _error,
                      struct spi_frame_t *const _spiFrame)
{
    _spiFrame->command = SPI_CMD_ERROR;
    _spiFrame->address = _addr;
    _spiFrame->endpoint = _endpoint;
    _spiFrame->status = 0;
    _spiFrame->data[0] = _cmd;
    Long2Ptr(_error, &_spiFrame->data[1]);
    memset(&_spiFrame->data[5], 0, SPI_DATA_MAX - 5);
}

void protocol_rxError(const struct spi_frame_t *const _spiFrame,
                      ubyte1 *_addr,
                      ubyte1 *_endpoint,
                      ubyte1 *_cmd,
                      ubyte4 *_error)
{
    *_addr = _spiFrame->address;
    *_endpoint = _spiFrame->endpoint;
    *_cmd = _spiFrame->data[0];
    *_error = Ptr2Long(&_spiFrame->data[1]);
}

void protocol_txReset(struct spi_frame_t *const _spiFrame)
{
    _spiFrame->command = SPI_CMD_RESET;
    _spiFrame->endpoint = SPI_ENDP_DEVICE;
    _spiFrame->status = 0;
    memset(&_spiFrame->data[0], 0, SPI_DATA_MAX);
}

void protocol_txEncoderMsg(sbyte2 _abs,
                           sbyte2 _rel,
                           ubyte1 _button,
                           struct spi_frame_t *const _spiFrame)
{
    _spiFrame->command = SPI_CMD_DATA;
    _spiFrame->endpoint = SPI_ENDP_ENCODER0;
    _spiFrame->address = SPI_ADDR_ENCODER;
    _spiFrame->status = 0;
    Int2Ptr((ubyte2)_abs, &_spiFrame->data[0]);
    Int2Ptr((ubyte2)_rel, &_spiFrame->data[2]);
    _spiFrame->data[4] = _button;
    memset(&_spiFrame->data[5], 0, SPI_DATA_MAX - 5);
}

void protocol_rxEncoderMsg(const struct spi_frame_t *const _spiFrame,
                           sbyte2 *_abs,
                           sbyte2 *_rel,
                           ubyte1 *_button)
{
    *_abs = (sbyte2)Ptr2Int(&_spiFrame->data[0]);
    *_rel = (sbyte2)Ptr2Int(&_spiFrame->data[2]);
    *_button = _spiFrame->data[4];
}

void protocol_txEncoderConfig0(ubyte1 _inc,
                               ubyte1 _thres,
                               ubyte1 _repeat,
                               ubyte1 _enabled,
                               struct spi_frame_t *const _spiFrame)
{
    _spiFrame->address = SPI_ADDR_ENCODER;
    _spiFrame->endpoint = SPI_ENDP_ENCODER0;
    _spiFrame->command = SPI_CMD_CONFIG0;
    _spiFrame->status = 0;
    _spiFrame->data[0] = _inc;
    _spiFrame->data[1] = _thres;
    _spiFrame->data[2] = _repeat;
    _spiFrame->data[3] = _enabled;
    memset(&_spiFrame->data[4], 0, SPI_DATA_MAX - 4);
}

void protocol_rxEncoderConfig0(const struct spi_frame_t *const _spiFrame,
                               ubyte1 *_inc,
                               ubyte1 *_thres,
                               ubyte1 *_repeat,
                               ubyte1 *_enabled)
{
    *_inc = _spiFrame->data[0];
    *_thres = _spiFrame->data[1];
    *_repeat = _spiFrame->data[2];
    *_enabled = _spiFrame->data[3];
}

void protocol_txVersion(ubyte1 _major, ubyte1 _minor, ubyte1 _patch, struct spi_frame_t *const _spiFrame)
{
    _spiFrame->command = SPI_CMD_CONFIG0;
    _spiFrame->address = SPI_ADDR_DEVICE;
    _spiFrame->endpoint = SPI_ENDP_DEVICE;
    _spiFrame->status = 0;
    _spiFrame->data[0] = _major;
    _spiFrame->data[1] = _minor;
    _spiFrame->data[2] = _patch;
    memset(&_spiFrame->data[3], 0, SPI_DATA_MAX - 3);
}

void protocol_rxVersion(const struct spi_frame_t *const _spiFrame, ubyte1 *_major, ubyte1 *_minor, ubyte1 *_patch)
{
    *_major = _spiFrame->data[0];
    *_minor = _spiFrame->data[1];
    *_patch = _spiFrame->data[2];
}

void protocol_txLEDS(ubyte1 _green, ubyte1 _red, struct spi_frame_t *const _spiFrame)
{
    _spiFrame->command = SPI_CMD_CONFIG0;
    _spiFrame->address = SPI_ADDR_LEDS;
    _spiFrame->endpoint = SPI_ENDP_LEDS;
    _spiFrame->status = 0;
    _spiFrame->data[0] = _green;
    _spiFrame->data[1] = _red;
    memset(&_spiFrame->data[2], 0, SPI_DATA_MAX - 2);
}

void protocol_rxLEDS(const struct spi_frame_t *const _spiFrame, ubyte1 *_green, ubyte1 *_red)
{
    *_green = _spiFrame->data[0];
    *_red = _spiFrame->data[1];
}

void protocol_txDevConfig(ubyte1 _channels, ubyte1 _rxFifo, ubyte1 _txFifo, ubyte1 _irqTime, ubyte1 _encTime, ubyte2 _rs485Time, ubyte2 _rs232Time, struct spi_frame_t *const _spiFrame)
{
    _spiFrame->command = SPI_CMD_CONFIG1;
    _spiFrame->address = SPI_ADDR_DEVICE;
    _spiFrame->endpoint = SPI_ENDP_DEVICE;
    _spiFrame->status = 0;
    _spiFrame->data[0] = _channels;
    _spiFrame->data[1] = _rxFifo;
    _spiFrame->data[2] = _txFifo;
    _spiFrame->data[3] = _irqTime;
    _spiFrame->data[4] = _encTime;
    Int2Ptr(_rs485Time, &_spiFrame->data[5]);
    Int2Ptr(_rs232Time, &_spiFrame->data[7]);
    memset(&_spiFrame->data[9], 0, SPI_DATA_MAX - 9);
}

void protocol_rxDevConfig(const struct spi_frame_t *const _spiFrame, ubyte1 *_channels, ubyte1 *_rxFifo, ubyte1 *_txFifo, ubyte1 *_irqTime, ubyte1 *_encTime, ubyte2 *_rs485Time, ubyte2 *_rs232Time)
{
    if (_channels) { 
        *_channels = _spiFrame->data[0]; 
    }
    if (_rxFifo) { 
        *_rxFifo = _spiFrame->data[1]; 
    }
    if (_txFifo) { 
        *_txFifo = _spiFrame->data[2]; 
    }
    
    *_irqTime = _spiFrame->data[3];
    *_encTime = _spiFrame->data[4];
    *_rs485Time = Ptr2Int(&_spiFrame->data[5]);
    *_rs232Time = Ptr2Int(&_spiFrame->data[7]);
}
#ifdef XC2000_FIRMWARE
    #pragma endsection
#endif
















