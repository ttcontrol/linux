/*******************************************************************************
 *** Network device driver for transfering CAN-message using SPI to the XC2000
 *** CPU.
 ***
 *** File:          spi_can.c
 *** Author:        Markus Ferringer
 *** Created Date:  30/10/2009
 *** Last Modified: 13/01/2010
 ******************************************************************************/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#include <linux/platform_device.h>
#include <linux/errno.h>
#include <net/rtnetlink.h>

#include <linux/can.h>
#include <linux/can/dev.h>
//#include "linux/can/version.h"

#include "spi_can.h"
#include "spi_prot.h"
#include "protocol.h"
#include "procfs.h"

/*******************************************************************************/
/***                                                                         ***/
/*** DEFINES AND GENERAL DECLARATIONS/DIRECTIVES                             ***/
/***                                                                         ***/
/*******************************************************************************/
#undef MOD_NAME
#define MOD_NAME "spi_can: "

MODULE_DESCRIPTION("spi-can interface");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Markus Ferringer <mfr@ttcontrol.com>");

/*******************************************************************************/
/***                                                                         ***/
/*** PROTOTYPE DECLARATIONS                                                  ***/
/***                                                                         ***/
/*******************************************************************************/
static struct net_device *add_channel(void);
static int spi_can_bittiming(struct net_device *dev);
static int spi_can_mode(struct net_device *dev, enum can_mode mode);
static int spi_can_open(struct net_device *dev);
static int spi_can_stop(struct net_device *dev);
static int spi_can_transmit(struct sk_buff *skb, struct net_device *dev);


/*******************************************************************************/
/***                                                                         ***/
/*** VARIABLE DECLARATIONS                                                   ***/
/***                                                                         ***/
/*******************************************************************************/
static const struct net_device_ops spi_can_netdev_ops = {
    .ndo_start_xmit = spi_can_transmit,
    .ndo_open = spi_can_open,
    .ndo_stop = spi_can_stop,
};

struct spi_can_priv_t {
    struct can_priv      can;     /* must be the first member! */
    struct net_device   *netdev;
    ubyte1               can_if;
};

static struct net_device *spi_can_devs[SPI_ENDP_CAN_MAX];

/*******************************************************************************/
/***                                                                         ***/
/*** GENERAL FUNCTIONS                                                       ***/
/***                                                                         ***/
/*******************************************************************************/
static struct net_device *add_channel(void) {
    struct net_device *dev;
    struct spi_can_priv_t *priv;
    int err, i;
    bool found;
    char name[10];

    dev = alloc_candev(sizeof(*priv), 0);
    priv = netdev_priv(dev);
    priv->netdev = dev;
    priv->can.do_set_bittiming = spi_can_bittiming;
    priv->can.do_set_mode = spi_can_mode;
    priv->can.bittiming.bitrate = SPI_CAN_DEFAULT_BITRATE;
    priv->can.bittiming.sjw = SPI_CAN_DEFAULT_SJW;
    priv->can.bittiming.phase_seg1 = SPI_CAN_DEFAULT_TSEG1;
    priv->can.bittiming.phase_seg2 = SPI_CAN_DEFAULT_TSEG2;
    priv->can.state = CAN_STATE_STOPPED;

    dev->flags = IFF_NOARP | IFF_ECHO;
    dev->netdev_ops = &spi_can_netdev_ops;

    err = register_candev(dev);
    if (err) {
        goto error;
    }

    // check the name and assign the index
    found = FALSE;
    for (i = 0; i < SPI_ENDP_CAN_MAX; ++i) {
        sprintf(name, "can%i", i);
        if (strcmp(dev->name, name) == 0) {
            PRINT_DBG("Found name: %s; dev->name: %s\n", name, dev->name);
            found = TRUE;
            priv->can_if = i;
            break;
        }
    }
    // Error if not found (string other than can0 - can3)
    if (found == FALSE) {
        PRINT_ERR("Cannot find device! [name: %s; dev->name: %s]\n", name, dev->name);
        err = -ENODEV;
        goto error;
    }

    return dev;

error:
    unregister_candev(dev);
    free_netdev(dev);
    return ERR_PTR(err);
}

int spi_can_receive(ubyte1 _dev, struct can_frame *_frame) {
    struct net_device *dev;
    struct spi_can_priv_t *priv;
    struct sk_buff *skb;
    struct net_device_stats *stats;
    struct can_frame *dummy_frame;

    PRINT_INF("spi_can_receive(CAN%i) called...\n", _dev);

    // Internal function only, parameter check not necessary

    // Get the corresponding device and stats
    dev = spi_can_devs[_dev];
    priv = netdev_priv(dev);
    stats = &dev->stats;
    
    // create sk_buff
    skb = netdev_alloc_skb(dev, sizeof(struct can_frame));
    if (unlikely(!skb))
        return SPI_ERR_KERNEL;
        
    skb->protocol = __constant_htons(ETH_P_CAN);
    skb->pkt_type = PACKET_BROADCAST;
    skb->ip_summed = CHECKSUM_UNNECESSARY;
    dummy_frame = (struct can_frame *)skb_put(skb, sizeof(struct can_frame));
    skb->dev = dev;
    memcpy(dummy_frame, _frame, sizeof(struct can_frame));
    
    // Receive the frame using SocketCAN
    // Increase the stats-counters in case of error-messages
    if (_frame->can_id & CAN_ERR_FLAG) {
        if (_frame->can_id & CAN_ERR_BUSOFF) {
            priv->can.can_stats.bus_off++;
            priv->can.state = CAN_STATE_BUS_OFF;
        }
        else if (_frame->can_id & CAN_ERR_CRTL) {
            if ((_frame->data[1] & CAN_ERR_CRTL_TX_PASSIVE) ||
                (_frame->data[1] & CAN_ERR_CRTL_RX_PASSIVE))
            {
                priv->can.can_stats.error_passive++;
                priv->can.state = CAN_STATE_ERROR_PASSIVE;
            }
            if (_frame->data[1] & CAN_ERR_CRTL_RX_OVERFLOW) {
                stats->rx_dropped++;
                //stats->rx_over_errors++;
                stats->rx_errors++;
            }
            if (_frame->data[1] & CAN_ERR_CRTL_TX_OVERFLOW) {
                //stats->tx_over_errors++;
                stats->tx_dropped++;
                stats->tx_errors++;
            }
        }
        else
        {
            priv->can.state = CAN_STATE_ERROR_ACTIVE;
        }
    }
    else
    {
        dev->last_rx = jiffies;
        stats->rx_packets++;
        stats->rx_bytes += _frame->can_dlc;
    }

    netif_receive_skb(skb);
    
    return SPI_OK;
}

/*******************************************************************************/
/***                                                                         ***/
/*** MODULE FUNCTIONS                                                        ***/
/***                                                                         ***/
/*******************************************************************************/
static int spi_can_bittiming(struct net_device *dev) {
    struct spi_can_priv_t *priv = netdev_priv(dev);
    struct can_bittiming *bt = &priv->can.bittiming;
    struct spi_can_status_t stat;

    PRINT_INF("spi_can_bittiming(CAN%i, %i) called...\n", priv->can_if, bt->bitrate);
    
    spi_prot_CANStatus(priv->can_if, &stat);
    
    // If the parameters are zero, we don't change them
    if (bt->bitrate == 0) bt->bitrate = stat.baudrate;
    if (bt->phase_seg1 == 0) bt->phase_seg1 = stat.tseg1;
    if (bt->phase_seg2 == 0) bt->phase_seg2 = stat.tseg2;
    if (bt->sjw == 0) bt->sjw = stat.sjw;
    bt->prop_seg = 1;
    bt->sample_point = 700;
    bt->tq = 1000000000UL / (ubyte4)(bt->bitrate * (1 + 1 + bt->phase_seg1 + bt->phase_seg2));
    priv->can.clock.freq = 123456;
    spi_prot_CANConfigure(priv->can_if, bt->bitrate, bt->phase_seg1, bt->phase_seg2, bt->sjw, TRUE);
    priv->can.state = CAN_STATE_ERROR_ACTIVE;
    
    return 0;
}

static int spi_can_mode(struct net_device *dev, enum can_mode mode) {
    PRINT_DBG("spi_can_mode(Mode %i) called...\n", mode);
    switch (mode) {
        case CAN_MODE_START:
            spi_can_open(dev);
            if (netif_queue_stopped(dev))
            {
                netif_wake_queue(dev);
            }
            break;
        default:
            return -EOPNOTSUPP;
    }
    return 0;
}

static int spi_can_open(struct net_device *dev) {
    PRINT_INF("spi_can_open(CAN%i) called...\n", ((struct spi_can_priv_t *)netdev_priv(dev))->can_if);
    
    // Configure the CAN-Device if it's opened.
    spi_can_bittiming(dev);
    return 0;
}

static int spi_can_stop(struct net_device *dev) {
    struct spi_can_priv_t *priv = netdev_priv(dev);

    PRINT_INF("spi_can_stop(CAN%i) called...\n", priv->can_if);
    
    // Explicitly disable the device when it's closed
    spi_prot_CANConfigure(priv->can_if, 
                          SPI_CAN_DEFAULT_BITRATE, 
                          SPI_CAN_DEFAULT_TSEG1, 
                          SPI_CAN_DEFAULT_TSEG2, 
                          SPI_CAN_DEFAULT_SJW, 
                          FALSE);
    priv->can.state = CAN_STATE_STOPPED;
    
    return 0;
}

static int spi_can_transmit(struct sk_buff *skb, struct net_device *dev)
{
    struct can_frame *cf = (struct can_frame *)skb->data;
    struct net_device_stats *stats = &dev->stats;
    struct spi_can_priv_t *priv = netdev_priv(dev);

    PRINT_DBG("spi_can_transmit(SPI_CAN_%i) called (len: %i %i)...\n", priv->can_if, skb->data_len, skb->len);
    PRINT_INF("spi_can_transmit(CAN%i) called...\n", priv->can_if);
    
    stats->tx_packets++;
    stats->tx_bytes += cf->can_dlc;
    
    spi_prot_CANWrite(priv->can_if, cf);
 
    kfree_skb(skb);
 
    return NET_XMIT_SUCCESS;
}

/*******************************************************************************/
/***                                                                         ***/
/*** MODULE LOAD/UNLOAD FUNCTIONS                                            ***/
/***                                                                         ***/
/*******************************************************************************/
int spi_can_init(void)
{
    int i;
    PRINT_DBG("spi_can_init() called...\n");
    
    gpio_request(IOMUX_TO_GPIO(MX51_PIN_GPIO1_6), "gpio1_6");
    gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_GPIO1_6), 0);
    gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_6), 0);
    gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_6), 1);
        
    procfs_create_root();
    for (i = 0; i < SPI_ENDP_CAN_MAX; ++i) {
        spi_can_devs[i] = NULL;
        spi_can_devs[i] = add_channel();
        if (IS_ERR(spi_can_devs[i])) {
            PRINT_ERR ("add_channel() failed for device-index %i with error-code %i\n", i, (int)PTR_ERR(spi_can_devs[i]));
            spi_can_exit();
            return -1;
        }
        procfs_add_device(i);
    }
    
    return 0;
}

void spi_can_exit(void)
{
    int i;
    PRINT_DBG("spi_can_exit() called...\n");

    for (i = 0; i < SPI_ENDP_CAN_MAX; ++i) {
        if (!IS_ERR(spi_can_devs[i])) {
            unregister_candev(spi_can_devs[i]);
            free_candev(spi_can_devs[i]);
            spi_can_devs[i] = NULL;
        }
        procfs_remove_device(i);
    }
    procfs_remove_root();
}


