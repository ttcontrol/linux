#ifndef SPI_XC_H
#define SPI_XC_H

#include "protocol.h"

void   spi_xc_init(void);
void   spi_xc_exit(void);
int    spi_xc_read_write(struct spi_frame_t *_frame);

extern volatile struct spi_statistics_t spi_stats;

#endif
