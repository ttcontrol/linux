#ifndef GLOBALS_H
#define GLOBALS_H

#ifndef SOCKETCAN_TEST
    #include <mach/hardware.h>
    #include <mach/gpio.h>
    #include <../arch/arm/mach-mx5/mx51_pins.h>
#endif

#ifndef bool
    #define bool unsigned char
#endif

#ifndef FALSE
    #define FALSE (1 == 0)
#endif

#ifndef TRUE
    #define TRUE (1 == 1)
#endif

//#define SPI_CAN_DEBUG

#define SPI_DRIVER_VERSION_MAJOR 1
#define SPI_DRIVER_VERSION_MINOR 6
#define SPI_DRIVER_VERSION_PATCH 0

#define MOD_NAME "<unknown>: "
#ifdef SPI_CAN_DEBUG
    #define PRINT_DBG(s...) printk(KERN_INFO "[" __TIMESTAMP__ "] " MOD_NAME s)
    #define PRINT_INF(s...) printk(KERN_INFO "[" __TIMESTAMP__ "] " MOD_NAME s)
    #define SET_DBG_LED(val) gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_6), val)
#else
    #define PRINT_DBG(s...) 0
    #define PRINT_INF(s...) 0
    #define SET_DBG_LED(val) 0
#endif
#define PRINT_ERR(s...) printk(KERN_ERR "[" __TIMESTAMP__ "] " MOD_NAME s)


typedef unsigned char ubyte1;
typedef unsigned short ubyte2;
typedef unsigned int ubyte4;
typedef unsigned long ubyte8;

typedef signed char sbyte1;
typedef signed short sbyte2;
typedef signed int sbyte4;
typedef signed long sbyte8;

#endif


