#ifndef PROTOCOL_H
#define PROTOCOL_H

#ifdef XC2000_FIRMWARE
    #include "ptypes_xe167.h"
    #include <string.h>
#else
    #include "globals.h"
#endif

#define SPI_CAN_OK             0x00
#define SPI_CAN_ENABLED        0x00
#define SPI_CAN_DISABLED       0x01

#define SPI_CAN_ERROR_NONE     0x0000
#define SPI_CAN_ERROR_UNKNOWN  0x0001
#define SPI_CAN_ERROR_ACTIVE   0x0002
#define SPI_CAN_ERROR_PASSIVE  0x0004
#define SPI_CAN_ERROR_BUSOFF   0x0008
#define SPI_CAN_ERROR_RX_OF    0x0010 // Overflow in RX buffer
#define SPI_CAN_ERROR_TX_OF    0x0020 // Overflow in TX buffer

#define SPI_ENCODER_BTN_NO_CHANGE  0x00   // No change of state since last time
#define SPI_ENCODER_BTN_DOWN       0x01   // button has been pressed (EVENT)
#define SPI_ENCODER_BTN_UP         0x02   // button has been released (EVENT)
#define SPI_ENCODER_BTN_DOWN_RPT   0x03   // button has been pressed (REPEAT-EVENT), and repeat-rate is > 0 => repeat-event

#define SPI_CAN_EXT_FRAME (ubyte4)0x80000000U

//#define SPI_STATUS_MORETOSEND  0x04
//#define SPI_STATUS_REQUEST     0x02
#define SPI_STATUS_ACKFRAME    0x01

//#define IsRequest(stat)    (((stat) & SPI_STATUS_REQUEST) != 0)
//#define IsMoreToSend(stat) (((stat) & SPI_STATUS_MORETOSEND) != 0)
#define IsAckFrame(stat)   (((stat) & SPI_STATUS_ACKFRAME) != 0)

/****************************************************************************//**
 * FLAGS
 * =====
 * [M|S|r][A|-]
 * M ... Sent by Master to Slave
 * S ... Sent by Slave to Master
 * r ... Sent as response to an explicit request.
 * c ... Sent automatically by slave if one of the respective fields changes
 * A ... Slave must explicitly acknowledge the message using the respective status-command
 * - ... Message is not explicitly acknowledged.
 *
 * STATUS-BYTE
 * ===========
 * |==============|==============|==============|==============|==============|==============|==============|==============|
 * |    Bit7      |    Bit6      |    Bit5      |    Bit4      |    Bit3      |    Bit2      |    Bit1      |    Bit0      |
 * |==============|==============|==============|==============|==============|==============|==============|==============|
 * | Checksum [4] | Checksum [3] | Checksum [2] | Checksum [1] | Checksum [0] | MoreToSend[1]| MoreToSend[0]| IsAckFrame   |
 * |--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|
 * Checksum is a magic word, 0xA8
 *
 * ERROR-FRAME
 * ===========
 * |=======|==============|==============|==============|==============|==============|==============|
 * | Flags |   1 Byte     |    1 Byte    |   1 Byte     |   1 Byte     |   4 Bytes    |  8 Bytes     |
 * |=======|==============|==============|==============|==============|==============|==============|
 * | S-    |    ERROR     |   ADDR+ENDP  |   STATUS     |   COMMAND    |  ERR-CODE3-0 |    0x00      |
 * |-------|--------------|--------------|--------------|--------------|--------------|--------------|
 *
 * SPI_ADDR_DEVICE
 * ===============
 * |=======|==============|==============|==============|==============|
 * | Flags |   1 Byte     |    1 Byte    |   1 Byte     |  13 Bytes    |
 * |=======|==============|==============|==============|==============|
 * | M-    |    RESET     |       0      |   STATUS     |      0       |
 * |-------|--------------|--------------|--------------|--------------|
 *
 * |=======|==============|==============|==============|==============|==============|==============|==============|
 * | Flags |   1 Byte     |    1 Byte    |   1 Byte     |   1 Byte     |   1 Byte     |   1 Byte     |   10 Bytes   |
 * |=======|==============|==============|==============|==============|==============|==============|==============|
 * | MA    |   CONFIG0    |       0      |   STATUS     |  VER_MAJOR   |  VER_MINOR   |  VER_PATCH   |      0       |
 * |-------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|
 * | r-    |   STATUS0    |       0      |   STATUS     |  VER_MAJOR   |  VER_MINOR   |  VER_PATCH   |      0       |
 * |-------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|
 *
 * |=======|==============|==============|==============|==============|==============|==============|==============|==============|==============|==============|==============|
 * | Flags |   1 Byte     |    1 Byte    |   1 Byte     |   1 Byte     |   1 Byte     |   1 Byte     |   1 Byte     |   1 Byte     |   2 Byte     |   2 Byte     |   4 Bytes    |
 * |=======|==============|==============|==============|==============|==============|==============|==============|==============|==============|==============|==============|
 * | M-    |   CONFIG1    |       0      |   STATUS     |      - [4]   |     - [4]    |     - [4]    |  IRQ_TIME    |  ENC_TIME    |  RS485_TIME  |  RS232_TIME  |      0       |
 * |-------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|
 * | M-    |   STATUS1    |       0      |   STATUS     |  CAN_CH_CNT  |  RX_FIFOS    |  TX_FIFOS    |  IRQ_TIME    |  ENC_TIME    |  RS485_TIME  |  RS232_TIME  |      0       |
 * |-------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|
 * [1] I use a separate command for configuring the timings, as usually one wants just to use the default settings.
 * [2] CAN_CH_CNT (Number of activated CAN-Channels)
 *     RX_FIFO, TX_FIFO (Number of used message objects for RX/TX per activated channel)
 *     IRQ_TIME (Period of iMX-IRQ line in multiples of 500us)
 *     ENC_TIME (Period of calling encoer-task in multiples of 1ms)
 *     RS485_TIME, RS232_TIME (Period of calling RS485 & RS232 Task in multiples of 100us)
 * [3] In CONFIG1, transmitting one of the values as zeros means that the respective value will not be changed.
 *     Basic range checks are performed at the target.
 * [4] Read-Only, values are don't care
 * [5] If irqTime is changed by the iMX, it must be AFTER _ALL_ CAN channels have been actived.
 *     Chaning this configuration also changes the respective assignment to the default value for the
 *     remaining number of activated CAN channels.
 *                                                                                                    
 * SPI_ADDR_CAN
 * ============
 * |=======|==============|==============|==============|==============|==============|==============|
 * | Flags |   1 Byte     |    1 Byte    |   1 Byte     |   4 Bytes    |   1 Byte     |  8 Bytes     |
 * |=======|==============|==============|==============|==============|==============|==============|
 * | M-    |     DATA     |   ADDR+ENDP  |   STATUS     |    ID3-0 [1] |    DLC       |  DATA7-0     | ... Transmitting CAN-Messages
 * |-------|--------------|--------------|--------------|--------------|--------------|--------------|
 * | S-    |     DATA     |   ADDR+ENDP  |   STATUS     |    ID3-0 [1] |    DLC       |  DATA7-0     | ... Receiving CAN-Messages
 * |-------|--------------|--------------|--------------|--------------|--------------|--------------|
 * [1] ID3-0 uses flags CAN_xxx_FLAG to identify extended/standard/remote frames
 *     from header-file <socketcan/can.h>.
 *
 * |=======|===========|=============|==========|============|===========|===========|==========|===========|==============|============|
 * | Flags |  1 Byte   |   1 Byte    |  1 Byte  |  4 Bytes   |   1 Byte  |   1 Byte  | 1 Byte   |  1 Byte   |  1 Byte      |  4 Bytes   |
 * |=======|===========|=============|==========|============|===========|===========|==========|===========|==============|============|
 * | MA    |  CONFIG0  |  ADDR+ENDP  |  STATUS  |  BAUDRATE  |   TSEG1   |   TSEG2   |   SJW    |ENABLED [1]|    0x00      |    0x00    |
 * |-------|-----------|-------------|----------|------------|-----------|-----------|----------|-----------|--------------|------------|
 * | SrcA  |  STATUS0  |  ADDR+ENDP  |  STATUS  |  BAUDRATE  |   TSEG1   |   TSEG2   |   SJW    |ENABLED [1]|CONFIGURED [1]|    0x00    | ... Automatically sent if any of the fields have changed
 * |-------|-----------|-------------|----------|------------|-----------|-----------|----------|-----------|--------------|------------|
 * [1] Either TRUE or FALSE.
 *
 * |=======|===========|=============|==========|============|===========|============|============|
 * | Flags |  1 Byte   |   1 Byte    |  1 Byte  |  2 Bytes   |  2 Bytes  |  2 Bytes   |   7 Bytes  |
 * |=======|===========|=============|==========|============|===========|============|============|
 * | Sc-   |  STATUS1  |  ADDR+ENDP  |  STATUS  | STATE [1]  | TX-ErrCnt | RX-ErrCnt  |   0x00     | ... Automatically sent if any of the fields have changed
 * |-------|-----------|-------------|----------|------------|-----------|------------|------------|
 * [1] SPI_CAN_ERROR_*, ERROR_ACTIVE is NOT signaled!
 *
 *
 * SPI_ADDR_ENCODER
 * ================
 * |=======|==============|==============|==============|==============|==============|==============|==============|
 * | Flags |   1 Byte     |    1 Byte    |   1 Byte     |   2 Bytes    |   2 Bytes    |   1 Byte     |  8 Bytes     |
 * |=======|==============|==============|==============|==============|==============|==============|==============|
 * | S-    |     DATA     |   ADDR+ENDP  |   STATUS     | Absolute [1] | Relative [1] |  Button [2]  |    0x00      | ... Transmitting Encoder-Position
 * |-------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|
 * [1] Absolute is the absolute position of the rotary encoder, in 16-bit signed (2's complement) format.
 *     Relative is the relative change since the last update, also in 16-bit signed (2' complement) format.
 * [2] Values according to SPI_ENCODER_BTN_* define the state of the button
 *
 * |=======|==============|==============|==============|==============|==============|==============|==============|==============|
 * | Flags |   1 Byte     |    1 Byte    |   1 Byte     |   1 Byte     |   1 Byte     |   1 Byte     |  1 Byte      |   11 Bytes   |
 * |=======|==============|==============|==============|==============|==============|==============|==============|==============|
 * | M-    |  CONFIG0     |   ADDR+ENDP  |   STATUS     | IncValue [1] | Threshold[1] | Repeat  [2]  | Enabled [3]  |     0x00     |
 * |-------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|
 * | SrA   |  STATUS0     |   ADDR+ENDP  |   STATUS     | IncValue [1] | Threshold[1] | Repeat  [2]  | Enabled [3]  |     0x00     |
 * |-------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|
 * [1] IncValue is the number that is added to a variable whenever the encoder performs one step.
 *     Threshold is the value which must be reached to actually send an encoder-rotation event.
 *     E.g., I = T = 1 => each step is one event message
 *     E.g., I = 2, T = 1 => each step is TWO event messages
 *     E.g., I = 1, T = 2 => each TWO steps are ONE event message
 *     !!! THESE SETTINGS ARE NOT SUPPORTED !!! I = T = 1 IS HARDCODED !!!
 * [2] If the button is pressed down, automatic repeat-events can be sent (like a real keyboard). To
 *     disable, write 0 to it. The unit is "events per second"
 *     !!! THIS SETTING IS NOT SUPPORTED !!! IT IS DISABLED !!!
 * [3] Either TRUE or FALSE. The latter prevents the sending of any event messages.
 *
 * SPI_ADDR_LEDS
 * =============
 * |=======|==============|==============|==============|==============|==============|==============|
 * | Flags |   1 Byte     |    1 Byte    |   1 Byte     |   1 Byte     |   1 Byte     |  13 Bytes    |
 * |=======|==============|==============|==============|==============|==============|==============|
 * | M-    |  CONFIG0     |   ADDR+ENDP  |   STATUS     |  % GREEN LED |  % RED LED   |    0x00      |
 * |-------|--------------|--------------|--------------|--------------|--------------|--------------|
 * | SrA   |  STATUS0     |   ADDR+ENDP  |   STATUS     |  % GREEN LED |  % RED LED   |    0x00      |
 * |-------|--------------|--------------|--------------|--------------|--------------|--------------|
 * [1] LED-values are specified in percent, i.e. 0 to 100 for none to maximum brightness.
 *******************************************************************************/
#define SPI_ADDR_RES       0    // Reserved address. Do not use.
#define SPI_ADDR_DEVICE    1    // "Address" of the entire chip (e.g., for SPI_CMD_RESET)
#define SPI_ADDR_CAN       2    // Base-Address of all CAN channels
#define SPI_ADDR_ENCODER   3    // Base-Address of the Enoder
#define SPI_ADDR_LEDS      4

#define SPI_ENDP_CAN0      0    // Endpoint-Address of CAN0
#define SPI_ENDP_CAN1      1    // Endpoint-Address of CAN1
#define SPI_ENDP_CAN2      2    // Endpoint-Address of CAN2
#define SPI_ENDP_CAN3      3    // Endpoint-Address of CAN3
#define SPI_ENDP_CAN_MAX   4    // Number of CAN-devices
#define SPI_ENDP_ENCODER0  0    // Endpoint-Address of Encoder0
#define SPI_ENDP_DEVICE    0    // Endpoint-Address of Device
#define SPI_ENDP_LEDS      0

// The commands are sorted in ascending priority, i.e. the higher the value, the
// higher the overall priority. If the send-bufferes are not handled in FIFO style,
// this might come in handy.
#define SPI_CMD_NOP        0     // No operation - mainly used to receive from slave
#define SPI_CMD_CONFIG0    1     // R/W configuration0 from target
#define SPI_CMD_CONFIG1    2     // R/W configuration1 from target
#define SPI_CMD_CONFIG2    3     // R/W configuration2 from target
#define SPI_CMD_RESET      4     // Reset and reconfigure the device (only the specified endpoint)
#define SPI_CMD_DATA       5     // R/W data to the target (eg, CAN, DIGOUT, etc)
#define SPI_CMD_ERROR      6     // Used to signal any errors
#define SPI_CMD_IDLE     255     // No operation - if slave has nothing to send, the line is high.
#define SPI_CMD_STATUS0 SPI_CMD_CONFIG0
#define SPI_CMD_STATUS1 SPI_CMD_CONFIG1
#define SPI_CMD_STATUS2 SPI_CMD_CONFIG2

#define SPI_ERROR_UNKNOWN         1
#define SPI_ERROR_NOT_ENABLED     2  // device or channel is not enabled
#define SPI_ERROR_NOT_CONFIGURED  3  // device or channel has not been configured
#define SPI_ERROR_BUSOFF          4  // CAN channel is in busoff
#define SPI_ERROR_UNKNOWN_CMD     5  // XC received an unknown command
#define SPI_ERROR_INVALID         6  // XC received an unknown/invalid spi frame
#define SPI_ERROR_BUF_RESET       7  // After to many invalid frames, the buffers have been reset
#define SPI_ERROR_UNKNOWN_ADDR    8  // The specified destination address is unkown or invalid

#define SPI_STATUS_MAGIC 0xA8     // Magic bits added to upper 5 bits of STATUS

#define SPI_DATA_MAX 13
struct spi_frame_t {
    ubyte1 command;             // Defines the command that is sent to slave/from slave
    ubyte1 address:5;           // Address of receiver/sender (e.g., CAN/DIGIN/ANIN/RS232
    ubyte1 endpoint:3;          // Endpoint of address, e.g., CAN0/CAN1/CAN2/AIN0/AIN1/AIN2...
    ubyte1 status;              // Status/Error byte. Only sent by slave, master sends all zeros
    ubyte1 data[SPI_DATA_MAX];  // Data-field: Payload
};
#define spi_frame_t_SIZE sizeof(struct spi_frame_t)

struct version_t {
    ubyte1 major;
    ubyte1 minor;
    ubyte1 patch; 
};

struct xc_device_info_t {
    ubyte1 activeChannels;   // number of activated CAN-channels
    ubyte1 rxFifos;          // number of rx message objects per channel
    ubyte1 txFifos;          // number of tx message objects per channel
    ubyte4 irqTime;          // period of iMX-IRQ line in us
    ubyte4 encTime;          // period of calling encoder task in us
    ubyte4 rs485Time;        // period of calling rs485 task in us
    ubyte4 rs232Time;        // period of calling rs232 task in us

  #ifdef XC2000_FIRMWARE
    ubyte4 irqStarttime;     // Starttime for time measurement with RTC
    ubyte4 encStarttime;     // Starttime for time measurement with RTC
    ubyte4 rs485Starttime;   // Starttime for time measurement with RTC
    ubyte4 rs232Starttime;   // Starttime for time measurement with RTC
  #endif
};

struct statistics_t {
    ubyte4 can_rx_all;     // All received CAN messages
    ubyte4 can_tx_all;     // All sent CAN messages
    ubyte4 spi_rx_all;     // All received spi messages (inc dmy)
    ubyte4 spi_rx_dmy;     // All received dmy messages
    ubyte4 spi_tx_dmy;     // -- unused -- All transmitted dummy messages 
    ubyte4 spi_tx_all;     // All transmitted messages (inc dmy)
    ubyte4 can_rx_full;    // CAN rx-FIFO full (maybe message lost, but cannot be determined) (sum for all channels)
    ubyte4 can_tx_lost;    // CAN Messages lost because CAN-tx buffers were full (sum for all channels)
    ubyte4 spi_rx_full;    // Incoming SPI Messages lost because receive buffer was full
    ubyte4 spi_tx_lost;    // Outgoing SPI messages lost because transmit buffer was full
    ubyte4 invalid_frame;  // Number of received invalid frames
    ubyte4 spi_gen_error;  // General SPI-erro (e.g., chipselect active in SPI-ISR, received message too short
    ubyte4 tx_buff_max;    // Highest value of spi transmit buffer
    ubyte4 rx_buff_max;    // Highest value of spi receive buffer
    ubyte4 error_cnt;      // Number of general errors (CAN not configured, unkown commands, etc)
    ubyte4 can_err;        // Counts the number of CAN-Bus state changes to Busoff or error-passive
    ubyte4 imx_irq;        // Counts the number of generate iMX-IRQs (transitions => real IRQs at iMX)
    ubyte4 spi_tx_irq;
    ubyte4 spi_cs_irq;
    ubyte4 spi_tx_error;   // Counts the number of times the TX-FIFO has an not-aligned fill level at the end of transmission
};

extern volatile struct statistics_t stats;          // defined in main.c of XC firmware

struct spi_statistics_t {
    struct version_t ver;      // Holds the XC-firmware's version
    struct version_t imxVer;   // Holds the iMX driver version
    ubyte4 tx_dmy;             // # of transmitted dummy messages
    ubyte4 rx_dmy;             // # of received dummy messages
    ubyte4 tx_all;             // # of all transmitted messages
    ubyte4 rx_all;             // # of all received messages
    ubyte4 rx_invalid;         // # of invalid messages received (eg, data miss-alignment)
    ubyte4 rx_error;           // # of error frames received
    ubyte4 buffer_reset;       // # # of times the message buffers of the XC have been reset
    ubyte4 messages;           // # of sent spi_message objects sent;
    ubyte4 transfers;          // # of transfers
    ubyte4 spi_tx_overflow;    // # of spi messages lost due to tx overflow
    ubyte4 xc_irq;             // # of xc-IRQs executed
    ubyte4 enc_left;
    ubyte4 enc_right;
    ubyte4 enc_enter;
    ubyte4 tx_buf_warning;     // Signals that tx_buf contains more messages as can be sent in one transfer
    ubyte4 spi_lock_error;     // Increments if work_handler is called while another work_handler is running
};

struct spi_can_status_t {
    ubyte2 state;      // State (OK, busoff, error active, error passive)
    ubyte2 error_rx;   // RX error counter
    ubyte2 error_tx;   // TX error counter
    ubyte1 rx_buf;     // Number of elements in RX buffer
    ubyte1 tx_buf;     // Number of elements in TX buffer
    ubyte4 baudrate;   // Reflects the currently configured baudrate
    ubyte1 tseg1;      // Reflects the currently configured tseg1 time segment
    ubyte1 tseg2;      // Reflects the currently configured tseg2 time segment
    ubyte1 sjw;        // Reflects the currently configured synchronization jump width
    ubyte1 enabled;    // Is the module enabled?
    ubyte1 configured; // Is the module configured?
};

struct spi_encoder_status_t {
    sbyte2 absValue;
    sbyte2 relValue;
    ubyte1 button;
    ubyte1 incValue;   // See footonote [1] of command SPI_CMD_STATUS0
    ubyte1 threshold;  // See footonote [1] of command SPI_CMD_STATUS0
    ubyte1 repeatRate; // See footonote [2] of command SPI_CMD_STATUS0
    ubyte1 enabled;    // Is the module enabled?
};

// SPI_CMD_DATA for SPI_ADDR_CAN
void protocol_txCANMsg(ubyte1 _channel, ubyte4 _id, ubyte1 _dlc, const ubyte1 *const _data, struct spi_frame_t *const _spiFrame);
void protocol_rxCANMsg(const struct spi_frame_t *const _spiFrame, ubyte1 *const _channel, ubyte4 *const _id, ubyte1 *const _dlc, ubyte1 *const _data);

// SPI_CMD_CONFIG0/SPI_CMD_STATUS0 for SPI_ADDR_CAN
void protocol_txCANConfig0(ubyte1 _channel, ubyte4 _baudrate, ubyte1 _tseg1, ubyte1 _tseg2, ubyte1 _sjw, bool _enabled, bool _configured, struct spi_frame_t *const _spiFrame);
void protocol_rxCANConfig0(const struct spi_frame_t *const _spiFrame, ubyte1 *const _channel, ubyte4 *const _baudrate, ubyte1 *const _tseg1, ubyte1 *const _tseg2, ubyte1 *const _sjw, bool *const _enabled, bool *const _configured);

// SPI_CMD_STATUS0 for SPI_ADDR_CAN
void protocol_txCANStatus1(ubyte1 _channel, ubyte2 _state, ubyte2 _txCnt, ubyte2 _rxCnt, struct spi_frame_t *const _spiFrame);
void protocol_rxCANStatus1(const struct spi_frame_t *const _spiFrame, ubyte1 *const _channel, ubyte2 *const _state, ubyte2 *const _txCnt, ubyte2 *const _rxCnt);

// SPI_CMD_ERROR
void protocol_txError(ubyte1 _addr, ubyte1 _endpoint, ubyte1 _cmd, ubyte4 _error, struct spi_frame_t *const _spiFrame);
void protocol_rxError(const struct spi_frame_t *const _spiFrame, ubyte1 *_addr, ubyte1 *_endpoint, ubyte1 *_cmd, ubyte4 *_error);

// SPI_CMD_RESET for SPI_ADDR_DEVICE
void protocol_txReset(struct spi_frame_t *const _spiFrame);

// SPI_CMD_DATA for SPI_ADDR_ENCODER
void protocol_txEncoderMsg(sbyte2 _abs, sbyte2 _rel, ubyte1 _button, struct spi_frame_t *const _spiFrame);
void protocol_rxEncoderMsg(const struct spi_frame_t *const _spiFrame, sbyte2 *_abs, sbyte2 *_rel, ubyte1 *_button);

// SPI_CMD_CONFIG0 for SPI_ADDR_ENCODER
void protocol_txEncoderConfig0(ubyte1 _inc, ubyte1 _thres, ubyte1 _repeat, ubyte1 _enabled, struct spi_frame_t *const _spiFrame);
void protocol_rxEncoderConfig0(const struct spi_frame_t *const _spiFrame, ubyte1 *_inc, ubyte1 *_thres, ubyte1 *_repeat, ubyte1 *_enabled);

// SPI_CMD_CONFIG0 for SPI_ADDR_DEVICE
void protocol_txVersion(ubyte1 _major, ubyte1 _minor, ubyte1 _patch, struct spi_frame_t *const _spiFrame);
void protocol_rxVersion(const struct spi_frame_t *const _spiFrame, ubyte1 *_major, ubyte1 *_minor, ubyte1 *_patch);

// SPI_CMD_CONFIG1 for SPI_ADD_DEVICE
void protocol_txDevConfig(ubyte1 _channels, ubyte1 _rxFifo, ubyte1 _txFifo, ubyte1 _irqTime, ubyte1 _encTime, ubyte2 _rs485Time, ubyte2 _rs232Time, struct spi_frame_t *const _spiFrame);
void protocol_rxDevConfig(const struct spi_frame_t *const _spiFrame, ubyte1 *_channels, ubyte1 *_rxFifo, ubyte1 *_txFifo, ubyte1 *_irqTime, ubyte1 *_encTime, ubyte2 *_rs485Time, ubyte2 *_rs232Time);

// SPI_CMD_CONFIG0 for SPI_ADDR_DEVICE
void protocol_txLEDS(ubyte1 _green, ubyte1 _red, struct spi_frame_t *const _spiFrame);
void protocol_rxLEDS(const struct spi_frame_t *const _spiFrame, ubyte1 *_green, ubyte1 *_red);

#endif
