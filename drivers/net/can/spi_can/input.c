/*******************************************************************************
 *** Device driver for the input-device (encoder)
  ***
 *** File:          input.c
 *** Author:        Markus Ferringer
 *** Created Date:  04/02/2010
 *** Last Modified: 04/02/2010
 ******************************************************************************/
#include <linux/init.h>
#include <linux/input.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#include <linux/platform_device.h>
#include <linux/errno.h>
#include <net/rtnetlink.h>

#include "input.h"

/*******************************************************************************/
/***                                                                         ***/
/*** DEFINES AND GENERAL DECLARATIONS/DIRECTIVES                             ***/
/***                                                                         ***/
/*******************************************************************************/
#undef MOD_NAME
#define MOD_NAME "input: "

MODULE_DESCRIPTION("Encoder Input device");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Markus Ferringer <mfr@ttcontrol.com>");

/*******************************************************************************/
/***                                                                         ***/
/*** PROTOTYPE DECLARATIONS                                                  ***/
/***                                                                         ***/
/*******************************************************************************/

/*******************************************************************************/
/***                                                                         ***/
/*** VARIABLE DECLARATIONS                                                   ***/
/***                                                                         ***/
/*******************************************************************************/
static struct input_dev *encoder_dev;

/*******************************************************************************/
/***                                                                         ***/
/*** MODULE FUNCTIONS                                                        ***/
/***                                                                         ***/
/*******************************************************************************/
int input_key_event(ubyte2 _code, ubyte1 _value)
{
    // The third parameter defines whether the key is pressed (!= 0) or
    // released (== 0).
    PRINT_DBG("input_press_key() called...\n");
    input_report_key(encoder_dev, _code, _value);
    input_sync(encoder_dev);
    return 0;
}

/*******************************************************************************/
/***                                                                         ***/
/*** MODULE LOAD/UNLOAD FUNCTIONS                                            ***/
/***                                                                         ***/
/*******************************************************************************/
int input_init(void)
{
    int ret;
    PRINT_DBG("input_init() called...\n");

    encoder_dev = input_allocate_device();
    if (encoder_dev == NULL) {
        return -ENOMEM;
    }

    encoder_dev->evbit[0] = BIT_MASK(EV_KEY);
    encoder_dev->name = "TTControl: Rotary Encoder Input Device";

    encoder_dev->keybit[BIT_WORD(INPUT_KEY_CONFIRM)]  |= BIT_MASK(INPUT_KEY_CONFIRM);
    encoder_dev->keybit[BIT_WORD(INPUT_KEY_FORWARD)]  |= BIT_MASK(INPUT_KEY_FORWARD);
    encoder_dev->keybit[BIT_WORD(INPUT_KEY_BACKWARD)] |= BIT_MASK(INPUT_KEY_BACKWARD);

    ret = input_register_device(encoder_dev);
    if (ret != 0) {
        input_free_device(encoder_dev);
    }

    return ret;
}

void input_exit(void)
{
    PRINT_DBG("input_exit() called...\n");
    
    input_unregister_device(encoder_dev);
    input_free_device(encoder_dev);
    encoder_dev = NULL;
}

