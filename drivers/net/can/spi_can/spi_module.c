#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#include <linux/platform_device.h>
#include <linux/errno.h>
#include <net/rtnetlink.h>

#include "spi_can.h"
#include "input.h"
#include "spi_prot.h"
#include "spi_xc.h"

#undef MOD_NAME
#define MOD_NAME "spi_module: "

MODULE_DESCRIPTION("spi-module interface");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Markus Ferringer <mfr@ttcontrol.com>");

/*******************************************************************************/
/***                                                                         ***/
/*** MODULE LOAD/UNLOAD FUNCTIONS                                            ***/
/***                                                                         ***/
/*******************************************************************************/
static int spi_module_init(void)
{
    PRINT_DBG("spi_module_init() called...\n");
    
#ifdef SPI_CAN_DEBUG 
    gpio_request(IOMUX_TO_GPIO(MX51_PIN_GPIO1_6), "gpio1_6");
    gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_GPIO1_6), 0);
    gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_6), 0);
    gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_6), 1);
#endif
    
    input_init();
    spi_can_init();
    spi_xc_init();
    
    return 0;
}

static void spi_module_exit(void)
{
    PRINT_DBG("spi_module_exit() called...\n");
    
    spi_xc_exit();
    spi_can_exit();
    input_exit();
}

module_init(spi_module_init);
module_exit(spi_module_exit);
