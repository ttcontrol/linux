#ifndef SPI_CAN_H
#define SPI_CAN_H

#include "globals.h"
#include <linux/can.h>

#define SPI_CAN_DEFAULT_BITRATE 500000
#define SPI_CAN_DEFAULT_SJW     1
#define SPI_CAN_DEFAULT_TSEG1   5
#define SPI_CAN_DEFAULT_TSEG2   3

int  spi_can_receive(ubyte1 _dev, struct can_frame *_frame);
int  spi_can_init(void);
void spi_can_exit(void);

#endif
