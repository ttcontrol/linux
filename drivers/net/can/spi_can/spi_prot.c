/*******************************************************************************
 *** High-Level SPI-protocol to provide simple functions for reading/writing
 *** CAN-messages/IOs, etc. to the user. Uses the SPI module to send all
 *** data to the XC2000
 ***
 *** File:          spi_prot.c
 *** Author:        Markus Ferringer
 *** Created Date:  16/12/2009
 *** Last Modified: 16/12/2009
 ******************************************************************************/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#include <linux/platform_device.h>
#include <linux/errno.h>
#include <net/rtnetlink.h>
#include <linux/serial_reg.h>
#include <linux/input.h>
#include <linux/types.h>

#include <linux/can.h>
#include <linux/can/dev.h>

#include "protocol.h"
#include "input.h"
#include "spi_prot.h"
#include "spi_xc.h"

#undef MOD_NAME
#define MOD_NAME "spi_prot: "

MODULE_DESCRIPTION("spi-prot interface");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Markus Ferringer <mfr@ttcontrol.com>");

/**************************************************************************/
/***                                                                    ***/
/*** DEFINES AND GENERAL DECLARATIONS/DIRECTIVES                        ***/
/***                                                                    ***/
/**************************************************************************/

/**************************************************************************/
/***                                                                    ***/
/*** PROTOTYPE DECLARATIONS                                             ***/
/***                                                                    ***/
/**************************************************************************/

/**************************************************************************/
/***                                                                    ***/
/*** VARIABLE DECLARATIONS                                              ***/
/***                                                                    ***/
/**************************************************************************/
struct spi_can_status_t spiCanStatus[SPI_ENDP_CAN_MAX];
struct xc_device_info_t xcDeviceInfo;

static struct spi_encoder_status_t spiEncoderStatus;
static ubyte1 leds_green = 0, leds_red = 0;

/**************************************************************************/
/***                                                                    ***/
/*** GENERAL FUNCTIONS                                                  ***/
/***                                                                    ***/
/**************************************************************************/
void spi_prot_processMessage(const struct spi_frame_t *const _frame) {
    struct can_frame can_msg;
    ubyte1 dev;
    ubyte4 error;
    ubyte1 addr, endp, cmd;
    struct spi_can_status_t *status;
    sbyte2 i;
    ubyte1 *d = (ubyte1 *)_frame;
    
    PRINT_DBG("spi_prot_processMessage(CMD: %i; ADDR: %i; ENDP: %i) called...\n", _frame->command, _frame->address, _frame->endpoint);

    if ((_frame->command == SPI_CMD_NOP) || (_frame->command == SPI_CMD_IDLE)) {
        // Nothing to do in these cases
        spi_stats.rx_dmy++;
        return;
    }
    
    if ((_frame->status & 0xF8) != SPI_STATUS_MAGIC) {
        spi_stats.rx_invalid++;
        PRINT_ERR("Received invalid status 0x%02X (0x%02X%02X%02X%02X%02X%02X%02X%02X)...\n", 
                  _frame->status, d[0], d[1], d[2], d[3], d[4], d[5], d[6], d[7]);
        return;
    }
      
    switch (_frame->address) {
        case SPI_ADDR_LEDS:
            if (_frame->command == SPI_CMD_CONFIG0) {
                protocol_rxLEDS(_frame, &leds_green, &leds_red);
            }
            break;
        case SPI_ADDR_ENCODER:
            if (_frame->command == SPI_CMD_DATA) {
                protocol_rxEncoderMsg(_frame, &spiEncoderStatus.absValue, &spiEncoderStatus.relValue, &spiEncoderStatus.button);
                i = spiEncoderStatus.relValue;
                while (i != 0) {
                    if (i < 0) {
                        i++;
                        spi_stats.enc_left++;
                        input_key_event(INPUT_KEY_BACKWARD, 1);
			input_key_event(INPUT_KEY_BACKWARD, 0);
                    } else {
                        i--;
                        spi_stats.enc_right++;
                        input_key_event(INPUT_KEY_FORWARD, 1);
			input_key_event(INPUT_KEY_FORWARD, 0);
                    }
                }
                if ( (spiEncoderStatus.button == SPI_ENCODER_BTN_DOWN      ) ||
                     (spiEncoderStatus.button == SPI_ENCODER_BTN_DOWN_RPT) ) {
                    spi_stats.enc_enter++;
                    input_key_event(INPUT_KEY_CONFIRM, 1);
                } else {
                    input_key_event(INPUT_KEY_CONFIRM, 0);
                }
            } else if (_frame->command == SPI_CMD_CONFIG0) {
                protocol_rxEncoderConfig0(_frame, &spiEncoderStatus.incValue, &spiEncoderStatus.threshold, &spiEncoderStatus.repeatRate, &spiEncoderStatus.enabled);
            }
            break;
        case SPI_ADDR_CAN:
            dev = _frame->endpoint;
            if (_frame->command == SPI_CMD_DATA) {
                if (!IsAckFrame(_frame->status)) {
                    protocol_rxCANMsg(_frame, &dev, &can_msg.can_id, &can_msg.can_dlc, can_msg.data);
                    spi_can_receive(dev, &can_msg);
                }
            } else if (_frame->command == SPI_CMD_STATUS0) {
                status = &spiCanStatus[(ubyte1)dev];
                protocol_rxCANConfig0(_frame, &dev, &status->baudrate, &status->tseg1, &status->tseg2, &status->sjw, &status->enabled, &status->configured);
            } else if (_frame->command == SPI_CMD_STATUS1) {
                status = &spiCanStatus[(ubyte1)dev];
                protocol_rxCANStatus1(_frame, &dev, &status->state, &status->error_rx, &status->error_tx);
                can_msg.can_id = CAN_ERR_FLAG;
                can_msg.can_dlc = CAN_ERR_DLC;
                memset(can_msg.data, 0, 8);
//                if (status->state == SPI_CAN_ERROR_NONE) {
                    switch (status->state) {
                        case SPI_CAN_ERROR_ACTIVE:
                            break;
                        case SPI_CAN_ERROR_PASSIVE:
                            can_msg.can_id |= CAN_ERR_CRTL;
                            can_msg.data[1] = (status->error_tx > status->error_tx) ?
                                CAN_ERR_CRTL_TX_PASSIVE :
                                CAN_ERR_CRTL_RX_PASSIVE;
                            break;
                        case SPI_CAN_ERROR_BUSOFF:
                            can_msg.can_id |= CAN_ERR_BUSOFF;
                            break;
                        case SPI_CAN_ERROR_RX_OF:
                            can_msg.can_id |= CAN_ERR_CRTL;
                            can_msg.data[1] = CAN_ERR_CRTL_RX_OVERFLOW;
                            break;
                        case SPI_CAN_ERROR_TX_OF: 
                            can_msg.can_id |= CAN_ERR_CRTL;
                            can_msg.data[1] = CAN_ERR_CRTL_TX_OVERFLOW;
                            break;
                        case SPI_CAN_ERROR_UNKNOWN: // fall through
                        default:
                            // Just issue some general controller error
                            can_msg.can_id |= CAN_ERR_CRTL;
                            break;
                    }
                    spi_can_receive(dev, &can_msg);
//                }
            } else if (_frame->command == SPI_CMD_ERROR) {
                protocol_rxError(_frame, &addr, &endp, &cmd, &error);
                switch (error) {
                    case SPI_ERROR_BUSOFF:          // fall through
                    case SPI_ERROR_NOT_CONFIGURED:  // fall through
                    case SPI_ERROR_NOT_ENABLED:
                        spi_prot_CANConfigure(dev, 
                                              spiCanStatus[dev].baudrate, 
                                              spiCanStatus[dev].tseg1,
                                              spiCanStatus[dev].tseg2, 
                                              spiCanStatus[dev].sjw, 
                                              TRUE);
                        break;
                    default:
                        break;
                }
            }
            break;
        case SPI_ADDR_DEVICE:
            if (_frame->command == SPI_CMD_ERROR) {
                ubyte4 error_id = ((ubyte4)_frame->data[1] << 24) |
                                  ((ubyte4)_frame->data[2] << 16) |
                                  ((ubyte4)_frame->data[3] <<  8) |
                                  ((ubyte4)_frame->data[4] <<  0);
                spi_stats.rx_error++;
                if (error_id == SPI_ERROR_BUF_RESET) {
                    spi_stats.buffer_reset++;
                }
                PRINT_ERR("Received Error-Frame %i!\n", error_id);
            } else if (_frame->command == SPI_CMD_STATUS0) {
                protocol_rxVersion(_frame, (ubyte1 *)&spi_stats.ver.major, (ubyte1 *)&spi_stats.ver.minor, (ubyte1 *)&spi_stats.ver.patch);
            } else if (_frame->command == SPI_CMD_STATUS1) {
                ubyte1 dmy1, dmy2;
                ubyte2 dmy3, dmy4;
                protocol_rxDevConfig(_frame, 
                                     &xcDeviceInfo.activeChannels, 
                                     &xcDeviceInfo.rxFifos,
                                     &xcDeviceInfo.txFifos, 
                                     &dmy1,
                                     &dmy2,
                                     &dmy3,
                                     &dmy4);
                // convert to us
                xcDeviceInfo.irqTime = dmy1 * 500;
                xcDeviceInfo.encTime = dmy2 * 1000;
                xcDeviceInfo.rs485Time = dmy3 * 100;
                xcDeviceInfo.rs232Time = dmy4 * 100;
            }
            break;
        case SPI_ADDR_RES:     // Fall-through
        case (~SPI_ADDR_RES) & 0x1F:
            //Count the number of empty messages received, just for statistical reasons
            break;
        default:
            //PRINT_ERR("Received invalid command (0x%02X%02X%02X%02X%02X%02X%02X%02X)...\n", d[0], d[1], d[2], d[3], d[4], d[5], d[6], d[7]);
            break;
    }
}


static int spiWrite(const struct spi_frame_t *const _frame)
{
    spi_xc_read_write((struct spi_frame_t *)_frame);

    return SPI_OK;
}

/**************************************************************************/
/***                                                                    ***/
/*** MODULE FUNCTIONS                                                   ***/
/***                                                                    ***/
/**************************************************************************/

/**************************************************************************
 *
 * \brief Sends a CAN-message to the specified CAN-device over SPI
 *
 * \param _dev    Specifies the target-CAN-device
 * \param _frame  Holds the message that is to be sent over _dev
 *
 * \return Returns on of the SPI_ERR_* defines in case of error, or
 *         SPI_OK in case of success. Special return values are listed below.
 * \retval SPI_ERR_FULL The write-buffer is full. The message is not accepted.
 ***************************************************************************/
int spi_prot_CANWrite(ubyte1 _dev, const struct can_frame *const _frame) {
    int ret;
    struct spi_frame_t spiFrame;
    PRINT_DBG("spi_prot_CANWrite() called...\n");

    // Make range-check for _dev parameter
    if (_dev >= SPI_ENDP_CAN_MAX) {
        return SPI_ERR_INVALIDDEV;
    }

    // Make validity-check for _frame parameter
    if (_frame == NULL) {
        return SPI_ERR_NULL_PTR;
    }

    // Assemble message
    protocol_txCANMsg(_dev, _frame->can_id, _frame->can_dlc, _frame->data, &spiFrame);

    // Forward message to SPI-interface
    ret = spiWrite(&spiFrame);

    return ret;
}

/*************************************************************************
 *
 * \brief Configures the CAN-device specified. Notice that configuration is
 *        also possible during operation, although messages already in the
 *        send-buffer might be lost.
 *
 * \param _dev      Specifies the target-CAN-device
 * \param _baudrate Defines the baudrate in bit/second
 * \param _tseg1    Defines the number of time-quanta for tseg1
 * \param _tseg2    Defines the number of time-quanta for tseg2
 * \param _sjw      Defines the maximum synchronization jump width
 *
 * \return Returns on of the SPI_ERR_* defines in case of error, or
 *         SPI_OK in case of success. Special return values are listed below.
 ***************************************************************************/
int spi_prot_CANConfigure(ubyte1 _dev, ubyte4 _baudrate, ubyte1 _tseg1, ubyte1 _tseg2, ubyte1 _sjw, bool _enabled) {
    int ret;
    struct spi_frame_t spiFrame;
    PRINT_DBG("spi_prot_CANConfigure() called...\n");

    // Make range-check for _dev parameter
    if (_dev >= SPI_ENDP_CAN_MAX) {
        return SPI_ERR_INVALIDDEV;
    }

    spiCanStatus[_dev].baudrate = _baudrate;
    spiCanStatus[_dev].tseg1 = _tseg1;
    spiCanStatus[_dev].tseg2 = _tseg2;
    spiCanStatus[_dev].sjw = _sjw;
    spiCanStatus[_dev].state = SPI_CAN_OK;
    spiCanStatus[_dev].enabled = _enabled;
    spiCanStatus[_dev].configured = FALSE;  // set "configured-pending"-Flag

    // Assemble message
    protocol_txCANConfig0(_dev, _baudrate, _tseg1, _tseg2, _sjw, _enabled, 0, &spiFrame);
    // Forward message to SPI-interface
    ret = spiWrite(&spiFrame);
    
    // Also request an update for the xc device configuration (changes after each reconfig)
    protocol_txDevConfig(0, 0, 0, 0, 0, 0, 0, &spiFrame);
    ret = spiWrite(&spiFrame);

    return ret;
}

int  spi_prot_EncoderConfigure(ubyte1 _inc, ubyte1 _thres, ubyte1 _repeat, ubyte1 _enabled) {
    int ret;
    struct spi_frame_t spiFrame;
    
    PRINT_DBG("spi_prot_EncoderConfigure() called...\n");
    
    protocol_txEncoderConfig0(_inc, _thres, _repeat, _enabled, &spiFrame);
    ret = spiWrite(&spiFrame);
    return ret;
}

/***********************************************************************
 *
 * \brief Requests the status of the given CAN-device. A request is sent, and
 *        it is actively waited for a response! Call is blocking!
 *
 * \param _dev      Specifies the target-CAN-device
 * \param _status   Pointer to the status-structure.
 *
 * \return Returns on of the SPI_ERR_* defines in case of error, or
 *         SPI_OK in case of success. Special return values are listed below.
 ***************************************************************************/
int spi_prot_CANStatus(ubyte1 _dev, struct spi_can_status_t *const _status) {
    PRINT_DBG("spi_prot_CANStatus() called...\n");
    
    // Make range-check for _dev parameter
    if (_dev >= SPI_ENDP_CAN_MAX) {
        return SPI_ERR_INVALIDDEV;
    }
    
    if (_status == NULL) {
        return SPI_ERR_NULL_PTR;
    }
    
    *_status = spiCanStatus[_dev];
    //memcpy(_status, &spiStatus[_dev], sizeof(struct spi_can_status_t));
    
    return SPI_OK;
}

/****************************************************************************
 *
 * \brief Performs a reset of the target device. All settings will be lost! The
 *        device needs to be reconfigured after reset.
 *
 * \return Returns on of the SPI_ERR_* defines in case of error, or
 *         SPI_OK in case of success. Special return values are listed below.
 ***************************************************************************/
int spi_prot_ResetAndClear(void) {
    struct spi_frame_t spiFrame;
    ubyte1 i;
    PRINT_DBG("spi_prot_ResetAndClear() called...\n");

    for (i = 0; i < SPI_ENDP_CAN_MAX; ++i) {
        spiCanStatus[i].state = SPI_CAN_DISABLED;
        spiCanStatus[i].enabled = FALSE;
        spiCanStatus[i].configured = FALSE;
    }

    spi_prot_Reset();
    return SPI_OK;
}

int spi_prot_Reset(void) {
    struct spi_frame_t spiFrame;
    ubyte1 i;
    PRINT_DBG("spi_prot_Reset() called...\n");

    protocol_txReset(&spiFrame);
    spiWrite(&spiFrame);
    return SPI_OK;
}

int spi_prot_LED_SetBrightness(ubyte1 _green, ubyte1 _red) {
    struct spi_frame_t spiFrame;
    protocol_txLEDS(_green, _red, &spiFrame);
    spiWrite(&spiFrame);
    return SPI_OK;
}

int spi_prot_LED_GetBrightness(ubyte1 *_green, ubyte1 *_red) {
    *_green = leds_green;
    *_red = leds_red;
    return SPI_OK;
}

int spi_prot_RequestVersion(void) {
    struct spi_frame_t spiFrame;
    
    PRINT_DBG("spi_prot_GetVersion() called...\n");
    
    protocol_txVersion(0, 0, 0, &spiFrame);
    spiWrite(&spiFrame);
    return SPI_OK;
}

/**************************************************************************/
/***                                                                    ***/
/*** MODULE PROBE/REMOVE FUNCTIONS                                      ***/
/***                                                                    ***/
/**************************************************************************/
int spi_prot_probe(void)
{
    int ret = SPI_OK;
    ubyte1 i;
    PRINT_DBG("spi_prot_probe() called...\n");
    for (i = 0; i < SPI_ENDP_CAN_MAX; ++i) {
        memset(&spiCanStatus[i], 0, sizeof(struct spi_can_status_t));
        spiCanStatus[i].baudrate = SPI_CAN_DEFAULT_BITRATE;
        spiCanStatus[i].configured = FALSE;
        spiCanStatus[i].enabled = FALSE;
        spiCanStatus[i].sjw = SPI_CAN_DEFAULT_SJW;
        spiCanStatus[i].tseg1 = SPI_CAN_DEFAULT_TSEG1;
        spiCanStatus[i].tseg2 = SPI_CAN_DEFAULT_TSEG2;
        spiCanStatus[i].state = SPI_CAN_DISABLED;
    }
    memset(&xcDeviceInfo, 0, sizeof(xcDeviceInfo));
    spi_prot_EncoderConfigure(1, 1, 0, TRUE);
    spi_prot_RequestVersion();

    return ret;
}

void spi_prot_remove(void)
{
    ubyte1 i;

    PRINT_DBG("spi_prot_remove() called...\n");

    for (i = 0; i < SPI_ENDP_CAN_MAX; ++i) {
        spiCanStatus[i].configured = FALSE;
        spiCanStatus[i].enabled = FALSE;
        spiCanStatus[i].state = SPI_CAN_DISABLED;
    }
    spi_prot_EncoderConfigure(1, 1, 0, FALSE);
}

void spi_prot_suspend(void)
{
    
    PRINT_DBG("spi_prot_suspend() called...\n");

}

void spi_prot_resume(void)
{
    int i;

    PRINT_DBG("spi_prot_resume() called...\n");

    for (i = 0; i < SPI_ENDP_CAN_MAX; ++i)
    {
        if (spiCanStatus[i].enabled == TRUE)
        {
            spi_prot_CANConfigure(i
                                , spiCanStatus[i].baudrate
                                , spiCanStatus[i].tseg1
                                , spiCanStatus[i].tseg2
                                , spiCanStatus[i].sjw
                                , spiCanStatus[i].enabled);
        }
    }

    spi_prot_LED_SetBrightness(leds_green, leds_red);

    spi_prot_EncoderConfigure(1, 1, 0, TRUE);
}
