/*******************************************************************************
 *** Functions for dealing with the procfs file system.
 ***
 *** File:          procfs.c
 *** Author:        Markus Ferringer
 *** Created Date:  29/01/2010
 *** Last Modified: 29/01/2010
 ******************************************************************************/
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

#include "procfs.h"
#include "spi_can.h"
#include "spi_prot.h"
#include "protocol.h"
#include "spi_xc.h"

#undef MOD_NAME
#define MOD_NAME "procfs: "

/*******************************************************************************/
/***                                                                         ***/
/*** DEFINES                                                                 ***/
/***                                                                         ***/
/*******************************************************************************/
#define BUFFER_SIZE 256

/*******************************************************************************/
/***                                                                         ***/
/*** VARIABLE DECLARATIONS, LOCAL TYPES                                      ***/
/***                                                                         ***/
/*******************************************************************************/
int procfs_bittiming_read(char *buffer, char **buffer_location, off_t offset, int buffer_length, int *eof, void *data);
int procfs_bittiming_write(struct file *file, const char *buffer, unsigned long count, void *data);
int procfs_filtering_read(char *buffer, char **buffer_location, off_t offset, int buffer_length, int *eof, void *data);
int procfs_filtering_write(struct file *file, const char *buffer, unsigned long count, void *data);
int procfs_stats_read(char *buffer, char **buffer_location, off_t offset, int buffer_length, int *eof, void *data);
int procfs_info_read(char *buffer, char **buffer_location, off_t offset, int buffer_length, int *eof, void *data);
int procfs_led_write(struct file *file, const char *buffer, unsigned long count, void *data);
int procfs_led_read(char *buffer, char **buffer_location, off_t offset, int buffer_length, int *eof, void *data);
static struct proc_dir_entry *procfs_timing[SPI_ENDP_CAN_MAX];
static struct proc_dir_entry *procfs_filter[SPI_ENDP_CAN_MAX];
static struct proc_dir_entry *procfs_folder[SPI_ENDP_CAN_MAX];
static struct proc_dir_entry *procfs_can_root = NULL;
static struct proc_dir_entry *procfs_stats;
static struct proc_dir_entry *procfs_info;
static struct proc_dir_entry *procfs_led_root = NULL;
static struct proc_dir_entry *procfs_led_a_value;
static struct proc_dir_entry *procfs_led_b_value;

static int led_a_value = 0;
static int led_b_value = 0;

/*******************************************************************************/
/***                                                                         ***/
/*** HELPER FUNCTION                                                         ***/
/***                                                                         ***/
/*******************************************************************************/
static bool isDigit(char _c)
{
    if ((_c >= '0') && (_c <= '9')) {
        return TRUE;
    }
    return FALSE;
}

static bool isSpace(char _c)
{
    if ((_c == ' ')  || (_c == '\n') || (_c == '\r') || (_c == '\t') || (_c == '\0'))
    {
        return TRUE;
    }
    return FALSE;
}

static int toInt(char _c)
{
    if (!isDigit(_c)) {
        return 0;
    }
    return (int)(_c - '0');
}

/*******************************************************************************/
/***                                                                         ***/
/*** GENERAL FUNCTIONS                                                       ***/
/***                                                                         ***/
/*******************************************************************************/

void procfs_create_root(void)
{
    // Create static CAN entries
    procfs_can_root = proc_mkdir(PROCFS_CAN_ROOT_NAME, NULL);
    
    procfs_stats = create_proc_entry(PROCFS_STATS_NAME, S_IRWXU | S_IRWXG | S_IRWXO, procfs_can_root);
    procfs_stats->read_proc = procfs_stats_read;
    procfs_stats->write_proc = NULL;
    procfs_stats->mode = S_IFREG;
    procfs_stats->uid = 0;
    procfs_stats->gid = 0;
    procfs_stats->size = 0;
    
    procfs_info = create_proc_entry(PROCFS_INFO_NAME, S_IRWXU | S_IRWXG | S_IRWXO, procfs_can_root);
    procfs_info->read_proc = procfs_info_read;
    procfs_info->write_proc = NULL;
    procfs_info->mode = S_IFREG;
    procfs_info->uid = 0;
    procfs_info->gid = 0;
    procfs_info->size = 0;

    // Create static LED entries
    procfs_led_root = proc_mkdir(PROCFS_LED_ROOT_NAME, NULL);

    procfs_led_a_value = create_proc_entry(PROCFS_LED_A_VALUE_NAME, S_IRWXU | S_IRWXG | S_IRWXO, procfs_led_root);
    procfs_led_a_value->read_proc = procfs_led_read;
    procfs_led_a_value->write_proc = procfs_led_write;
    procfs_led_a_value->data = &led_a_value;
    procfs_led_a_value->mode = S_IFREG;
    procfs_led_a_value->uid = 0;
    procfs_led_a_value->gid = 0;
    procfs_led_a_value->size = 0;

    procfs_led_b_value = create_proc_entry(PROCFS_LED_B_VALUE_NAME, S_IRWXU | S_IRWXG | S_IRWXO, procfs_led_root);
    procfs_led_b_value->read_proc = procfs_led_read;
    procfs_led_b_value->write_proc = procfs_led_write;
    procfs_led_b_value->data = &led_b_value;
    procfs_led_b_value->mode = S_IFREG;
    procfs_led_b_value->uid = 0;
    procfs_led_b_value->gid = 0;
    procfs_led_b_value->size = 0;

    spi_prot_LED_GetBrightness((ubyte1 *)&led_a_value, (ubyte1 *)&led_b_value);
}

void procfs_remove_root(void)
{
    // Remove static LED entries
    remove_proc_entry(procfs_led_a_value->name, procfs_led_root);
    remove_proc_entry(procfs_led_b_value->name, procfs_led_root);
    remove_proc_entry(PROCFS_LED_ROOT_NAME, NULL);
    procfs_led_root = NULL;

    // Remove static CAN entries
    remove_proc_entry(procfs_stats->name, procfs_can_root);
    remove_proc_entry(procfs_info->name, procfs_can_root);
    remove_proc_entry(PROCFS_CAN_ROOT_NAME, NULL);
    procfs_can_root = NULL;
}

void procfs_add_device(ubyte1 _dev)
{
    char name[8];

    if ((_dev > SPI_ENDP_CAN_MAX) || (procfs_can_root == NULL)) {
        return;
    }

    sprintf(name, "can%i", _dev);
    procfs_folder[_dev] = proc_mkdir(name, procfs_can_root);

    procfs_timing[_dev] = create_proc_entry(PROCFS_BITTIMING_NAME, S_IRWXU | S_IRWXG | S_IRWXO, procfs_folder[_dev]);
    procfs_timing[_dev]->read_proc = procfs_bittiming_read;
    procfs_timing[_dev]->write_proc = procfs_bittiming_write;
    procfs_timing[_dev]->mode = S_IFREG;
    procfs_timing[_dev]->uid = 0;
    procfs_timing[_dev]->gid = 0;
    procfs_timing[_dev]->size = 0;
    procfs_timing[_dev]->data = kmalloc(sizeof(ubyte1), GFP_KERNEL);
    *((ubyte1 *)procfs_timing[_dev]->data) = _dev;

    procfs_filter[_dev] = create_proc_entry(PROCFS_FILTERING_NAME, S_IRWXU | S_IRWXG | S_IRWXO, procfs_folder[_dev]);
    procfs_filter[_dev]->read_proc = procfs_filtering_read;
    procfs_filter[_dev]->write_proc = procfs_filtering_write;
    procfs_filter[_dev]->mode = S_IFREG;
    procfs_filter[_dev]->uid = 0;
    procfs_filter[_dev]->gid = 0;
    procfs_filter[_dev]->size = 0;
    procfs_filter[_dev]->data = kmalloc(sizeof(ubyte1), GFP_KERNEL);
    *((ubyte1 *)procfs_filter[_dev]->data) = _dev;
}

void procfs_remove_device(ubyte1 _dev)
{
    if ((_dev > SPI_ENDP_CAN_MAX) || (procfs_can_root == NULL)) {
        return;
    }

    kfree(procfs_timing[_dev]->data);
    kfree(procfs_filter[_dev]->data);
    remove_proc_entry(procfs_timing[_dev]->name, procfs_folder[_dev]);
    remove_proc_entry(procfs_filter[_dev]->name, procfs_folder[_dev]);
    remove_proc_entry(procfs_folder[_dev]->name, procfs_can_root);
    procfs_timing[_dev] = NULL;
    procfs_filter[_dev] = NULL;
}

int procfs_stats_read(char *buffer, char **buffer_location, off_t offset, int buffer_length, int *eof, void *data) {
    int ret;
    
    PRINT_DBG("procfs_stats_read() called...\n");
    
    sprintf(buffer, "\n*** SPI-XC Interface DEBUG INFORMATION and STATISTICS\n"
                    "*** XC-Firmware-Version V%i.%i.%i \n"
                    "*** iMX-Driver-Version  V%i.%i.%i \n"
                    "rx_all:          %08i\n"
                    "tx_all:          %08i\n"
                    "rx_dmy:          %08i\n"
                    "tx_dmy:          %08i\n"
                    "rx_invalid:      %08i\n" 
                    "rx_error:        %08i\n"
                    "XC_buffer_reset: %08i\n"
                    "messages:        %08i\n"
                    "transfers:       %08i\n"
                    "tx_overflow:     %08i\n"
                    "xc_irq:          %08i\n"
                    "enc_left:        %08i\n"
                    "enc_right:       %08i\n"
                    "enc_enter:       %08i\n"
                    "tx_buf_warning:  %08i\n"
                    "spi_lock_error:  %08i\n\n",
                    spi_stats.ver.major,       spi_stats.ver.minor,       spi_stats.ver.patch,
                    spi_stats.imxVer.major,    spi_stats.imxVer.minor,    spi_stats.imxVer.patch,
                    spi_stats.rx_all,          spi_stats.tx_all, 
                    spi_stats.rx_dmy,          spi_stats.tx_dmy, 
                    spi_stats.rx_invalid,      spi_stats.rx_error,        spi_stats.buffer_reset,
                    spi_stats.messages,        spi_stats.transfers,       spi_stats.spi_tx_overflow,
                    spi_stats.xc_irq,          spi_stats.enc_left,
                    spi_stats.enc_right,       spi_stats.enc_enter,
                    spi_stats.tx_buf_warning,  spi_stats.spi_lock_error);
    ret = strlen(buffer);
    return ret;
}

int procfs_info_read(char *buffer, char **buffer_location, off_t offset, int buffer_length, int *eof, void *data) {
    int ret;
    
    PRINT_DBG("procfs_info_read() called...\n");
    
    sprintf(buffer, "\n*** SPI-XC Interface DEVICE INFORMATION\n"
                    "*** XC-Firmware-Version V%i.%i.%i \n"
                    "*** iMX-Driver-Version  V%i.%i.%i \n"
                    "Active CAN Channels: %i\n"
                    "RX-FIFO Size: %i\n"
                    "TX-FIFO Size: %i\n"
                    "IRQ Period: %i us\n"
                    "Encoder Task Period: %i us\n"
                    "RS485 Task Period: %i us\n"
                    "RS232 Task Period: %i us\n\n",
                    spi_stats.ver.major,         spi_stats.ver.minor,       spi_stats.ver.patch,
                    spi_stats.imxVer.major,      spi_stats.imxVer.minor,    spi_stats.imxVer.patch,
                    xcDeviceInfo.activeChannels, xcDeviceInfo.rxFifos,       xcDeviceInfo.txFifos,
                    xcDeviceInfo.irqTime,
                    xcDeviceInfo.encTime, 
                    xcDeviceInfo.rs485Time,
                    xcDeviceInfo.rs232Time);
    ret = strlen(buffer);
    return ret;
}

int procfs_bittiming_read(char *buffer, char **buffer_location, off_t offset, int buffer_length, int *eof, void *data)
{
    int ret;
    struct spi_can_status_t stat;
    ubyte1 index = *((ubyte1 *)data);

    PRINT_DBG("procfs_bittiming_read() called...\n");

    if ((offset > 0) || (index > SPI_ENDP_CAN_MAX)) {
        // As we don't need to return much data, we only allow one single call which
        // returns everything at once.
        ret = 0;
        *eof = 1;
    } else {
        // Get current status from the spi_prot module
        spi_prot_CANStatus(index, &stat);
        // Although not a good practice, we just assume that buffer is big-enough
        // for our purposes. According to documentation, it's common practice to do so.
        sprintf(buffer, "baudrate: %u\ntseg1: %u\ntseg2: %u\nsjw: %u\n", stat.baudrate, stat.tseg1, stat.tseg2, stat.sjw);

        ret = strlen(buffer);
    }
    return ret;
}

/**
 * Input values must be provided in the following oder: BAUDRATE TSEG1 TSEG2 SJW
 * BAUDRATE ... CAN-Baudrate in bit/s, or 0 if current value shall be preserved
 * TSEG1 ... 3 - 16, or 0 if current value shall be preserved
 * TSEG2 ... 2 - 8, or 0 if current value shall be preserved
 * SJW ... 1 - 4, or 0 if current value shall be preserved
 *
 * All characters other than digits and whitespaces (space, newline, tab, etc) are ignored.
 * The values themselves must be directly surrounded by whitespaces
 * E.g., VALID input could be
 * 250000 5 3 1
 * Bitrate: 250000 tseg1: 5 tseg2: 3 sjw: 2
 *
 * E.g., INVALID input would be
 * Bitrate: 250000 tseg1:5 tseg2: 3sjw: 2
 * (here, tseg1 and tseg2 values are not surrounded by whitespace)
 */
int procfs_bittiming_write(struct file *file, const char *buffer, unsigned long count, void *data)
{
    ubyte1 index = *((ubyte1 *)data);
    char input[BUFFER_SIZE];
    char c;
    bool lastSpace = TRUE;
    ubyte1 wordCnt = 0;
    ubyte2 i;
    ubyte4 baudrate = 0;
    ubyte1 tseg1 = 0, tseg2 = 0, sjw = 0;
    struct spi_can_status_t stat;

    PRINT_DBG("procfs_bittiming_write() called...\n");

    if (count > BUFFER_SIZE) {
        return -ENOMEM;
    }

    if (copy_from_user(input, buffer, count)) {
        return -EFAULT;
    }

    i = 0;
    c = input[0];
    while ((c != '\0') && (i < count)) {
        if ((lastSpace) && (isDigit(c))) {
            wordCnt++;
            while ((isDigit(c)) && (i < count)) {
                switch (wordCnt) {
                case 1: baudrate = baudrate * 10 + toInt(c); break;
                case 2: tseg1 = tseg1 * 10 + toInt(c); break;
                case 3: tseg2 = tseg2 * 10 + toInt(c); break;
                case 4: sjw = sjw * 10 + toInt(c); break;
                default: break;
                }
                i++;
                c = input[i];
            }
        }
        if (isSpace(c)) {
            lastSpace = TRUE;
        } else {
            lastSpace = FALSE;
        }
        i++;
        c = input[i];
    }

    if (wordCnt != 4) {
        return -EINVAL;
    }

    if ( ( (tseg1 != 0) && ((tseg1 < 3) || (tseg1 > 16)) ) ||
         ( (tseg2 != 0) && ((tseg2 < 2) || (tseg2 >  8)) ) ||
         ( sjw > 4 ) ) {
        return -EINVAL;
    }

    spi_prot_CANStatus(index, &stat);
    if (baudrate != 0) stat.baudrate = baudrate;
    if (tseg1 != 0) stat.tseg1 = tseg1;
    if (tseg2 != 0) stat.tseg2 = tseg2;
    if (sjw != 0) stat.sjw = sjw;

    spi_prot_CANConfigure(index, stat.baudrate, stat.tseg1, stat.tseg2, stat.sjw, TRUE);

    return count;
}

int procfs_filtering_read(char *buffer, char **buffer_location, off_t offset, int buffer_length, int *eof, void *data)
{
    sprintf(buffer, "Not supported (yet)...\n");
    return strlen(buffer);
}

int procfs_filtering_write(struct file *file, const char *buffer, unsigned long count, void *data)
{
    return -EPERM;
}

int procfs_led_read(char *buffer, char **buffer_location, off_t offset, int buffer_length, int *eof, void *data)
{
    // spi_prot_LED_GetBrightness(&green, &red);
    sprintf(buffer, "%d\n", *(int*)data);
    return strlen(buffer);
}

int procfs_led_write(struct file *file, const char *buffer, unsigned long count, void *data)
{
/*    ubyte1 green = 0, red = 0;
    ubyte2 i = 0;
    ubyte1 state = 0;
    // state 0: from beginning till first digit
    // state 1: from first digit till next non-digit (GREEN)
    // state 2: from first non-digit of state1 till next digit
    // state 3: till last digit (RED)

    // Loop through the buffer until a digit is found, then parse GREEN until a non-digit is found.
    // Then parse RED until a non-digit is found
    while ((buffer[i] != '\0') && (i < count)) {
        if ((state == 0) && isDigit(buffer[i])) {
            state = 1;
        }
        if ((state == 1) && isDigit(buffer[i])) {
            green = green * 10 + (buffer[i] - '0');
        }
        if ((state == 1) && !isDigit(buffer[i])) {
            state = 2;
        }
        if ((state == 2) && isDigit(buffer[i])) {
            state = 3;
        }
        if ((state == 3) && isDigit(buffer[i])) {
            red = red * 10 + (buffer[i] - '0');
        }
        if ((state == 3) && !isDigit(buffer[i])) {
            break;
        }
        ++i;
    }*/

    if (sscanf(buffer, "%d", (int*)data))
    {
        spi_prot_LED_SetBrightness((ubyte1)led_a_value, (ubyte1)led_b_value);
    }

    return count;
}
