/*
 * Copyright 2008-2009 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

/*!
 * @file drivers/hwmon/bh1780gli.c
 *
 * @brief bh1780gli ambient light sensor Driver
 *
 * @ingroup
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/hwmon.h>
#include <linux/hwmon-sysfs.h>
#include <linux/ctype.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/regulator/consumer.h>
#include <linux/workqueue.h>
#include <mach/hardware.h>

struct bh1780_data {
	struct i2c_client *client;
	struct device *hwmon_dev;
	unsigned char enable;
};

static struct i2c_client *bh1780_client;

#define WQ_IDLE -3
#define WQ_RUNNING -2
#define LUX_ERROR -1

static void read_lux_wq(struct work_struct *work);
DECLARE_WORK(read_lux_work, read_lux_wq);
static atomic_t lux;

/*
 * This function do the bh1780 config and enable.
 */
static int bh1780_on(void)
{
	struct bh1780_data *data = i2c_get_clientdata(bh1780_client);

	if (data->enable)
		return -ENODEV;

	if (i2c_smbus_write_byte_data(bh1780_client, 0x80, 0x03))
		return -ENODEV;

	if (i2c_smbus_write_byte(bh1780_client, 0x8c))
		return -ENODEV;

	data->enable = 1;

	pr_info("bh1780 on\n");
	return 0;
}

/*!
 * This function shut down the bh1780.
 */
static int bh1780_off(void)
{
	struct bh1780_data *data = i2c_get_clientdata(bh1780_client);

	if (!data->enable)
		return 0;

	if (i2c_smbus_write_byte_data(bh1780_client, 0x80, 0x03))
		return -ENODEV;

	data->enable = 0;

	pr_info("bh1780 off\n");
	return 0;
}

/*!
 * This function read the bh1780 lux registers and convert them to the lux
 * value.
 *
 * @output buffer	this param holds the lux value, when =-1, read fail
 *
 * @return 0
 */
static int bh1780_read_lux(struct bh1780_data *data)
{
	char buf[2];

	if (!data->enable)
		goto err;

	if (i2c_master_recv(bh1780_client,buf,2) < 0)
		goto err;

	msleep(150);

	if (i2c_master_recv(bh1780_client,buf,2) < 0)
		goto err;

	return (buf[1]<<8)+buf[0];;

err:
	return -1;
}

static void read_lux_wq(struct work_struct *work)
{
	struct bh1780_data *client_data = i2c_get_clientdata(bh1780_client);

	atomic_set(&lux, bh1780_read_lux(client_data));
	sysfs_notify(&client_data->client->dev.kobj, NULL, "lux");
}

static ssize_t ls_enable(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	char *endp;
	int enable = simple_strtoul(buf, &endp, 0);
	size_t size = endp - buf;

	if (*endp && isspace(*endp))
		size++;
	if (size != count)
		return -EINVAL;

	if (enable == 1) {
		if (bh1780_on())
			pr_info("device open fail\n");
	}
	if (enable == 0) {
		if (bh1780_off())
			pr_info("device powerdown fail\n");
	}

	return count;
}

static SENSOR_DEVICE_ATTR(enable, S_IWUGO, NULL, ls_enable, 0);

static ssize_t show_lux(struct device *dev, struct device_attribute *attr,
			char *buf)
{
	ssize_t buf_size;
        int val;

	val = atomic_read (&lux);

	if (val == WQ_IDLE)
	{
		*buf = '\0';
		schedule_work(&read_lux_work);
		atomic_set(&lux, WQ_RUNNING);
		return 0;
	}
	else if (val == WQ_RUNNING)
	{
		*buf = '\0';
		return 0;
	}
	else
	{
		buf_size = snprintf(buf, PAGE_SIZE, "%u\n", val);
		atomic_set(&lux, WQ_IDLE);
		return buf_size;
	}
}

static SENSOR_DEVICE_ATTR(lux, S_IRUGO, show_lux, NULL, 0);

static int bh1780_i2c_probe(struct i2c_client *client,
			      const struct i2c_device_id *did)
{
	int err = 0;
	struct bh1780_data *data;

	bh1780_client = client;
	data = kzalloc(sizeof(struct bh1780_data), GFP_KERNEL);
	if (data == NULL) {
		err = -ENOMEM;
		goto exit1;
	}

	i2c_set_clientdata(client, data);
	data->client = client;
	
	data->enable = 0;

	if (bh1780_on())
		err = -ENODEV;

	msleep(10);

	if (i2c_smbus_write_byte(bh1780_client, 0x8a))
		err = -ENODEV;

	if (bh1780_read_lux(data) != 0x0181)
		err = -ENODEV;
	
	if (i2c_smbus_write_byte(bh1780_client, 0x8c))
		err = -ENODEV;

	if (err)
		goto exit2;

	err = device_create_file(&client->dev, &sensor_dev_attr_enable.dev_attr);
	if (err)
		goto exit2;

	err = device_create_file(&client->dev, &sensor_dev_attr_lux.dev_attr);
	if (err)
		goto exit_remove1;

	/* Register sysfs hooks */
	data->hwmon_dev = hwmon_device_register(&client->dev);
	if (IS_ERR(data->hwmon_dev)) {
		err = PTR_ERR(data->hwmon_dev);
		goto exit_remove2;
	}

	return 0;

exit_remove2:
	device_remove_file(&client->dev, &sensor_dev_attr_lux.dev_attr);
exit_remove1:
	device_remove_file(&client->dev, &sensor_dev_attr_enable.dev_attr);
exit2:
	kfree(data);
exit1:
	bh1780_client = NULL;
	return err;
}

static int bh1780_i2c_remove(struct i2c_client *client)
{
	struct bh1780_data *data = i2c_get_clientdata(client);
	
	bh1780_off();

	hwmon_device_unregister(data->hwmon_dev);
	device_remove_file(&client->dev, &sensor_dev_attr_enable.dev_attr);
	device_remove_file(&client->dev, &sensor_dev_attr_lux.dev_attr);
	kfree(data);
	return 0;
}

static int bh1780_suspend(struct i2c_client *client, pm_message_t message)
{
	struct bh1780_data *data = i2c_get_clientdata(client);

	if (!data->enable)
		goto exit;

	if (bh1780_off())
		goto err;
exit:
	return 0;
err:
	pr_err("bh1780: suspend failed");
	return -ENODEV;
}

static int bh1780_resume(struct i2c_client *client)
{
	struct bh1780_data *data = i2c_get_clientdata(client);

	if (data->enable)
		goto exit;

	if (bh1780_on())
		goto err;
	
exit:
	return 0;
err:
	pr_err("bh1780: resume failed");
	return -ENODEV;
}

static const struct i2c_device_id bh1780_id[] = {
	{"bh1780", 0},
	{}
};
MODULE_DEVICE_TABLE(i2c, bh1780_id);

static struct i2c_driver bh1780_driver = {
	.driver = {
		   .name = "bh1780",
		   },
	.probe = bh1780_i2c_probe,
	.remove = bh1780_i2c_remove,
	.suspend = bh1780_suspend,
	.resume = bh1780_resume,
	.id_table = bh1780_id,
};

static int __init bh1780_init(void)
{
	atomic_set(&lux, WQ_IDLE);
	return i2c_add_driver(&bh1780_driver);;
}

static void __exit bh1780_cleanup(void)
{
	i2c_del_driver(&bh1780_driver);
}

module_init(bh1780_init);
module_exit(bh1780_cleanup);

MODULE_AUTHOR("Klaus Steinhammer <kst@tttech.com>");
MODULE_DESCRIPTION("bh1780 ambient light sensor driver");
MODULE_LICENSE("GPL");
