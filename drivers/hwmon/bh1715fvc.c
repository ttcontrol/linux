/*
 * Copyright 2008-2009 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

/*!
 * @file drivers/hwmon/bh1715fvc.c
 *
 * @brief bh1715fvc ambient light sensor Driver
 *
 * @ingroup
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/hwmon.h>
#include <linux/hwmon-sysfs.h>
#include <linux/ctype.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/regulator/consumer.h>
#include <mach/hardware.h>

#define BH1715_CMD_POWER_OFF		0x00
#define BH1715_CMD_POWER_ON			0x01
#define BH1715_CMD_RESET			0x07
#define BH1715_CMD_CONT_H_MODE		0x10
#define BH1715_CMD_CONT_L_MODE		0x13
#define BH1715_CMD_SINGLE_H_MODE	0x20
#define BH1715_CMD_SINGLE_L_MODE	0x23
#define BH1715_CMD_CHG_MEASR_TM_H	0x40
#define BH1715_CMD_CHG_MEASR_TM_L	0x60

struct bh1715_data {
	struct i2c_client *client;
	struct device *hwmon_dev;
	int high_mode;
	unsigned char enable;
};

static struct i2c_client *bh1715_client;


/*!
 * This function do the bh1715 config and enable.
 */
static int bh1715_on(void)
{
	struct bh1715_data *data = i2c_get_clientdata(bh1715_client);

	if (data->enable)
		return -ENODEV;

	if (i2c_smbus_write_byte(bh1715_client, BH1715_CMD_POWER_ON))
		return -ENODEV;

	msleep(10);

	if (data->high_mode) 
	{
		if (i2c_smbus_write_byte(bh1715_client, BH1715_CMD_CONT_H_MODE))
			return -ENODEV;
	}
	else
	{
		if (i2c_smbus_write_byte(bh1715_client, BH1715_CMD_CONT_L_MODE))
			return -ENODEV;
	}

	data->enable = 1;

	pr_info("bh1715 on\n");
	return 0;
}

/*!
 * This function shut down the bh1715.
 */
static int bh1715_off(void)
{
	struct bh1715_data *data = i2c_get_clientdata(bh1715_client);

	if (!data->enable)
		return 0;

	if (i2c_smbus_write_byte(bh1715_client, BH1715_CMD_POWER_OFF))
		return -ENODEV;

	data->enable = 0;

	pr_info("bh1715 off\n");
	return 0;
}

/*!
 * This function read the bh1715 lux registers and convert them to the lux
 * value.
 *
 * @output buffer	this param holds the lux value, when =-1, read fail
 *
 * @return 0
 */
static int bh1715_read_lux(void)
{
	char buf[2];
	int lux = -1;
	struct bh1715_data *data = i2c_get_clientdata(bh1715_client);
	
	if (!data->enable)
		goto err;

	if (i2c_master_recv(bh1715_client,buf,2) < 0)
		goto err;

	lux = (buf[0]<<8)+buf[1];;

	return lux;
err:
	return -1;
}

static ssize_t ls_enable(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	char *endp;
	int enable = simple_strtoul(buf, &endp, 0);
	size_t size = endp - buf;

	if (*endp && isspace(*endp))
		size++;
	if (size != count)
		return -EINVAL;

	if (enable == 1) {
		if (bh1715_on())
			pr_info("device open fail\n");
	}
	if (enable == 0) {
		if (bh1715_off())
			pr_info("device powerdown fail\n");
	}

	return count;
}

static SENSOR_DEVICE_ATTR(enable, S_IWUGO, NULL, ls_enable, 0);

static ssize_t ls_h_mode(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	char *endp;
	int enable = simple_strtoul(buf, &endp, 0);
	size_t size = endp - buf;
	struct bh1715_data *data = i2c_get_clientdata(bh1715_client);

	if (*endp && isspace(*endp))
		size++;
	if (size != count)
		return -EINVAL;

	if (enable == 1) {
		if (data->high_mode)
			return count;
			
		data->high_mode = 1;
		if (bh1715_off())
			pr_info("device powerdown fail\n");
		msleep(10);
		if (bh1715_on())
			pr_info("device open fail\n");
	}
	if (enable == 0) {
		if (data->high_mode == 0)
			return count;
			
		data->high_mode = 0;
		if (bh1715_off())
			pr_info("device powerdown fail\n");
		msleep(10);
		if (bh1715_on())
			pr_info("device open fail\n");
	}

	return count;
}

static SENSOR_DEVICE_ATTR(h_mode, S_IWUGO, NULL, ls_h_mode, 0);


static ssize_t show_lux(struct device *dev, struct device_attribute *attr,
			char *buf)
{
	struct bh1715_data *data = i2c_get_clientdata(bh1715_client);
	
	if (!data->enable)
		return snprintf(buf, PAGE_SIZE, "off\n");
	else
		return snprintf(buf, PAGE_SIZE, "%u\n", bh1715_read_lux());
}

static SENSOR_DEVICE_ATTR(lux, S_IRUGO, show_lux, NULL, 0);

static int bh1715_i2c_probe(struct i2c_client *client,
			      const struct i2c_device_id *did)
{
	int err = 0;
	struct bh1715_data *data;

	if (i2c_smbus_write_byte(client, BH1715_CMD_POWER_ON))
		err = -ENODEV;

	msleep(10);

	if (i2c_smbus_write_byte(client, BH1715_CMD_CONT_H_MODE))
		err = -ENODEV;

	bh1715_client = client;
	data = kzalloc(sizeof(struct bh1715_data), GFP_KERNEL);
	if (data == NULL) {
		err = -ENOMEM;
		goto exit1;
	}

	i2c_set_clientdata(client, data);
	data->client = client;
	
	data->high_mode = 1;
	data->enable = 1;

	err = device_create_file(&client->dev, &sensor_dev_attr_enable.dev_attr);
	if (err)
		goto exit2;

	err = device_create_file(&client->dev, &sensor_dev_attr_h_mode.dev_attr);
	if (err)
		goto exit_remove1;

	err = device_create_file(&client->dev, &sensor_dev_attr_lux.dev_attr);
	if (err)
		goto exit_remove2;

	/* Register sysfs hooks */
	data->hwmon_dev = hwmon_device_register(&client->dev);
	if (IS_ERR(data->hwmon_dev)) {
		err = PTR_ERR(data->hwmon_dev);
		goto exit_remove3;
	}

	return 0;

exit_remove3:
	device_remove_file(&client->dev, &sensor_dev_attr_lux.dev_attr);
exit_remove2:
	device_remove_file(&client->dev, &sensor_dev_attr_h_mode.dev_attr);
exit_remove1:
	device_remove_file(&client->dev, &sensor_dev_attr_enable.dev_attr);
exit2:
	kfree(data);
exit1:
	bh1715_client = NULL;
	return err;
}

static int bh1715_i2c_remove(struct i2c_client *client)
{
	struct bh1715_data *data = i2c_get_clientdata(client);

	hwmon_device_unregister(data->hwmon_dev);
	device_remove_file(&client->dev, &sensor_dev_attr_enable.dev_attr);
	device_remove_file(&client->dev, &sensor_dev_attr_h_mode.dev_attr);
	device_remove_file(&client->dev, &sensor_dev_attr_lux.dev_attr);
	kfree(data);
	return 0;
}

static int bh1715_suspend(struct i2c_client *client, pm_message_t message)
{
	struct bh1715_data *data = i2c_get_clientdata(client);

	if (!data->enable)
		goto exit;

	if (i2c_smbus_write_byte(client, BH1715_CMD_POWER_OFF))
		goto err;
exit:
	return 0;
err:
	return -ENODEV;
}

static int bh1715_resume(struct i2c_client *client)
{
	struct bh1715_data *data = i2c_get_clientdata(client);

	if (!data->enable)
		goto exit;

	if (i2c_smbus_write_byte(client, BH1715_CMD_POWER_ON))
		goto err;

	msleep(10);

	if (data->high_mode) 
	{
		if (i2c_smbus_write_byte(client, BH1715_CMD_CONT_H_MODE))
			goto err;
	}
	else
	{
		if (i2c_smbus_write_byte(client, BH1715_CMD_CONT_L_MODE))
			goto err;
	}
	
exit:
	return 0;
err:
	return -ENODEV;
}

static const struct i2c_device_id bh1715_id[] = {
	{"bh1715", 0},
	{}
};
MODULE_DEVICE_TABLE(i2c, bh1715_id);

static struct i2c_driver bh1715_driver = {
	.driver = {
		   .name = "bh1715",
		   },
	.probe = bh1715_i2c_probe,
	.remove = bh1715_i2c_remove,
	.suspend = bh1715_suspend,
	.resume = bh1715_resume,
	.id_table = bh1715_id,
};

static int __init bh1715_init(void)
{
	return i2c_add_driver(&bh1715_driver);;
}

static void __exit bh1715_cleanup(void)
{
	i2c_del_driver(&bh1715_driver);
}

module_init(bh1715_init);
module_exit(bh1715_cleanup);

MODULE_AUTHOR("Klaus Steinhammer <kst@tttech.com>");
MODULE_DESCRIPTION("bh1715 ambient light sensor driver");
MODULE_LICENSE("GPL");
