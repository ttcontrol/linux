/*
 * vision2_sensors.c
 *
 * driver for sensors connected via the PMIC ADC
 * Copyright (C) 2010 Klaus Steinhammer <kst@tttech.com>
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/sysfs.h>
#include <linux/hwmon.h>
#include <linux/mutex.h>
#include <linux/sched.h>
#include <linux/freezer.h>
#include <linux/pmic_external.h>
#include <linux/pmic_adc.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/slab.h>


#define DRVNAME		"vision2_sensors"

struct v2sensors {
	struct device *hwmon_dev;
	struct mutex lock;
	int    cpu_temp;
	int    pmic_temp;
	int    ambient_temp;
	int    main_voltage;
	int    main_current;
	int    licell_voltage;
	int    k30_voltage;
	int    state;
};

#define STATE_CONVERT_CPU		0
#define STATE_CONVERT_PMIC		1
#define STATE_CONVERT_AMBIENT	2
#define STATE_CONVERT_BATTERY_V	3
#define STATE_CONVERT_BATTERY_C	4
#define STATE_CONVERT_LICELL	5
#define STATE_CONVERT_K30_V	6

/* licell measurement is 500mV less than actual cell voltage because of circuit design */
#define LICELL_CORECTION_TERM 490

static volatile u32 v2sensors_task_installed;

static int v2sensors_thread(void *arg)
{
	unsigned short result;
	struct v2sensors *p_v2sensors = (struct v2sensors*)arg;

	daemonize("v2_sensor_thread");
	while (v2sensors_task_installed) {
		try_to_freeze();
		switch (p_v2sensors->state)
		{
		case STATE_CONVERT_CPU:
			if (0 != pmic_adc_convert(GEN_PURPOSE_AD6, &result))
				continue;
			if (result == 0)
				continue;
			p_v2sensors->cpu_temp = (((result * 1024) - 218453) * 1000 ) / 4369 ;
			p_v2sensors->state = STATE_CONVERT_PMIC;
			break;
		case STATE_CONVERT_PMIC:
			if (0 != pmic_adc_convert(DIE_TEMP, &result))
				continue;
			if (result == 0)
				continue;
			p_v2sensors->pmic_temp = (((result - 680) * 4244) / 10) + 25000 ;
			p_v2sensors->state = STATE_CONVERT_AMBIENT;
			break;
		case STATE_CONVERT_AMBIENT:
			if (0 != pmic_adc_convert(GEN_PURPOSE_AD8, &result))
				continue;
			if (result == 0)
				continue;
			p_v2sensors->ambient_temp = (((result * 1024) - 218453) * 1000 ) / 4369 ;
			p_v2sensors->state = STATE_CONVERT_K30_V;
			break;
                case STATE_CONVERT_K30_V:
                        if (0 != pmic_adc_convert(GEN_PURPOSE_AD7, &result))
                                continue;
                        if (result == 0)
                                continue;
                        p_v2sensors->k30_voltage = ((result * 2475) * 1000) / 33056 ;
                        p_v2sensors->state = STATE_CONVERT_BATTERY_V;
                        break;
		case STATE_CONVERT_BATTERY_V:
			if (0 != pmic_adc_convert(BATTERY_VOLTAGE, &result))
				continue;
			if (result == 0)
				continue;
			p_v2sensors->main_voltage = ((result * 615) * 1000 ) / 131072 ;
			p_v2sensors->state = STATE_CONVERT_BATTERY_C;
			break;
		case STATE_CONVERT_BATTERY_C:
			if (0 != pmic_adc_convert(BATTERY_CURRENT, &result))
				continue;
			if (result == 0)
				continue;
			p_v2sensors->main_current = ((result * 3075) * 1000 ) / 1048576 ;
			p_v2sensors->state = STATE_CONVERT_LICELL;
			break;
		case STATE_CONVERT_LICELL:
			if (0 != pmic_adc_convert(LICELL, &result))
				continue;
			if (result == 0)
				continue;
			p_v2sensors->licell_voltage = (((result * 1845) * 1000 ) / 524288) + LICELL_CORECTION_TERM;
			p_v2sensors->state = STATE_CONVERT_CPU;
			break;
		default:
			p_v2sensors->state = STATE_CONVERT_CPU;	
			break;
		}
		result = 0;

		msleep(166);
	}

	return 0;
}


/* sysfs hook function */
static ssize_t v2sensors_sense(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct v2sensors *p_v2sensors = dev_get_drvdata(dev);
	int ret;

	if (!strcmp(attr->attr.name,"cpu_temp"))
		ret = sprintf(buf, "%d\n", p_v2sensors->cpu_temp); /* millidegrees Celsius */
	else if (!strcmp(attr->attr.name,"pmic_temp"))
		ret = sprintf(buf, "%d\n", p_v2sensors->pmic_temp); /* millidegrees Celsius */
	else if (!strcmp(attr->attr.name,"ambient_temp"))
		ret = sprintf(buf, "%d\n", p_v2sensors->ambient_temp); /* millidegrees Celsius */
	else if (!strcmp(attr->attr.name,"main_voltage"))
		ret = sprintf(buf, "%d\n", p_v2sensors->main_voltage); /* milliVolt */
	else if (!strcmp(attr->attr.name,"main_current"))
		ret = sprintf(buf, "%d\n", p_v2sensors->main_current); /* milliAmpere */
	else if (!strcmp(attr->attr.name,"licell_voltage"))
		ret = sprintf(buf, "%d\n", p_v2sensors->licell_voltage); /* milliVolt */
	else if (!strcmp(attr->attr.name,"k30_voltage"))
                ret = sprintf(buf, "%d\n", p_v2sensors->k30_voltage); /* milliVolt */
	else
		ret = -1;
	return ret;
}

static DEVICE_ATTR(cpu_temp, S_IRUGO, v2sensors_sense, NULL);
static DEVICE_ATTR(pmic_temp, S_IRUGO, v2sensors_sense, NULL);
static DEVICE_ATTR(ambient_temp, S_IRUGO, v2sensors_sense, NULL);
static DEVICE_ATTR(main_voltage, S_IRUGO, v2sensors_sense, NULL);
static DEVICE_ATTR(main_current, S_IRUGO, v2sensors_sense, NULL);
static DEVICE_ATTR(licell_voltage, S_IRUGO, v2sensors_sense, NULL);
static DEVICE_ATTR(k30_voltage, S_IRUGO, v2sensors_sense, NULL);

/*----------------------------------------------------------------------*/
/* device name                                                          */
/*----------------------------------------------------------------------*/

static ssize_t v2sensors_show_name(struct device *dev, struct device_attribute
			      *devattr, char *buf)
{
	int ret;

	ret = sprintf(buf, "Vision2 sensor driver\n");
	return ret;
}

static DEVICE_ATTR(name, S_IRUGO, v2sensors_show_name, NULL);

/*----------------------------------------------------------------------*/
/* probe device                                                         */
/*----------------------------------------------------------------------*/


static int __devinit v2sensors_probe(struct platform_device *pdev)
{
	struct v2sensors *p_v2sensors;
	int status;

	if (!is_pmic_adc_ready())
		return -ENODEV;

	p_v2sensors = kzalloc(sizeof *p_v2sensors, GFP_KERNEL);
	if (!p_v2sensors)
		return -ENOMEM;

	mutex_init(&p_v2sensors->lock);
	
	p_v2sensors->hwmon_dev = hwmon_device_register(&pdev->dev);
	if (IS_ERR(p_v2sensors->hwmon_dev))
		return -ENODEV;

	if ((status = device_create_file(&pdev->dev, &dev_attr_cpu_temp))
	 || (status = device_create_file(&pdev->dev, &dev_attr_pmic_temp))
	 || (status = device_create_file(&pdev->dev, &dev_attr_ambient_temp))
	 || (status = device_create_file(&pdev->dev, &dev_attr_main_voltage))
	 || (status = device_create_file(&pdev->dev, &dev_attr_main_current))
	 || (status = device_create_file(&pdev->dev, &dev_attr_licell_voltage))
	 || (status = device_create_file(&pdev->dev, &dev_attr_k30_voltage))
	 || (status = device_create_file(&pdev->dev, &dev_attr_name))) {
		dev_dbg(&pdev->dev, "device_create_file failure.\n");
		goto out_dev_create_file_failed;
	}
	dev_set_drvdata (&pdev->dev, p_v2sensors);
	
	p_v2sensors->state = STATE_CONVERT_CPU;

	v2sensors_task_installed = 1;
	kernel_thread(v2sensors_thread, p_v2sensors, CLONE_VM | CLONE_FS);
	return 0;

out_dev_create_file_failed:
	device_remove_file(&pdev->dev, &dev_attr_cpu_temp);
	device_remove_file(&pdev->dev, &dev_attr_pmic_temp);
	device_remove_file(&pdev->dev, &dev_attr_ambient_temp);
	device_remove_file(&pdev->dev, &dev_attr_main_voltage);
	device_remove_file(&pdev->dev, &dev_attr_main_current);
	device_remove_file(&pdev->dev, &dev_attr_licell_voltage);
	device_remove_file(&pdev->dev, &dev_attr_k30_voltage);
	hwmon_device_unregister(p_v2sensors->hwmon_dev);
	kfree(p_v2sensors);
	return status;

}


static int __devexit v2sensors_remove(struct platform_device *pdev)
{
	struct v2sensors *p_v2sensors;

	v2sensors_task_installed = 0;
	p_v2sensors = dev_get_drvdata(&pdev->dev);

	device_remove_file(&pdev->dev, &dev_attr_cpu_temp);
	device_remove_file(&pdev->dev, &dev_attr_pmic_temp);
	device_remove_file(&pdev->dev, &dev_attr_ambient_temp);
	device_remove_file(&pdev->dev, &dev_attr_main_voltage);
	device_remove_file(&pdev->dev, &dev_attr_main_current);
	device_remove_file(&pdev->dev, &dev_attr_licell_voltage);
	device_remove_file(&pdev->dev, &dev_attr_k30_voltage);
	device_remove_file(&pdev->dev, &dev_attr_name);
	hwmon_device_unregister(p_v2sensors->hwmon_dev);
	kfree(p_v2sensors);

	return 0;
}



static struct platform_driver v2sensors_driver = {
	.driver = {
		   .name = "vision2_sensors",
		   },
	.suspend = NULL,
	.resume = NULL,
	.probe = v2sensors_probe,
	.remove = v2sensors_remove,
};

static int __init init_v2sensors(void)
{
	pr_debug("Vision2 OnBoard sensor driver loading...\n");
	return platform_driver_register(&v2sensors_driver);
}

static void __exit cleanup_v2sensors(void)
{
	platform_driver_unregister(&v2sensors_driver);
	pr_debug("Vision2 OnBoard sensor driver successfully unloaded\n");
}

late_initcall(init_v2sensors);
module_exit(cleanup_v2sensors);

MODULE_AUTHOR("Klaus Steinhammer <kst@tttech.com>");
MODULE_DESCRIPTION("TTC Vision2 onBoard Sensors driver");
MODULE_LICENSE("GPL");
