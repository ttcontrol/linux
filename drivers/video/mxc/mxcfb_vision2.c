/*
 * Copyright 2008-2009 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

/*!
 * @defgroup Framebuffer Framebuffer Driver for SDC and ADC.
 */

/*!
 * @file mxcfb_CMO_G070Y2-L01.c
 *
 * @brief Initialisation driver fro CMO_G070Y2-L01 VGA LCD
 *
 * @ingroup Framebuffer
 */

/*!
 * Include files
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/console.h>
#include <linux/delay.h>
#include <linux/errno.h>
#include <linux/fb.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/mxcfb.h>
#include <linux/regulator/consumer.h>
#include <mach/hardware.h>
#include <mach/gpio.h>
#include <linux/pwm_backlight.h>
#include "../../../arch/arm/mach-mx5/iomux.h"
#include "../../../arch/arm/mach-mx5/mx51_pins.h"

static void lcd_poweron(void);
static void lcd_poweroff(void);

static struct platform_device *plcd_dev;
static int lcd_on;
static unsigned char *backlight_on;

int lcd_open_fb(struct fb_info *info)
{
	struct fb_var_screeninfo var;
	struct list_head *pos;
	struct fb_modelist *modelist = NULL;

	printk(KERN_INFO "generic lcd driver: %s\n", info->fix.id);
	
	memset(&var, 0, sizeof(var));
	list_for_each(pos, &info->modelist)
	{
		modelist = list_entry(pos, struct fb_modelist, list);
	}

	if (modelist == NULL)
	{
		printk("mxcfb: Could not get video mode. FB not initialized.\n");
		return -EIO;
	}

	fb_videomode_to_var(&var, &modelist->mode);
	var.activate = FB_ACTIVATE_ALL;
	/* set virtual buffer three times the visible area to support PANSWAP */
//	var.yres_virtual = 512 * 3; /* use 512 instead of 480 due to offset */
	var.yres_virtual = (modelist->mode.yres + (512 % modelist->mode.yres)) * 3;

	acquire_console_sem();
	info->flags |= FBINFO_MISC_USEREVENT;
	fb_set_var(info, &var);
	info->flags &= ~FBINFO_MISC_USEREVENT;
	release_console_sem();
        return 0;
}

static int lcd_fb_event(struct notifier_block *nb, unsigned long val, void *v)
{
	struct fb_event *event = v;

	if (strcmp(event->info->fix.id, "DISP3 BG")) {
		return 0;
	}

	switch (val) {
	case FB_EVENT_FB_REGISTERED:
		lcd_open_fb(event->info);
		lcd_poweron();
		break;
	case FB_EVENT_BLANK:
/*		if ((event->info->var.xres != 800) ||
		    (event->info->var.yres != 480)) {
			break;
		}*/
/*		if (*((int *)event->data) == FB_BLANK_UNBLANK) {
			lcd_poweron();
		} else {
			lcd_poweroff();
		}*/
		break;
	}
	return 0;
}

static struct notifier_block nb = {
	.notifier_call = lcd_fb_event,
};


/*! /////XXX this comment does not mach the code!?
 * This function is called whenever the SPI slave device is detected.
 *
 * @param	spi	the SPI slave device
 *
 * @return 	Returns 0 on SUCCESS and error on FAILURE.
 */
static int __devinit lcd_probe(struct platform_device *pdev)
{
	struct device *pwm_dev;
	struct pwm_bl_data *pb;
#ifndef CONFIG_FB_KEEP_BOOTSPLASH
        int i;
#endif /* CONFIG_FB_KEEP_BOOTSPLASH */

	pwm_dev = bus_find_device_by_name(pdev->dev.bus,NULL,"pwm-backlight");
	if (pwm_dev) {
		pb = dev_get_drvdata(&((struct backlight_device*)(dev_get_drvdata(pwm_dev)))->dev);
		backlight_on = &(pb->on_off);
	}
	else
		backlight_on = NULL;

#ifndef CONFIG_FB_KEEP_BOOTSPLASH
        for (i = 0; i < num_registered_fb; i++) {
                if (strcmp(registered_fb[i]->fix.id, "DISP3 BG") == 0) {
                        lcd_open_fb(registered_fb[i]);
                        fb_show_logo(registered_fb[i], 0);
                        lcd_poweron();
                } else if (strcmp(registered_fb[i]->fix.id, "DISP3 FG") == 0) {
                        lcd_open_fb(registered_fb[i]);
                }
        }
#endif /* CONFIG_FB_KEEP_BOOTSPLASH */
	fb_register_client(&nb);

	plcd_dev = pdev;

	return 0;
}

static int __devexit lcd_remove(struct platform_device *pdev)
{
	fb_unregister_client(&nb);
	lcd_poweroff();

	return 0;
}

#ifdef CONFIG_PM
static int lcd_suspend(struct platform_device *pdev, pm_message_t state)
{
	lcd_poweroff();
	return 0;
}

static int lcd_resume(struct platform_device *pdev)
{
	lcd_poweron();
	return 0;
}
#else
#define lcd_suspend NULL
#define lcd_resume NULL
#endif

/*!
 * platform driver structure for CMO G070Y2-L01 WVGA LCD
 */
static struct platform_driver lcd_driver = {
	.driver = {
		   .name = "mxcfb_vision2"},
	.probe = lcd_probe,
	.remove = __devexit_p(lcd_remove),
	.suspend = lcd_suspend,
	.resume = lcd_resume,
};

static void lcd_poweron(void)
{
	if (backlight_on) {
		*backlight_on = 1;
		return;
	}
	
	if (lcd_on)
		return;

	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_7), 1);	/* set 12V_SUP_DISn */
	msleep(10);
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_DI1_PIN12), 1);	/* set DAB_DISPLAY_EN */
	msleep(10);
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_DI1_D1_CS), 1);	/* set DAB_LIGHT_EN */
	msleep(11);

	mxc_request_iomux(MX51_PIN_GPIO1_2, IOMUX_CONFIG_ALT0);	/* no pwm -> gpio output */
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_2), 1);		/* 100% on */
		
	lcd_on = 1;
}

static void lcd_poweroff(void)
{
	if (backlight_on) {
		*backlight_on = 0;
		return;
	}
	
	lcd_on = 0;
	dev_dbg(&plcd_dev->dev, "mxcfb_CMO_G070Y2-L01: turning off LCD\n");
	
	mxc_request_iomux(MX51_PIN_GPIO1_2, IOMUX_CONFIG_ALT0);	/* GPIO output */
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_2), 0);		/* 0% on */
	
	msleep(11);
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_DI1_D1_CS), 0);	/* set DAB_LIGHT_EN */
	msleep(10);
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_DI1_PIN12), 0);	/* set DAB_DISPLAY_EN */
	msleep(10);
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_7), 0);	/* set 12V_SUP_DISn */
}

static int __init vision2_lcd_init(void)
{
	return platform_driver_register(&lcd_driver);
}

static void __exit vision2_lcd_exit(void)
{
	platform_driver_unregister(&lcd_driver);
}

module_init(vision2_lcd_init);
module_exit(vision2_lcd_exit);

MODULE_AUTHOR("TTControl GmbH");
MODULE_DESCRIPTION("Vision2 generic LCD device driver");
MODULE_LICENSE("GPL");
