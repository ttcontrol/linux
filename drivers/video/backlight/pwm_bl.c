/*
 * linux/drivers/video/backlight/pwm_bl.c
 *
 * simple PWM based backlight control, board code has to setup
 * 1) pin configuration so PWM waveforms can output
 * 2) platform_data being correctly configured
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/fb.h>
#include <linux/backlight.h>
#include <linux/err.h>
#include <linux/pwm.h>
#include <linux/pwm_backlight.h>
#include <linux/slab.h>
#include <linux/freezer.h>
#include <linux/delay.h>
#include <mach/gpio.h>
#include "../../../arch/arm/mach-mx5/iomux.h"
#include "../../../arch/arm/mach-mx5/mx51_pins.h"

#define FADE_TASK_PERIOD_MS		10

static volatile u32 fade_thread_installed;

#define STATE_OFF		0
#define STATE_SWIITCH_ON	1
#define STATE_ACTIVE		2
#define STATE_SWITCH_OFF	3

static int pwm_fade_thread(void *arg)
{
        struct backlight_device *bldev = arg;
	struct pwm_bl_data *pb = dev_get_drvdata(&bldev->dev);
	unsigned char state;

	daemonize("pwm_fade_thread");
	state = STATE_ACTIVE;	
	while (fade_thread_installed) {
		try_to_freeze();
		switch (state)
		{
			case STATE_OFF:
				if (pb->on_off) {
					state = STATE_SWIITCH_ON;
					gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_DI1_PIN12), 1);	/* set DAB_DISPLAY_EN */
					msleep(100);
					gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_DI1_D1_CS), 1);	/* set DAB_LIGHT_EN */
					msleep(11);
				
					mxc_request_iomux(MX51_PIN_GPIO1_2, IOMUX_CONFIG_ALT1);	/* pwm output */
				}
				break;
			case STATE_SWIITCH_ON:
				if (pb->fade_value != pb->fade_target) {
					pb->fade_buf += pb->fade_increment;
					if (pb->fade_buf > 1000)
					{
						pb->fade_value += (pb->fade_buf / 1000);
						if (pb->fade_value > pb->fade_target)
							pb->fade_value = pb->fade_target;
									
						pb->fade_buf -= ((pb->fade_buf / 1000) * 1000);
					}
					pwm_config(pb->pwm, (pb->fade_value * pb->period) / bldev->props.max_brightness, pb->period);
					pwm_enable(pb->pwm);					
				}
				else {
					pb->fade_buf = 0;
					state = STATE_ACTIVE;
				}
				break;
			case STATE_ACTIVE:
				if (!(pb->on_off))
					state = STATE_SWITCH_OFF;
					
				if (pb->fade_value != pb->fade_target)
				{
					pb->fade_buf += pb->fade_increment;
					if (pb->fade_buf > 1000)
					{
						if (pb->fade_value < pb->fade_target) {
							pb->fade_value += (pb->fade_buf / 1000);
							if (pb->fade_value > pb->fade_target)
								pb->fade_value = pb->fade_target;
						}
						else {
							pb->fade_value -= (pb->fade_buf / 1000);
							if ((pb->fade_value < pb->fade_target) || (pb->fade_value < 0))
								pb->fade_value = pb->fade_target;
						}
		
						pb->fade_buf -= ((pb->fade_buf / 1000) * 1000);
					}
					if (pb->fade_value == 0) {
						pwm_config(pb->pwm, 0, pb->period);
						pwm_disable(pb->pwm);
					} else {
						pwm_config(pb->pwm, (pb->fade_value * pb->period) / bldev->props.max_brightness, pb->period);
						pwm_enable(pb->pwm);
					}
					
				}
				else
					pb->fade_buf = 0;
				break;
			case STATE_SWITCH_OFF:
				if (pb->fade_value != 0)
				{
					pb->fade_buf += pb->fade_increment;
					if (pb->fade_buf > 1000)
					{
						pb->fade_value -= (pb->fade_buf / 1000);
						if (pb->fade_value < 0)
							pb->fade_value = 0;
		
						pb->fade_buf -= ((pb->fade_buf / 1000) * 1000);
					}
					if (pb->fade_value == 0) {
						pwm_config(pb->pwm, 0, pb->period);
						pwm_disable(pb->pwm);
					} else {
						pwm_config(pb->pwm, (pb->fade_value * pb->period) / bldev->props.max_brightness, pb->period);
						pwm_enable(pb->pwm);
					}				
				}
				else {
					state = STATE_OFF;
					pb->fade_buf = 0;

					gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_2), 0);
					mxc_request_iomux(MX51_PIN_GPIO1_2, IOMUX_CONFIG_ALT0);	/* GPIO output */
					
					msleep(11);
					gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_DI1_D1_CS), 0);	/* set DAB_LIGHT_EN */
					msleep(10);
					gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_DI1_PIN12), 0);	/* set DAB_DISPLAY_EN */
				}
				break;
			default:
				state = STATE_OFF;
		}
		msleep(FADE_TASK_PERIOD_MS);
	}
	return 0;
}

static int pwm_backlight_update_status(struct backlight_device *bl)
{
	struct pwm_bl_data *pb = dev_get_drvdata(&bl->dev);
	struct platform_pwm_backlight_data *data = bl->dev.platform_data;
	int brightness = bl->props.brightness;
	int max = bl->props.max_brightness;

/*	if (bl->props.power != FB_BLANK_UNBLANK)
		brightness = 0;

	if (bl->props.fb_blank != FB_BLANK_UNBLANK)
		brightness = 0;*/

	if (data && data->notify)
		brightness = data->notify(&bl->dev, brightness);

	if (pb->fade_increment) {
		pb->fade_target = brightness;
	}
	else
	{
		if (brightness == 0) {
			pwm_config(pb->pwm, 0, pb->period);
			pwm_disable(pb->pwm);
		} else {
			pwm_config(pb->pwm, (brightness * pb->period) / max, pb->period);
			pwm_enable(pb->pwm);
		}
	}
	return 0;
}

static int pwm_backlight_get_brightness(struct backlight_device *bl)
{
	return bl->props.brightness;
}

static const struct backlight_ops pwm_backlight_ops = {
	.update_status	= pwm_backlight_update_status,
	.get_brightness	= pwm_backlight_get_brightness,
};

static int pwm_backlight_probe(struct platform_device *pdev)
{
	struct backlight_properties props;
	struct platform_pwm_backlight_data *data = pdev->dev.platform_data;
	struct backlight_device *bl;
	struct pwm_bl_data *pb;
	int ret;

	if (!data) {
		dev_err(&pdev->dev, "failed to find platform data\n");
		return -EINVAL;
	}

	if (data->init) {
		ret = data->init(&pdev->dev);
		if (ret < 0)
			return ret;
	}

	pb = kzalloc(sizeof(*pb), GFP_KERNEL);
	if (!pb) {
		dev_err(&pdev->dev, "no memory for state\n");
		ret = -ENOMEM;
		goto err_alloc;
	}

	pb->period = data->pwm_period_ns;

	pb->pwm = pwm_request(data->pwm_id, "backlight");
	if (IS_ERR(pb->pwm)) {
		dev_err(&pdev->dev, "unable to request PWM for backlight\n");
		ret = PTR_ERR(pb->pwm);
		goto err_pwm;
	} else
		dev_dbg(&pdev->dev, "got pwm for backlight\n");

	memset(&props, 0, sizeof(struct backlight_properties));
	props.max_brightness = data->max_brightness;
	bl = backlight_device_register(dev_name(&pdev->dev), &pdev->dev, pb,
				       &pwm_backlight_ops, &props);
	if (IS_ERR(bl)) {
		dev_err(&pdev->dev, "failed to register backlight\n");
		ret = PTR_ERR(bl);
		goto err_bl;
	}

	bl->props.brightness = data->dft_brightness;
	backlight_update_status(bl);

	platform_set_drvdata(pdev, bl);
	
	pb->fade_value = data->dft_brightness;
	pb->fade_target = data->dft_brightness;
	pb->fade_buf = 0;
	pb->on_off = 1; // keep on for boot image
	pwm_config(pb->pwm, (pb->fade_target * pb->period) / data->max_brightness, pb->period);
	pwm_enable(pb->pwm);					

	if (data->fade_steps_per_sec) {
		pb->fade_increment = data->fade_steps_per_sec * FADE_TASK_PERIOD_MS;
		fade_thread_installed = 1;
		kernel_thread(pwm_fade_thread, bl, CLONE_VM | CLONE_FS);
	}
	else
		pb->fade_increment = 0;
		
	return 0;

err_bl:
	pwm_free(pb->pwm);
err_pwm:
	kfree(pb);
err_alloc:
	if (data->exit)
		data->exit(&pdev->dev);
	return ret;
}

static int pwm_backlight_remove(struct platform_device *pdev)
{
	struct platform_pwm_backlight_data *data = pdev->dev.platform_data;
	struct backlight_device *bl = platform_get_drvdata(pdev);
	struct pwm_bl_data *pb = dev_get_drvdata(&bl->dev);
	
	fade_thread_installed = 0;

	backlight_device_unregister(bl);
	pwm_config(pb->pwm, 0, pb->period);
	pwm_disable(pb->pwm);
	pwm_free(pb->pwm);
	kfree(pb);
	if (data->exit)
		data->exit(&pdev->dev);
	return 0;
}

#ifdef CONFIG_PM
static int pwm_backlight_suspend(struct platform_device *pdev,
				 pm_message_t state)
{
	struct backlight_device *bl = platform_get_drvdata(pdev);
	struct pwm_bl_data *pb = dev_get_drvdata(&bl->dev);
	struct platform_pwm_backlight_data *data = pdev->dev.platform_data;

	if (data && data->notify)
		data->notify(&bl->dev, 0);
	pwm_config(pb->pwm, 0, pb->period);
	pwm_disable(pb->pwm);
	return 0;
}

static int pwm_backlight_resume(struct platform_device *pdev)
{
	struct backlight_device *bl = platform_get_drvdata(pdev);

	backlight_update_status(bl);
	return 0;
}
#else
#define pwm_backlight_suspend	NULL
#define pwm_backlight_resume	NULL
#endif

static struct platform_driver pwm_backlight_driver = {
	.driver		= {
		.name	= "pwm-backlight",
		.owner	= THIS_MODULE,
	},
	.probe		= pwm_backlight_probe,
	.remove		= pwm_backlight_remove,
	.suspend	= pwm_backlight_suspend,
	.resume		= pwm_backlight_resume,
};

static int __init pwm_backlight_init(void)
{
	return platform_driver_register(&pwm_backlight_driver);
}
module_init(pwm_backlight_init);

static void __exit pwm_backlight_exit(void)
{
	platform_driver_unregister(&pwm_backlight_driver);
}
module_exit(pwm_backlight_exit);

MODULE_DESCRIPTION("PWM based Backlight Driver");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:pwm-backlight");

