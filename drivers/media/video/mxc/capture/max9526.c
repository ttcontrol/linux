/*
 * Copyright 2005-2009 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

/*!
 * @file max9526.c
 *
 * @brief Maxim MAX9526 video decoder functions
 *
 * @ingroup Camera
 */


#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/ctype.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <linux/i2c.h>
#include <linux/wait.h>
#include <linux/videodev2.h>
#include <linux/workqueue.h>
#include <linux/regulator/consumer.h>
#include <media/v4l2-int-device.h>
#include <mach/hardware.h>
#include "mxc_v4l2_capture.h"
#include <linux/i2c/max9526.h>

extern void gpio_sensor_active(void);
extern void gpio_sensor_inactive(void);

static int max9526_probe(struct i2c_client *adapter, const struct i2c_device_id *id);
static int max9526_detach(struct i2c_client *client);

static const struct i2c_device_id max9526_id[] = {
	{"max9526", 0},
	{},
};

MODULE_DEVICE_TABLE(i2c, max9526_id);

static struct i2c_driver max9526_i2c_driver = {
	.driver = {
		   .owner = THIS_MODULE,
		   .name = "max9526",
		   },
	.probe = max9526_probe,
	.remove = max9526_detach,
	.id_table = max9526_id,
};

/*! List of input video formats supported. The video formats is corresponding
 * with v4l2 id in video_fmt_t
 */
typedef enum {
	MAX9526_NTSC = 0,	/*!< Locked on (M) NTSC video signal. */
	MAX9526_PAL,		/*!< (B, G, H, I, N)PAL video signal. */
	MAX9526_NOT_LOCKED,	/*!< Not locked on a signal. */
} video_fmt_idx;

/*! Number of video standards supported (including 'not locked' signal). */
#define MAX9526_STD_MAX		(MAX9526_PAL + 1)

/*! Video format structure. */
typedef struct {
	int v4l2_id;		/*!< Video for linux ID. */
	char name[16];		/*!< Name (e.g., "NTSC", "PAL", etc.) */
	u16 raw_width;		/*!< Raw width. */
	u16 raw_height;		/*!< Raw height. */
	u16 active_width;	/*!< Active width. */
	u16 active_height;	/*!< Active height. */
} video_fmt_t;

/*!
 * Maintains the information on the current state of the sensor.
 */
struct sensor {
	struct v4l2_int_device v4l2_int_device;
	struct v4l2_int_slave v4l2_int_slave;
	struct i2c_client *i2c_client;
	struct v4l2_pix_format pix;
	struct v4l2_captureparm streamcap;
	bool on;
	unsigned int csi;
	struct semaphore mutex;

	/* control settings */
	int brightness;
	int hue;
	int contrast;
	int saturation;
	int active_input;
	int red;
	int green;
	int blue;
	int ae_mode;

	v4l2_std_id std_id;

	video_fmt_idx video_idx;
};

/*! Description of video formats supported.
 *
 *  PAL: raw=720x625, active=720x576.
 *  NTSC: raw=720x525, active=720x480.
 */
static video_fmt_t video_fmts[] = {
	{			/*! NTSC */
	 .v4l2_id = V4L2_STD_NTSC,
	 .name = "NTSC",
	 .raw_width = 721,	/* SENS_FRM_WIDTH */
	 .raw_height = 288,	/* SENS_FRM_HEIGHT */
	 .active_width = 720,	/* ACT_FRM_WIDTH plus 1 */
	 .active_height = (480 / 2),	/* ACT_FRM_WIDTH plus 1 */
	 },
	{			/*! (B, G, H, I, N) PAL */
	 .v4l2_id = V4L2_STD_PAL,
	 .name = "PAL",
	 .raw_width = 721,
	 .raw_height = (576 / 2),// + 24 * 2 - 1,
	 .active_width = 720,
	 .active_height = (576 / 2),
	 },
	{			/*! Unlocked standard */
	 .v4l2_id = V4L2_STD_ALL,
	 .name = "Autodetect",
	 .raw_width = 721,
	 .raw_height = (576 / 2),// + 24 * 2 - 1,
	 .active_width = 720,
	 .active_height = (576 / 2),
	 },
};

/*!* Standard index of MAX9526. */
//static video_fmt_idx video_idx = MAX9526_PAL;

#define IF_NAME                    "max9526"
#define MAX9526_STATUS_0               0x00
#define MAX9526_STATUS_1               0x01
#define MAX9526_IRQ_MASK_0             0x02
#define MAX9526_IRQ_MASK_1             0x03
#define MAX9526_STD_SEL                0x04
#define MAX9526_CONTRAST               0x05
#define MAX9526_BRIGHTNESS             0x06
#define MAX9526_HUE                    0x07
#define MAX9526_SATURATION             0x08
#define MAX9526_INPUT_SELECT           0x09
#define MAX9526_GAIN_CTRL              0x0a
#define MAX9526_COLOR_KILL             0x0b
#define MAX9526_OUTPUT_TEST_SIGNAL     0x0c
#define MAX9526_CLOCK_AND_OUTPUT       0x0d
#define MAX9526_PLL_CONTROL            0x0e
#define MAX9526_MISC                   0x0f

/* supported controls */
/* This hasn't been fully implemented yet.
 * This is how it should work, though. */
static struct v4l2_queryctrl max9526_qctrl[] = {
	{
	.id = V4L2_CID_BRIGHTNESS,
	.type = V4L2_CTRL_TYPE_INTEGER,
	.name = "Brightness",
	.minimum = 0,
	.maximum = 255,
	.step = 1,
	.default_value = 0,
	.flags = 0,
	}, {
	.id = V4L2_CID_CONTRAST,
	.type = V4L2_CTRL_TYPE_INTEGER,
	.name = "Contrast",
	.minimum = 0,
	.maximum = 255,
	.step = 0x1,
	.default_value = 128,
	.flags = 0,
	}, {
	.id = V4L2_CID_SATURATION,
	.type = V4L2_CTRL_TYPE_INTEGER,
	.name = "Saturation",
	.minimum = 0,
	.maximum = 255,
	.step = 0x1,
	.default_value = 128,
	.flags = 0,
	},{
	.id		= V4L2_CID_MXC_CAMSELECT,	// use CAMSELECT parameter to select active input
	.type		= V4L2_CTRL_TYPE_INTEGER,
	.name		= "Camera input select",
	.minimum	= 0,
	.maximum	= 1,
	.step = 0x1,
	.default_value = 0,
	.flags = 0,
	}, {
	.id = V4L2_CID_HUE,
	.type = V4L2_CTRL_TYPE_INTEGER,
	.name = "Hue",
	.minimum = 0,
	.maximum = 255,
	.step = 0x1,
	.default_value = 128,
	.flags = 0,
	}
};

/***********************************************************************
 * I2C transfert.
 ***********************************************************************/

/*! Read one register from a MAX9526 i2c slave device.
 *
 *  @param *reg		register in the device we wish to access.
 *
 *  @return		       0 if success, an error code otherwise.
 */
static inline int max9526_read(struct sensor *sensor, u8 reg)
{

	int ret = 0;
	u8 regvalue = 0;
	
	struct i2c_adapter *adap=sensor->i2c_client->adapter;
	struct i2c_msg msg[2];

	msg[0].addr = sensor->i2c_client->addr;
	msg[0].flags = sensor->i2c_client->flags & I2C_M_TEN;
	msg[0].len = 1;
	msg[0].buf = &reg;

	msg[1].addr = sensor->i2c_client->addr;
	msg[1].flags = sensor->i2c_client->flags & I2C_M_TEN;
	msg[1].flags |= I2C_M_RD;
	msg[1].len = 1;
	msg[1].buf = &regvalue;

	ret = i2c_transfer(adap, msg, 2);

	if (ret != 2)
		goto err;
	
	dev_dbg(&sensor->i2c_client->dev, "%s:read reg: reg=%2x val=%2x\n", __func__, reg, regvalue);
	return regvalue;
	
err:
	dev_dbg(&sensor->i2c_client->dev, "%s:read reg error: reg=%2x \n", __func__, reg);
	return -ENODEV;

}

/*! Write one register of a MAX9526 i2c slave device.
 *
 *  @param *reg		register in the device we wish to access.
 *
 *  @return		       0 if success, an error code otherwise.
 */
static int max9526_write_reg(struct sensor *sensor, u8 reg, u8 val)
{
	int ret = 0;
	unsigned char msgbuf[2];

	msgbuf[0] = reg;
	msgbuf[1] = val;
	ret = i2c_master_send(sensor->i2c_client, msgbuf, 2);
	if (ret < 0) {
		dev_dbg(&sensor->i2c_client->dev, "%s:write reg error:reg=%2x,val=%2x\n", __func__, reg, val);
		return ret;
	}
	
	dev_dbg(&sensor->i2c_client->dev, "%s:write reg: reg=%2x,val=%2x\n", __func__, reg, val);

	return 0;
}

/***********************************************************************
 * mxc_v4l2_capture interface.
 ***********************************************************************/

/*!
 * Return attributes of current video standard.
 * Since this device autodetects the current standard, this function also
 * sets the values that need to be changed if the standard changes.
 * There is no set std equivalent function.
 *
 *  @return		None.
 */
static void max9526_get_std(struct sensor *sensor, v4l2_std_id *std)
{
	int tmp;
	int idx;

	dev_dbg(&sensor->i2c_client->dev, "In max9526_get_std\n");

	if ((((max9526_read(sensor, MAX9526_INPUT_SELECT) & 0x40) == 0x00) && (max9526_read(sensor, MAX9526_STATUS_0) & 0x80)) || /* ch 0 selected & valid signal */
	    (((max9526_read(sensor, MAX9526_INPUT_SELECT) & 0x40) == 0x40) && (max9526_read(sensor, MAX9526_STATUS_0) & 0x40))    /* ch 1 selected & valid signal */
	   ) { 
			tmp = max9526_read(sensor, MAX9526_STATUS_1) & 0x40;
			if (tmp == 0x00) {
				down(&sensor->mutex);
				/* PAL */
				dev_dbg(&sensor->i2c_client->dev, "PAL detected\n");
				*std = V4L2_STD_PAL;
				idx = MAX9526_PAL;
				up(&sensor->mutex);
			 }
			 else {
				down(&sensor->mutex);
				/*NTSC*/
				dev_dbg(&sensor->i2c_client->dev, "NTSC detected\n");
				*std = V4L2_STD_NTSC;
				idx = MAX9526_NTSC;
				up(&sensor->mutex);
			}
		}
		else {
			down(&sensor->mutex);
			// Set to PAL as test pattern output by MAX9526 is configured to be PAL format
			*std = V4L2_STD_PAL;
			idx = MAX9526_PAL;
			dev_dbg(&sensor->i2c_client->dev, "No valid input detected => PAL Test Pattern! \n");
			up(&sensor->mutex);
		}

	/* This assumes autodetect which this device uses. */
	if (*std != sensor->std_id) {
		sensor->video_idx = idx;
		sensor->std_id = *std;
		sensor->pix.width = video_fmts[sensor->video_idx].raw_width;
		sensor->pix.height = video_fmts[sensor->video_idx].raw_height;
	}
}

/***********************************************************************
 * IOCTL Functions from v4l2_int_ioctl_desc.
 ***********************************************************************/

/*!
 * ioctl_g_ifparm - V4L2 sensor interface handler for vidioc_int_g_ifparm_num
 * s: pointer to standard V4L2 device structure
 * p: pointer to standard V4L2 vidioc_int_g_ifparm_num ioctl structure
 *
 * Gets slave interface parameters.
 * Calculates the required xclk value to support the requested
 * clock parameters in p.  This value is returned in the p
 * parameter.
 *
 * vidioc_int_g_ifparm returns platform-specific information about the
 * interface settings used by the sensor.
 *
 * Called on open.
 */
static int ioctl_g_ifparm(struct v4l2_int_device *s, struct v4l2_ifparm *p)
{
	struct sensor *sensor = s->priv;

	dev_dbg(&sensor->i2c_client->dev, "In max9526:ioctl_g_ifparm\n");

	if (s == NULL) {
		pr_err("   ERROR!! no slave device set!\n");
		return -1;
	}

	/* Initialize structure to 0s then set any non-0 values. */
	memset(p, 0, sizeof(*p));
	p->if_type = V4L2_IF_TYPE_BT656; /* This is the only possibility. */
	p->u.bt656.mode = V4L2_IF_TYPE_BT656_MODE_NOBT_8BIT;
	//p->u.bt656.mode = V4L2_IF_TYPE_BT656_MODE_BT_10BIT;
//	p->u.bt656.clock_curr = 2; /* BT 656 INTERLACED */
	p->u.bt656.clock_curr = 1; /* GATED CLOCK */
	p->u.bt656.nobt_hs_inv = 1;
	p->u.bt656.nobt_vs_inv = 1;
//	p->u.bt656.latch_clk_inv = 1;
//	p->u.bt656.bt_sync_correct = 1;  /* Indicate external vsync */

	/* MAX9526 has a dedicated clock so no clock settings needed. */

	return 0;
}

/*!
 * Sets the camera power.
 *
 * s  pointer to the camera device
 * on if 1, power is to be turned on.  0 means power is to be turned off
 *
 * ioctl_s_power - V4L2 sensor interface handler for vidioc_int_s_power_num
 * @s: pointer to standard V4L2 device structure
 * @on: power state to which device is to be set
 *
 * Sets devices power state to requrested state, if possible.
 * This is called on open, close, suspend and resume.
 */
static int ioctl_s_power(struct v4l2_int_device *s, int on)
{
	struct sensor *sensor = s->priv;

	dev_dbg(&sensor->i2c_client->dev, "In max9526:ioctl_s_power\n");

	if (on && !sensor->on) {
		gpio_sensor_active();
		if (max9526_write_reg(sensor, MAX9526_STD_SEL, 0x11) != 0) // switch on & soft reset
			return -EIO;
	} else if (!on && sensor->on) {
		if (max9526_write_reg(sensor, MAX9526_STD_SEL, 0x08) != 0) // switch to low power
			return -EIO;
		gpio_sensor_inactive();
	}
	sensor->on = on;

	return 0;
}

/*!
 * ioctl_g_parm - V4L2 sensor interface handler for VIDIOC_G_PARM ioctl
 * @s: pointer to standard V4L2 device structure
 * @a: pointer to standard V4L2 VIDIOC_G_PARM ioctl structure
 *
 * Returns the sensor's video CAPTURE parameters.
 */
static int ioctl_g_parm(struct v4l2_int_device *s, struct v4l2_streamparm *a)
{
	struct sensor *sensor = s->priv;
	struct v4l2_captureparm *cparm = &a->parm.capture;

	dev_dbg(&sensor->i2c_client->dev, "In max9526:ioctl_g_parm\n");

	switch (a->type) {
	/* These are all the possible cases. */
	case V4L2_BUF_TYPE_VIDEO_CAPTURE:
		pr_debug("   type is V4L2_BUF_TYPE_VIDEO_CAPTURE\n");
		memset(a, 0, sizeof(*a));
		a->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		cparm->capability = sensor->streamcap.capability;
		cparm->timeperframe = sensor->streamcap.timeperframe;
		cparm->capturemode = sensor->streamcap.capturemode;
		break;

	case V4L2_BUF_TYPE_VIDEO_OUTPUT:
	case V4L2_BUF_TYPE_VIDEO_OVERLAY:
	case V4L2_BUF_TYPE_VBI_CAPTURE:
	case V4L2_BUF_TYPE_VBI_OUTPUT:
	case V4L2_BUF_TYPE_SLICED_VBI_CAPTURE:
	case V4L2_BUF_TYPE_SLICED_VBI_OUTPUT:
		break;

	default:
		pr_debug("ioctl_g_parm:type is unknown %d\n", a->type);
		break;
	}

	return 0;
}

/*!
 * ioctl_s_parm - V4L2 sensor interface handler for VIDIOC_S_PARM ioctl
 * @s: pointer to standard V4L2 device structure
 * @a: pointer to standard V4L2 VIDIOC_S_PARM ioctl structure
 *
 * Configures the sensor to use the input parameters, if possible.  If
 * not possible, reverts to the old parameters and returns the
 * appropriate error code.
 *
 * This driver cannot change these settings.
 */
static int ioctl_s_parm(struct v4l2_int_device *s, struct v4l2_streamparm *a)
{
	struct sensor *sensor = s->priv;

	dev_dbg(&sensor->i2c_client->dev, "In max9526:ioctl_s_parm\n");

	switch (a->type) {
	/* These are all the possible cases. */
	case V4L2_BUF_TYPE_VIDEO_CAPTURE:
	case V4L2_BUF_TYPE_VIDEO_OUTPUT:
	case V4L2_BUF_TYPE_VIDEO_OVERLAY:
	case V4L2_BUF_TYPE_VBI_CAPTURE:
	case V4L2_BUF_TYPE_VBI_OUTPUT:
	case V4L2_BUF_TYPE_SLICED_VBI_CAPTURE:
	case V4L2_BUF_TYPE_SLICED_VBI_OUTPUT:
		break;

	default:
		pr_debug("   type is unknown - %d\n", a->type);
		break;
	}

	return 0;
}

/*!
 * ioctl_g_fmt_cap - V4L2 sensor interface handler for ioctl_g_fmt_cap
 * @s: pointer to standard V4L2 device structure
 * @f: pointer to standard V4L2 v4l2_format structure
 *
 * Returns the sensor's current pixel format in the v4l2_format
 * parameter.
 */
static int ioctl_g_fmt_cap(struct v4l2_int_device *s, struct v4l2_format *f)
{
	struct sensor *sensor = s->priv;

	dev_dbg(&sensor->i2c_client->dev, "In max9526:ioctl_g_fmt_cap\n");

	switch (f->type) {
	case V4L2_BUF_TYPE_VIDEO_CAPTURE:
		pr_debug("   Returning size of %dx%d\n",
			 sensor->pix.width, sensor->pix.height);
		f->fmt.pix = sensor->pix;
		break;

	case V4L2_BUF_TYPE_PRIVATE: {
		v4l2_std_id std;
		max9526_get_std(sensor, &std);
		f->fmt.pix.pixelformat = (u32)std;
		pr_debug("   Returning standard %d (PAL=%Lx, NTSC=%Lx)\n",
                        (u32)std, V4L2_STD_PAL, V4L2_STD_NTSC);
		}
		break;

	default:
		f->fmt.pix = sensor->pix;
		break;
	}

	return 0;
}

/*!
 * ioctl_queryctrl - V4L2 sensor interface handler for VIDIOC_QUERYCTRL ioctl
 * @s: pointer to standard V4L2 device structure
 * @qc: standard V4L2 VIDIOC_QUERYCTRL ioctl structure
 *
 * If the requested control is supported, returns the control information
 * from the video_control[] array.  Otherwise, returns -EINVAL if the
 * control is not supported.
 */
static int ioctl_queryctrl(struct v4l2_int_device *s,
			   struct v4l2_queryctrl *qc)
{
	int i;
	struct sensor *sensor = s->priv;

	dev_dbg(&sensor->i2c_client->dev, "In max9526:ioctl_queryctrl\n");

	for (i = 0; i < ARRAY_SIZE(max9526_qctrl); i++)
		if (qc->id && qc->id == max9526_qctrl[i].id) {
			memcpy(qc, &(max9526_qctrl[i]),
				sizeof(*qc));
			return (0);
		}

	return -EINVAL;
}

/*!
 * ioctl_g_ctrl - V4L2 sensor interface handler for VIDIOC_G_CTRL ioctl
 * @s: pointer to standard V4L2 device structure
 * @vc: standard V4L2 VIDIOC_G_CTRL ioctl structure
 *
 * If the requested control is supported, returns the control's current
 * value from the video_control[] array.  Otherwise, returns -EINVAL
 * if the control is not supported.
 */
static int ioctl_g_ctrl(struct v4l2_int_device *s, struct v4l2_control *vc)
{
	int ret = 0;
	struct sensor *sensor = s->priv;

	dev_dbg(&sensor->i2c_client->dev, "In max9526:ioctl_g_ctrl\n");

	switch (vc->id) {
	case V4L2_CID_BRIGHTNESS:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_BRIGHTNESS\n");
		sensor->brightness = max9526_read(sensor, MAX9526_BRIGHTNESS);
		vc->value = sensor->brightness;
		break;
	case V4L2_CID_CONTRAST:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_CONTRAST\n");
		sensor->contrast = max9526_read(sensor, MAX9526_CONTRAST);
		vc->value = sensor->contrast;
		break;
	case V4L2_CID_SATURATION:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_SATURATION\n");
		sensor->saturation = max9526_read(sensor, MAX9526_SATURATION);
		vc->value = sensor->saturation;
		break;
	case V4L2_CID_HUE:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_HUE\n");
		sensor->hue = max9526_read(sensor, MAX9526_HUE);
		vc->value = sensor->hue;
		break;
	case V4L2_CID_AUTO_WHITE_BALANCE:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_AUTO_WHITE_BALANCE\n");
		break;
	case V4L2_CID_DO_WHITE_BALANCE:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_DO_WHITE_BALANCE\n");
		break;
	case V4L2_CID_RED_BALANCE:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_RED_BALANCE\n");
		vc->value = sensor->red;
		break;
	case V4L2_CID_BLUE_BALANCE:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_BLUE_BALANCE\n");
		vc->value = sensor->blue;
		break;
	case V4L2_CID_GAMMA:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_GAMMA\n");
		break;
	case V4L2_CID_EXPOSURE:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_EXPOSURE\n");
		vc->value = sensor->ae_mode;
		break;
	case V4L2_CID_AUTOGAIN:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_AUTOGAIN\n");
		break;
	case V4L2_CID_GAIN:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_GAIN\n");
		break;
	case V4L2_CID_MXC_CAMSELECT:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_MXC_CAMSELECT\n");
		sensor->active_input = (max9526_read(sensor, MAX9526_INPUT_SELECT) & 0x40) >> 6;
		vc->value = sensor->active_input;
		break;
	case V4L2_CID_MXC_LOSTLOCK:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_MXC_LOSTLOCK\n");
		vc->value = (max9526_read(sensor, MAX9526_STATUS_0) & 0x01);
	case V4L2_CID_VFLIP:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_VFLIP\n");
		break;
	default:
		dev_dbg(&sensor->i2c_client->dev, "   Default case\n");
		vc->value = 0;
		ret = -EPERM;
		break;
	}

	return ret;
}

/*!
 * ioctl_s_ctrl - V4L2 sensor interface handler for VIDIOC_S_CTRL ioctl
 * @s: pointer to standard V4L2 device structure
 * @vc: standard V4L2 VIDIOC_S_CTRL ioctl structure
 *
 * If the requested control is supported, sets the control's current
 * value in HW (and updates the video_control[] array).  Otherwise,
 * returns -EINVAL if the control is not supported.
 */
static int ioctl_s_ctrl(struct v4l2_int_device *s, struct v4l2_control *vc)
{
	int retval = 0;
	u8 tmp;
	struct sensor *sensor = s->priv;

	dev_dbg(&sensor->i2c_client->dev, "In max9526:ioctl_s_ctrl\n");

	switch (vc->id) {
	case V4L2_CID_BRIGHTNESS:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_BRIGHTNESS\n");
		tmp = vc->value;
		max9526_write_reg(sensor, MAX9526_BRIGHTNESS, tmp);
		sensor->brightness = vc->value;
		break;
	case V4L2_CID_CONTRAST:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_CONTRAST\n");
		tmp = vc->value;
		max9526_write_reg(sensor, MAX9526_CONTRAST, tmp);
		sensor->contrast = vc->value;
		break;
	case V4L2_CID_SATURATION:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_SATURATION\n");
		tmp = vc->value;
		max9526_write_reg(sensor, MAX9526_SATURATION, tmp);
		sensor->saturation = vc->value;
		break;
	case V4L2_CID_HUE:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_HUE\n");
		tmp = vc->value;
		max9526_write_reg(sensor, MAX9526_HUE, tmp);
		sensor->hue = vc->value;
		break;
	case V4L2_CID_AUTO_WHITE_BALANCE:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_AUTO_WHITE_BALANCE\n");
		break;
	case V4L2_CID_DO_WHITE_BALANCE:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_DO_WHITE_BALANCE\n");
		break;
	case V4L2_CID_RED_BALANCE:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_RED_BALANCE\n");
		break;
	case V4L2_CID_BLUE_BALANCE:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_BLUE_BALANCE\n");
		break;
	case V4L2_CID_GAMMA:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_GAMMA\n");
		break;
	case V4L2_CID_EXPOSURE:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_EXPOSURE\n");
		break;
	case V4L2_CID_AUTOGAIN:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_AUTOGAIN\n");
		break;
	case V4L2_CID_GAIN:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_GAIN\n");
		break;
	case V4L2_CID_MXC_CAMSELECT:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_CAMSELECT\n");
		tmp = vc->value;
		if (tmp == 1)
			max9526_write_reg(sensor, MAX9526_INPUT_SELECT, 0x42);
		else
			max9526_write_reg(sensor, MAX9526_INPUT_SELECT, 0x02);
		sensor->active_input = vc->value;
		break;
	case V4L2_CID_VFLIP:
		dev_dbg(&sensor->i2c_client->dev, "   V4L2_CID_VFLIP\n");
		break;
	default:
		dev_dbg(&sensor->i2c_client->dev, "   Default case\n");
		retval = -EPERM;
		break;
	}

	return retval;
}

/*!
 * ioctl_init - V4L2 sensor interface handler for VIDIOC_INT_INIT
 * @s: pointer to standard V4L2 device structure
 */
static int ioctl_init(struct v4l2_int_device *s)
{
	struct sensor *sensor = s->priv;

	dev_dbg(&sensor->i2c_client->dev, "In max9526:ioctl_init\n");
	return 0;
}

/*!
 * ioctl_dev_init - V4L2 sensor interface handler for vidioc_int_dev_init_num
 * @s: pointer to standard V4L2 device structure
 *
 * Initialise the device when slave attaches to the master.
 */
static int ioctl_dev_init(struct v4l2_int_device *s)
{
	struct sensor *sensor = s->priv;

	dev_dbg(&sensor->i2c_client->dev, "In max9526:ioctl_dev_init\n");
	return 0;
}

/*!
 * This structure defines all the ioctls for this module.
 */
static struct v4l2_int_ioctl_desc max9526_ioctl_desc[] = {

	{vidioc_int_dev_init_num, (v4l2_int_ioctl_func *)ioctl_dev_init},

	/*!
	 * Delinitialise the dev. at slave detach.
	 * The complement of ioctl_dev_init.
	 */
/*	{vidioc_int_dev_exit_num, (v4l2_int_ioctl_func *)ioctl_dev_exit}, */

	{vidioc_int_s_power_num, (v4l2_int_ioctl_func *)ioctl_s_power},
	{vidioc_int_g_ifparm_num, (v4l2_int_ioctl_func *)ioctl_g_ifparm},
/*	{vidioc_int_g_needs_reset_num, (v4l2_int_ioctl_func *)ioctl_g_needs_reset}, */
/*	{vidioc_int_reset_num, (v4l2_int_ioctl_func *)ioctl_reset}, */
	{vidioc_int_init_num, (v4l2_int_ioctl_func *)ioctl_init},

	/*!
	 * VIDIOC_ENUM_FMT ioctl for the CAPTURE buffer type.
	 */
/*	{vidioc_int_enum_fmt_cap_num, (v4l2_int_ioctl_func *)ioctl_enum_fmt_cap}, */

	/*!
	 * VIDIOC_TRY_FMT ioctl for the CAPTURE buffer type.
	 * This ioctl is used to negotiate the image capture size and
	 * pixel format without actually making it take effect.
	 */
/*	{vidioc_int_try_fmt_cap_num, (v4l2_int_ioctl_func *)ioctl_try_fmt_cap}, */

	{vidioc_int_g_fmt_cap_num, (v4l2_int_ioctl_func *)ioctl_g_fmt_cap},

	/*!
	 * If the requested format is supported, configures the HW to use that
	 * format, returns error code if format not supported or HW can't be
	 * correctly configured.
	 */
/*	{vidioc_int_s_fmt_cap_num, (v4l2_int_ioctl_func *)ioctl_s_fmt_cap}, */

	{vidioc_int_g_parm_num, (v4l2_int_ioctl_func *)ioctl_g_parm},
	{vidioc_int_s_parm_num, (v4l2_int_ioctl_func *)ioctl_s_parm},
	{vidioc_int_queryctrl_num, (v4l2_int_ioctl_func *)ioctl_queryctrl},
	{vidioc_int_g_ctrl_num, (v4l2_int_ioctl_func *)ioctl_g_ctrl},
	{vidioc_int_s_ctrl_num, (v4l2_int_ioctl_func *)ioctl_s_ctrl},
};

/***********************************************************************
 * I2C client and driver.
 ***********************************************************************/

/*! MAX9526 Reset function.
 *
 *  @return		None.
 */
static int max9526_hard_reset(struct sensor *sensor)
{
	int ret;
	
	dev_dbg(&sensor->i2c_client->dev, "In max9526:max9526_hard_reset\n");

	/*! Driver works fine without explicit register
	 * initialization. Furthermore, initializations takes about 2 seconds
	 * at startup...
	 */

//	max9526_write_reg(sensor, MAX9526_IRQ_MASK_0, 0x00);
//	max9526_write_reg(sensor, MAX9526_IRQ_MASK_1, 0x00);
	
	ret = max9526_write_reg(sensor, MAX9526_STD_SEL, 0x14);
	
	ret = max9526_write_reg(sensor, MAX9526_STD_SEL, 0x10);
	if (ret) 
		return(ret);
	
//	max9526_write_reg(sensor, MAX9526_CONTRAST, 0x80);
//	max9526_write_reg(sensor, MAX9526_BRIGHTNESS, 0x00);
//	max9526_write_reg(sensor, MAX9526_HUE, 0x80);
//	max9526_write_reg(sensor, MAX9526_SATURATION, 0x88);
//	max9526_write_reg(sensor, MAX9526_INPUT_SELECT, 0x02);
//	max9526_write_reg(sensor, MAX9526_GAIN_CTRL, 0x00);
//	max9526_write_reg(sensor, MAX9526_COLOR_KILL, 0x23);

	ret = max9526_write_reg(sensor, MAX9526_OUTPUT_TEST_SIGNAL, 0x02);
	if (ret) 
		return(ret);

	ret = max9526_write_reg(sensor, MAX9526_CLOCK_AND_OUTPUT, 0x04);
	if (ret) 
		return(ret);

//	max9526_write_reg(sensor, MAX9526_PLL_CONTROL, 0x03);
//	max9526_write_reg(sensor, MAX9526_MISC, 0x18);

	return (0);
}

/*! MAX9526 I2C attach function.
 *
 *  @param *adapter	struct i2c_adapter *.
 *
 *  @return		Error code indicating success or failure.
 */

/*!
 * MAX9526 I2C probe function.
 * Function set in i2c_driver struct.
 * Called by insmod.
 *
 *  @param *adapter	I2C adapter descriptor.
 *
 *  @return		Error code indicating success or failure.
 */
static int max9526_probe(struct i2c_client *client,
			 const struct i2c_device_id *id)
{
	int ret = 0;
#ifdef DEBUG
	int i, data;
#endif
	struct sensor *sensor;
	struct v4l2_int_device *v4l2_int;
	struct v4l2_int_slave *v4l2_slv;
	struct max9526_platform_data *pdata = client->dev.platform_data;

	pr_debug("In max9526_probe\n");

	if (!pdata) {
		dev_err(&client->dev, "Platform data missing!\n");
		return -EINVAL;
	}

	sensor = kzalloc(sizeof(struct sensor), GFP_KERNEL);
	if (!sensor) {
		dev_err(&client->dev, "Failed to allocate sensor data!\n");
		return -ENOMEM;
	}

	v4l2_int = &sensor->v4l2_int_device;
	v4l2_slv = &sensor->v4l2_int_slave;

	/* Set initial values for the sensor struct. */
	init_MUTEX(&sensor->mutex);

	sensor->csi = pdata->csi;
	sensor->i2c_client = client;
	sensor->streamcap.capability = V4L2_CAP_TIMEPERFRAME;
	sensor->streamcap.timeperframe.denominator = 25;
	sensor->streamcap.timeperframe.numerator = 1;
	sensor->std_id = V4L2_STD_ALL;
	sensor->video_idx = MAX9526_NOT_LOCKED;
	sensor->pix.width = video_fmts[sensor->video_idx].raw_width;
	sensor->pix.height = video_fmts[sensor->video_idx].raw_height;
	sensor->pix.pixelformat = V4L2_PIX_FMT_UYVY;  /* YUV422 */
//	sensor->pix.field = V4L2_FIELD_INTERLACED;
	
	sensor->pix.priv = 1;  /* 1 is used to indicate TV in */
	sensor->on = true;
	sensor->active_input = 0;

	gpio_sensor_active();

	dev_dbg(&sensor->i2c_client->dev,
		"%s:max9526 probe i2c address is 0x%02X \n",
		__func__, sensor->i2c_client->addr);

	/*! MAX9526 initialization. */
	ret = max9526_hard_reset(sensor);
	if (ret) {
		dev_err(&sensor->i2c_client->dev, "%s:max9526 probe i2c write error\n", __func__);
		return(ret);
	}

	i2c_set_clientdata(client, sensor);

	/* This function attaches this structure to the /dev/video0 device.
	 * The pointer in priv points to the mt9v111_data structure here.*/
	v4l2_int->priv = sensor;

	v4l2_slv->ioctls = max9526_ioctl_desc,
	v4l2_slv->num_ioctls = ARRAY_SIZE(max9526_ioctl_desc),
	snprintf(v4l2_slv->attach_to, V4L2NAMESIZE, "mxc_v4l2_cap.%d", pdata->csi);

	v4l2_int->module = THIS_MODULE;
	v4l2_int->type = v4l2_int_type_slave;
	v4l2_int->u.slave = v4l2_slv;
	snprintf(v4l2_int->name, V4L2NAMESIZE, "max9526.%d", pdata->csi);

	pr_debug("   type is %d (expect %d)\n",
		 v4l2_int->type, v4l2_int_type_slave);
	pr_debug("   num ioctls is %d\n",
		 v4l2_int->u.slave->num_ioctls);

	ret = v4l2_int_device_register(&sensor->v4l2_int_device);


#ifdef DEBUG
	for (i = 0; i <= 0x0f; i++) {
		data = max9526_read(sensor, i);
	    printk("MAX9526 Register id: %d, Value: 0x%02x \n", i, data);
	}
#endif



	return ret;
}

/*!
 * MAX9526 I2C detach function.
 * Called on rmmod.
 *
 *  @param *client	struct i2c_client*.
 *
 *  @return		Error code indicating success or failure.
 */
static int max9526_detach(struct i2c_client *client)
{
	struct sensor *sensor = i2c_get_clientdata(client);

	dev_dbg(&sensor->i2c_client->dev,
		"%s:Removing %s video decoder @ 0x%02X from adapter %s \n",
		__func__, IF_NAME, client->addr << 1, client->adapter->name);

	v4l2_int_device_unregister(&sensor->v4l2_int_device);
	kfree(sensor);

	return 0;
}

/*!
 * MAX9526 init function.
 * Called on insmod.
 *
 * @return    Error code indicating success or failure.
 */
static __init int max9526_init(void)
{
	u8 err = 0;

	pr_debug("In max9526_init\n");

	/* Tells the i2c driver what functions to call for this driver. */
	err = i2c_add_driver(&max9526_i2c_driver);
	if (err != 0)
		pr_err("%s:driver registration failed, error=%d \n",
			__func__, err);

	return err;
}

/*!
 * MAX9526 cleanup function.
 * Called on rmmod.
 *
 * @return   Error code indicating success or failure.
 */
static void __exit max9526_clean(void)
{
	pr_debug("In max9526_clean\n");
	i2c_del_driver(&max9526_i2c_driver);
	gpio_sensor_inactive();
}

module_init(max9526_init);
module_exit(max9526_clean);

MODULE_AUTHOR("Klaus Steinhammer <kst@tttech.com>");
MODULE_DESCRIPTION("Maxim MAX9526 video decoder driver");
MODULE_LICENSE("GPL");
