/*! \file smtc_ResSingleTouchProcess.c
* \brief Code to decode/process Single touch data to the input system
*
* Copyright (c) 2011 Semtech Corp
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License version 2 as
*  published by the Free Software Foundation.
*/

#include <linux/input/smtc/smtc_ResSingleTouchProcess.h>

/*! \fn int smtc_ResSingleTouch_initInput(psmtc_ResSingleTouch_t this,
 * psmtc_ResSingleTouch_platform_data_t pdata,struct device *parent,
 * int bustype
 * \brief initializer on the input for resistive Single touch
 * \param this Pointer to struct containing information to do with resisitve
 * Single touch
 * Single touch
 * \param pData Pointer to platform data specific to resistive Single touch
 * \param parent Pointer to device struct to pass as parent for input
 * \param busType integer id for the bus type used
 * \return 0 if successful, negative if unsuccessful
*/
int smtc_ResSingleTouch_initInput(psmtc_ResSingleTouch_t this,
                                    psmtc_ResSingleTouch_platform_data_t pdata,
                                     struct device *parent,int bustype)
{
  struct input_dev *input;
  int error = 0;
  error = -ENOMEM;
  if (this) {
    if (!this->input) {
    	input = input_allocate_device();
      if (input)
        input->name = "touchscreen";
    } else {
      error = 0; /* tell code to NOT register device allocated elsewhere */
      input = this->input;
    }
	  if (input && pdata) {
  	  input->id.bustype = bustype;
      input->dev.parent = parent;
	    input->evbit[0] |= /*BIT_MASK(EV_KEY) |*/ BIT_MASK(EV_ABS);
	    //input->keybit[BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH);
	    set_bit(EV_SYN, input->evbit);
      
      this->newData = 0;
      this->pPlatformData = pdata;
  	  
      /* register as singletouch device */
	    input_set_abs_params(input, ABS_X, pdata->x_axis.min, 
                                                     pdata->x_axis.max, 0, 0);
	    input_set_abs_params(input, ABS_Y, pdata->y_axis.min, 
                                                     pdata->y_axis.max, 0, 0);
	    input_set_abs_params(input, ABS_PRESSURE, 0, pdata->rt_max, 0, 0);
		
      if (error) {
  	    error = input_register_device(input);
      } else { 
        /* don't register device yet.. user may want to add to this later */
        error = 0;
      }
	    if (!error) { 
        this->input = input;
        return 0;
      }
      input_free_device(input);
    }
  }
  return error;
}

/*! \fn void smtc_ResSingleTouch_remove(psmtc_ResSingleTouch_t this)
 * \brief Unregisters input device
 * \param this Pointer to struct containing information to do with resisitve
 * Single touch
*/
void smtc_ResSingleTouch_remove(psmtc_ResSingleTouch_t this)
{
  if (this && this->input)
    input_unregister_device(this->input);
}

/*! \fn int smtc_ResSingleTouch_decode(psmtc_ResSingleTouch_t this,
 * u8 *buffer, int size)
 * \brief Decodes buffer passed in as an array of channel data.
 * \param this Pointer to struct containing information to do with resisitve
 * Single touch
 * \param buffer Array of byte data
 * \param size Size of the array being passed in
 * \return 0 if successful, -1 if error
*/
int smtc_ResSingleTouch_decode(psmtc_ResSingleTouch_t this,
                                  u8 *buffer, int size) {
	u16 data = 0;
	int i = 0;
  int ret = 0;
  struct smtc_resSTData *pdata = 0;

  if (this && buffer) {
    pdata = &this->currentData;
    size = size-1; /* This way we know will have 2 bytes per chan */
		for (i = 0; i < size; i++) {
			u16 ch;
      data = buffer[i++];
      data = data << 8;
      data = data | buffer[i];
			if (unlikely(data & 0x8000)) {
				dev_err(this->input->dev.parent, "hibit @ %d data: 0x%x\n", i>>1, data);
				ret = -1;
				continue;
			}
			ch = data >> 12;
			if (ch == SMTC_CH_X) {
				pdata->x = data & 0x0fff;
			} else if (ch == SMTC_CH_Y) {
				pdata->y = data & 0x0fff;
			} else if (ch == SMTC_CH_Z1) {
				pdata->z1 = data & 0x0fff;
			} else if (ch == SMTC_CH_Z2) {
        pdata->z2 = data & 0x0fff;
			} else {
				dev_err(this->input->dev.parent,"? %d %x\n", ch, data & 0x0fff);
				ret = -1;
			}
		}
    if ( (ret != -1) && (size!=0) )
    {
      this->newData = 1;
	    dev_dbg(this->input->dev.parent,"X: %d\tY: %d\n",pdata->x,pdata->y);
	    return ret;
    }
  }
  return -1;
}

/*! \fn void smtc_ResSingleTouch_processNoTouch((psmtc_ResSingleTouch_t this)
 * \brief Send input information for a no touch event
 * \param this Pointer to struct containing information to do with resisitve
 * Single touch
*/
void smtc_ResSingleTouch_processNoTouch(psmtc_ResSingleTouch_t this)
{
  if (this) {
    this->newData = 0;
//    input_report_key(this->input, BTN_TOUCH, 0);
    input_report_abs(this->input, ABS_PRESSURE, 0);
    input_sync(this->input);
  }
}

/*! \fn void smtc_ResSingleTouch_processTouch(psmtc_ResSingleTouch_t this)
 * \brief Process previously read touch data
 * \param this Pointer to struct containing information to do with resisitve
 * Single touch
*/
void smtc_ResSingleTouch_processTouch(psmtc_ResSingleTouch_t this)
{
	u16	     x, y, z1, z2;
	u32	     rt;
	struct input_dev *input = 0;
  psmtc_ResSingleTouch_platform_data_t pPlatformData = 0;
  struct device *dev = 0;
 
  if (this && this->newData) {
    this->newData = 0;
    input = this->input;
    if (!input) {
      printk(KERN_ERR "input null\n");
      return;
    }
    dev = input->dev.parent;
    if (!dev) {
      printk(KERN_ERR "dev.parent null\n");
      return;
    }
    pPlatformData = this->pPlatformData;
    if (!pPlatformData) {
      dev_err(dev, "error, platform data null\n");
      return;
    }
    /* ** Copy over the data points to a local variable ** */
	  x = this->currentData.x;
	  y = this->currentData.y;
	  z1 = this->currentData.z1;
	  z2 = this->currentData.z2;
    /* ******* */
   	if (likely(y && z1)) {
    		/* compute touch pressure resistance */
    		rt = z2;
    		rt -= z1;
    		rt *= y;
    		rt *= pPlatformData->y_axis.plate_ohms;
    		rt /= z1;

    		rt = (rt + 2047) >> 12;
    } else
    		rt = 1;
    /* If resistance is extremely large, don't report this set */
    if (rt >= pPlatformData->rt_max) {
      dev_dbg(dev, "ignored pressure %d\n", rt);
    	return;
    }
    /*********************************************************************/
	/* single-touch */   
 	dev_dbg(dev, "1=%d %d\n", x , y);
    
#if defined(SWAPXY)
       z1 = x;
       x = y;
       y = z1;
#endif     
#if defined (FLIPX)
        x = 4095 - x;
#endif
#if defined (FLIPY)
        y = 4095 - y;
#endif
    /* report the instrument is down */
//  	input_report_key(input, BTN_TOUCH, 1);
	input_report_abs(input, ABS_PRESSURE, rt);
	input_report_abs(input, ABS_X, x);
 	input_report_abs(input, ABS_Y, y);
	input_sync(input);
	dev_dbg(dev, "point(%4d,%4d), pressure (%4u)\n", x, y, rt);
  }
}


