/*! \file sx8654.c
 * \brief  SX8654 Touchscreen Driver
 *
 * Driver for the SX8654 Touchscreen (Multitouch + Proximity)
 * Copyright (c) 2011 Semtech Corp
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 */

#define DRIVER_NAME "sx8654"
//#define DEBUG

#include <linux/module.h>
#include <linux/slab.h>
#include <linux/i2c.h>


#include <linux/input/smtc/sx86_545556747576_Registers.h> /* reg address/fields */
#include <linux/input/smtc/sx8654_platform_data.h>  /* platform data */
#include <linux/input/smtc/sx86xx.h> /* main struct, interrupt,init,pointers */
#include <linux/input/smtc/smtc_ResSingleTouchProcess.h> /* multitouch */
#if 0
#include <linux/input/smtc/smtc_ResProximityProcess.h>  /* proximity */
#endif

/*! \struct sx8654
 * Specialized struct containing number of channels to read and touch / proximity
 * structs.
 */
struct sx8654
{
  int numEnabledTouchChannels;
  smtc_ResSingleTouch_t touch;
#if 0
  smtc_ResProximity_t   proximity;
#endif
} typedef sx8654_t, *psx8654_t;

/*! \fn static int write_command(psx86XX_t this, u8 command)
 * \brief Sends a write command to the device
 * \param this Pointer to main parent struct 
 * \param command 8-Bit command to send
 * \return Value from i2c_master_send
 */
static int write_command(psx86XX_t this, u8 command)
{
  struct i2c_client *i2c = 0;
  int returnValue = 0;
  returnValue = -ENOMEM;
  if (this && this->bus) {
    i2c = this->bus;

    returnValue = i2c_master_send(i2c,&command,1);
	  dev_dbg(&i2c->dev, "write_command Command: %d Return: %d\n",command,returnValue);
  }
  return (returnValue < 0) ? returnValue : 0;
}
/*! \fn static int write_register(psx86XX_t this, u8 address, u8 value)
 * \brief Sends a write register to the device
 * \param this Pointer to main parent struct 
 * \param address 8-bit register address
 * \param value   8-bit register value to write to address
 * \return Value from i2c_master_send
 */
static int write_register(psx86XX_t this, u8 address, u8 value)
{
  struct i2c_client *i2c = 0;
  char buffer[2];
  int returnValue = 0;
  buffer[0] = address;
  buffer[1] = value;
  returnValue = -ENOMEM;
  if (this && this->bus) {
    i2c = this->bus;

    returnValue = i2c_master_send(i2c,buffer,2);
	  dev_dbg(&i2c->dev,"write_register Address: 0x%x Value: 0x%x Return: %d\n",
        address,value,returnValue);
  }
  return returnValue;
}

/*! \fn static int read_register(psx86XX_t this, u8 address, u8 *value) 
* \brief Reads a register's value from the device
* \param this Pointer to main parent struct 
* \param address 8-Bit address to read from
* \param value Pointer to 8-bit value to save register value to 
* \return Value from i2c_smbus_read_byte_data if < 0. else 0
*/
static int read_register(psx86XX_t this, u8 address, u8 *value)
{
  struct i2c_client *i2c = 0;
  s32 returnValue = 0;
  if (this && value && this->bus) {
    i2c = this->bus;
retry:
    returnValue = i2c_smbus_read_byte_data(i2c,address|SMTC_CMD_READ);
	  dev_dbg(&i2c->dev, "read_register Address: 0x%x Return: 0x%x\n",address,returnValue);
    if (returnValue >= 0) {
      *value = returnValue;
      return 0;
    } else {
	printk("read reg failed ... retry\n");
	goto retry;
//      return returnValue;
    }
  }
  return -ENOMEM;
}
/*! \fn static int read_regStat(psx86XX_t this)
 * \brief Shortcut to read what caused interrupt.
 * \details This is to keep the drivers a unified
 * function that will read whatever register(s) 
 * provide information on why the interrupt was caused.
 * \param this Pointer to main parent struct 
 * \return If successful, Value of bit(s) that cause interrupt, else 0
 */
static int read_regStat(psx86XX_t this)
{
  u8 data = 0;
  if (this) {
    if (read_register(this,SMTC_REGIRQSRC,&data) == 0)
      return data;
  }
  return 0;
}

/*! \fn static int read_data(psx86XX_t this, u8* buffer, int bufferSize)
 * \brief Performs a read data command (for channels)
 * \param this Pointer to main parent struct 
 * \param buffer Pointer to 8-Bit array (used for storing data)
 * \param bufferSize Size of buffer
 * \return Number of bytes read (or negative if error)
 */

static int read_data(psx86XX_t this, u8* buffer, int bufferSize)
{
  int returnValue = 0;
  struct i2c_client *i2c = 0;
  if (this && buffer && this->bus)
  {
    i2c = this->bus;
    returnValue = i2c_master_recv(i2c,buffer,bufferSize);
	  dev_dbg(&i2c->dev,
        "read_data First Entry in Buffer: 0x%x Size: 0x%x Returned: 0x%x\n",
        buffer,bufferSize,returnValue);
    return returnValue;
  }
  return -ENOMEM;
}

/*! \fn static int initialize(psx86XX_t this)
 * \brief Performs all initialization needed to configure the device
 * \param this Pointer to main parent struct 
 * \return Last used command's return value (negative if error)
 */
static int initialize(psx86XX_t this)
{
  int i = 0;
  int returnValue = 0;
  psx86xx_reg_t pregisters = 0;
  if (this && this->pregisters_init) {    
    pregisters = this->pregisters_init;
    i = -1;

    /* prepare reset by disabling any irq handling */
    this->irq_disabled = 1;
    disable_irq(this->irq);
    /* perform a reset */
    write_register(this,SMTC_REGRESET,SMTC_REGRESET_VALUE);
    /* wait until the reset has finished by monitoring NIRQ */
    while(this->get_nirq_low && this->get_nirq_low());
    /* re-enable interrupt handling */
    enable_irq(this->irq);
    this->irq_disabled = 0;

    /* Write all the registers */
    while( ((++i) < this->size_registers_init_array)&&(returnValue >= 0))
      returnValue = write_register(this,pregisters[i].reg,
                                                    pregisters[i].data);
    /* If everything is ok, start device by going into Pen Trigger Mode */
    return ((returnValue >= 0) ? write_command(this,SMTC_CMD_PENTRG) :
                                                                returnValue);
  }
 return -ENOMEM;
}

/*! \fn static void noTouchProcess(psx86XX_t this)
 * \brief Handle what to do when a no touch has occured
 * \param this Pointer to main parent struct 
 */
static void noTouchProcess(psx86XX_t this)
{
  psx8654_t pDevice = 0;
u8 buffer[TOTAL_CHANNELS_AVAILABLE<<1];

  if (this && (pDevice = this->pDevice))
  {
	  dev_dbg(this->pdev, "Inside noTouchProcess()\n");
    /* No data to read, just reset touch */
    smtc_ResSingleTouch_processNoTouch(&(pDevice->touch));
read_data(this, buffer, pDevice->numEnabledTouchChannels << 1);
	  dev_dbg(this->pdev, "Leaving noTouchProcess()\n");
  }
}

/*! \fn static void touchProcess(psx86XX_t this)
 * \brief Handle what to do when a touch occurs
 * \param this Pointer to main parent struct 
 */
static void touchProcess(psx86XX_t this)
{
  u8 buffer[TOTAL_CHANNELS_AVAILABLE<<1];
  int tempReturned = 0;
  psx8654_t pDevice = 0;

  if (this && (pDevice = this->pDevice))
  {
    
	  dev_dbg(this->pdev, "Inside touchProcess()\n");
    /* Check if we need to process any stored data */
    smtc_ResSingleTouch_processTouch(&(pDevice->touch));
    /* Now read pending channel data */
    tempReturned = read_data(this, buffer, pDevice->numEnabledTouchChannels << 1);
    if (tempReturned == pDevice->numEnabledTouchChannels << 1) // > 0)
    {
      /* take read data and process it */
      smtc_ResSingleTouch_decode(&(pDevice->touch),buffer,tempReturned);
/*      if (this->get_nirq_low())
	{
		printk("IRQ still low, reg_stat=%x\n", read_regStat(this));
		goto reread;
	}*/
    } else
	{
	printk("touchProcess failed read_data\n");
	}
	  dev_dbg(this->pdev, "Leaving touchProcess()\n");
  }
}

#if 0
/*! \fn static void outProxProcess(psx86XX_t this)
 * \brief Handle what to do when we are no longer in proximity
 * \param this Pointer to main parent struct 
 */
static void outProxProcess(psx86XX_t this)
{
  u8 data = 0;
  u16 fullData = 0;
  psx8654_t pDevice = 0;
  if (this && (pDevice = this->pDevice)) {
    /* read MSB for useful */
    if (read_register(this,SMTC_REGPROX10,&data) == 0) {
      fullData = data;
      fullData = fullData << 8;
      /* read LSB for useful */
      if (read_register(this,SMTC_REGPROX11,&data) == 0) {
        fullData = fullData | data;
        if (fullData & 0x800)
        {
          fullData |= 0xF800;
        }
      }
    }
    /* process out of proximity. Pass in current useful data in case used */
    smtc_ResProximity_processOutProx(&(pDevice->proximity),fullData);
  }
}

/*! \fn static void inProxProcess(psx86XX_t this)
 * \brief Handle what to do when we go into proximity
 * \param this Pointer to main parent struct 
 */
static void inProxProcess(psx86XX_t this)
{
  u8 data = 0;
  u16 fullData = 0;
  psx8654_t pDevice = 0;
  if (this && (pDevice = this->pDevice)) {
    dev_dbg(this->pdev, "Inside inProxProcess()\n");
    /* read MSB for useful */
    if (read_register(this,SMTC_REGPROX10,&data) == 0)
    {
      fullData = data;
      fullData = fullData << 8;
      /* read LSB for useful */
      if (read_register(this,SMTC_REGPROX11,&data) == 0)
      {
        fullData = fullData | data;
        if (fullData & 0x800)
        {
          fullData |= 0xF800;
        }
      }
    }
    /* process in proximity. Pass in current useful data in case used */
    smtc_ResProximity_processInProx(&(pDevice->proximity),fullData);
	  dev_dbg(this->pdev, "Leaving inProxProcess()\n");
  }
}

/*! \fn static void determineProxProcess(psx86XX_t this, u8 command)
 * \brief Determine through another register if in or out of prox
 * \param this Pointer to main parent struct 
 */
static void determineProxProcess(psx86XX_t this)
{
  u8 data = 0;
  if (this)
  {
    /* RegStat will give a better picture of the interrupt. Depending on the
     * setup for interrupts, we may need this to fully understand if its an
     * in prox or out of prox.
     */
    if (read_register(this,SMTC_REGSTAT,&data) == 0)
    {
	    dev_dbg(this->pdev, "regstat checking prox: 0x%x\n",data);
      if (data&SMTC_PROXSTAT)
      {
        inProxProcess(this);
      }
      else
      {
        outProxProcess(this);
      }
    }
  }
}
#endif

/*! \fn static int sx8654_probe(struct i2c_client *client, const struct i2c_device_id *id)
 * \brief Probe function
 * \param client pointer to i2c_client
 * \param id pointer to i2c_device_id
 * \return Whether probe was successful
 */
static int sx8654_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
  psx86XX_t this = 0;
  psx8654_t pDevice = 0;
	struct sx8654_platform_data *pplatData = 0;
  
	dev_info(&client->dev, "probe()\n");

  pplatData = client->dev.platform_data;
	if (!pplatData) {
		dev_err(&client->dev, "platform data is required!\n");
		return -EINVAL;
	}

	if (!i2c_check_functionality(client->adapter,
				     I2C_FUNC_SMBUS_READ_WORD_DATA))
		return -EIO;

  this = kzalloc(sizeof(sx86XX_t), GFP_KERNEL); /* create memory for main struct */

  if (this)
  {
    /* In case we need to reinitialize data 
     * (e.q. if suspend reset device) */
    this->init = initialize;
    /* Write Command, 1 byte */
    this->write_command = write_command;
    /* Writing to registers */
    this->write_register = write_register;
    /* Reading from registers */
    this->read_register = read_register;
    /* Read in channel data, array of bytes */
    this->read_channelData = read_data;
    /* shortcut to read status of interrupt */
    this->refreshStatus = read_regStat;
    /* pointer to function from platform data to get pendown 
     * (1->NIRQ=0, 0->NIRQ=1) */
    this->get_nirq_low = pplatData->get_pendown_state;
    /* save irq in case we need to reference it */
    this->irq = client->irq;
    /* older models required a timer for penup
     * (also if we set RATE=0, then need this) */
    this->useIrqTimer = 0;

    /* Setup function to call on corresponding reg irq source bit */
    if (MAX_NUM_STATUS_BITS>= 8)
    {
      this->statusFunc[0] = 0;
      this->statusFunc[1] = 0;
      this->statusFunc[2] = noTouchProcess;
      this->statusFunc[3] = touchProcess;
      this->statusFunc[4] = 0;
#if 0
      this->statusFunc[5] = determineProxProcess;
      this->statusFunc[6] = determineProxProcess;
#endif
      this->statusFunc[7] = 0;
    }

    /* Setup initailization registers from platform data */
    this->size_registers_init_array = pplatData->size_init_array;
    this->pregisters_init = &(pplatData->init_array[0]);

    /* setup i2c communication */
	  this->bus = client;
	  i2c_set_clientdata(client, this);

    /* record device struct */
    this->pdev = &client->dev;
	 
    /* create memory for device specific struct */
    this->pDevice = pDevice = kzalloc(sizeof(struct sx8654), GFP_KERNEL);

    if (pDevice)
    {
      pplatData->init_platform_hw();
 
      pDevice->numEnabledTouchChannels = pplatData->numTouchChannelsEnabled;
      /* initialize multitouch */
      smtc_ResSingleTouch_initInput(&pDevice->touch,&pplatData->touchPlatData,
                                                          this->pdev,BUS_I2C);
#if 0
      /* initialize proximity */
      smtc_ResProximity_initInput(&pDevice->proximity,this->pdev,BUS_I2C);
#endif
    }
    return sx86XX_init(this);
  }
  return 0;
}

/*! \fn static int sx8654_remove(struct i2c_client *client)
 * \brief Called when device is to be removed
 * \param client Pointer to i2c_client struct
 * \return Value from sx86XX_remove()
 */
static int sx8654_remove(struct i2c_client *client)
{
	struct sx8654_platform_data *pplatData =0;
  psx8654_t pDevice = 0;
	psx86XX_t this = i2c_get_clientdata(client);
  if (this && this->pDevice)
  {
    pDevice = this->pDevice;
    pplatData = client->dev.platform_data;
#if 0
    smtc_ResProximity_remove(&pDevice->proximity);
#endif
    smtc_ResSingleTouch_remove(&pDevice->touch);
    pplatData->exit_platform_hw();
    kfree(this->pDevice);
  }
	return sx86XX_remove(this);
}

/*====================================================*/
/***** Kernel Suspend *****/
static int sx8654_suspend(struct i2c_client *client)
{
  psx86XX_t this = i2c_get_clientdata(client);
  sx86XX_suspend(this);
  return 0;
}
/***** Kernel Resume *****/
static int sx8654_resume(struct i2c_client *client)
{
  psx86XX_t this = i2c_get_clientdata(client);
  sx86XX_resume(this);
  return 0;
}
/*====================================================*/
static struct i2c_device_id sx8654_idtable[] = {
	{ DRIVER_NAME, 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, sx8654_idtable);
static struct i2c_driver sx8654_driver = {
	.driver = {
		.owner  = THIS_MODULE,
		.name   = DRIVER_NAME
	},
	.id_table = sx8654_idtable,
	.probe	  = sx8654_probe,
	.remove	  = __devexit_p(sx8654_remove),
#if defined(USE_KERNEL_SUSPEND)
  .suspend  = sx8654_suspend, 
  .resume   = sx8654_resume,
#endif
};
static int __init sx8654_init(void)
{
	return i2c_add_driver(&sx8654_driver);
}
static void __exit sx8654_exit(void)
{
	i2c_del_driver(&sx8654_driver);
}

module_init(sx8654_init);
module_exit(sx8654_exit);

MODULE_AUTHOR("Semtech Corp.");
MODULE_DESCRIPTION("SX8654 TouchScreen Driver");
MODULE_LICENSE("GPL");


