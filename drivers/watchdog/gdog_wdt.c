/*
 * gdog_wdt.c - a generic watchdog driver for processor supervisory circuits like Texas TPS382x
 *
 *	(c) Copyright 2010 TTTech Computertechnik AG 
 *	    Based on SoftDog driver by Alan Cox <alan@lxorguk.ukuu.org.uk>
 *            and sa1100_wdt.c by Oleg Drokin <green@crimea.edu>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version
 *	2 of the License, or (at your option) any later version.
 *
 */
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/watchdog.h>
#include <linux/init.h>
#include <linux/bitops.h>
#include <linux/uaccess.h>
#include <linux/timex.h>
#include <linux/freezer.h>
#include <linux/sched.h>
#include <linux/delay.h>


#define SW_INIT_TIMEOUT		60
#define HW_TIMEOUT_MS		900 
#define HW_TIMOUT_CYCLES	(HW_TIMEOUT_MS/2)

static unsigned long gdog_file_open = 0;
static volatile unsigned int sw_timeout; /* in HW_TIMOUT_CYCLES */
static volatile unsigned int remaining_cycles;

static volatile u32 gdog_task_installed;

extern int gpio_wdt_toggle(void);
extern int gpio_wdt_init(void);

static int gdog_thread(void *arg)
{	
	gpio_wdt_init();
	daemonize("gdog_thread");
	while (gdog_task_installed) {
		try_to_freeze();
		if (remaining_cycles)
				gpio_wdt_toggle();
		if ((gdog_file_open & 0x02) && (remaining_cycles))
			remaining_cycles --;
		msleep(HW_TIMOUT_CYCLES);
	}
	return(0);
}

/*
 *	Allow only one person to hold it open
 */
static int gdog_open(struct inode *inode, struct file *file)
{
	if (test_and_set_bit(0, &gdog_file_open))
		return -EBUSY;
		
	gdog_file_open |= 0x02;

	return nonseekable_open(inode, file);
}

/*
 * The watchdog cannot be disabled.
 */
static int gdog_release(struct inode *inode, struct file *file)
{
//	printk(KERN_CRIT "WATCHDOG: Device closed - timer will not stop\n");
	clear_bit(0, &gdog_file_open);
	return 0;
}

static ssize_t gdog_write(struct file *file, const char __user *data,
						size_t len, loff_t *ppos)
{
	if (len) {
		/* Refresh timer. */
		remaining_cycles = sw_timeout;
	}
	return len;
}

static const struct watchdog_info ident = {
	.options	= WDIOF_SETTIMEOUT | WDIOF_KEEPALIVEPING,
	.identity	= "generic Watchdog",
	.firmware_version	= 1,
};

static long gdog_ioctl(struct file *file, unsigned int cmd,
							unsigned long arg)
{
	int ret = -ENOTTY;
	int time;
	void __user *argp = (void __user *)arg;
	int __user *p = argp;

	switch (cmd) {
	case WDIOC_GETSUPPORT:
		ret = copy_to_user(argp, &ident,
				   sizeof(ident)) ? -EFAULT : 0;
		break;

	case WDIOC_GETSTATUS:
		ret = put_user(0, p);
		break;

	case WDIOC_KEEPALIVE:
		remaining_cycles = sw_timeout;
		ret = 0;
		break;

	case WDIOC_SETTIMEOUT:
		ret = get_user(time, p);
		if (ret)
			break;

		if (time <= 0 || ((((long long)(time*1000))/HW_TIMOUT_CYCLES) >= 0xffffffff)) {
			ret = -EINVAL;
			break;
		}

		sw_timeout = ((long long)(time*1000))/HW_TIMOUT_CYCLES;
		remaining_cycles = sw_timeout;
		/*fall through*/

	case WDIOC_GETTIMEOUT:
		ret = put_user((remaining_cycles*HW_TIMOUT_CYCLES)/1000, p);
		break;
	}
	return ret;
}

static const struct file_operations gdog_fops = {
	.owner		= THIS_MODULE,
	.llseek		= no_llseek,
	.write		= gdog_write,
	.unlocked_ioctl	= gdog_ioctl,
	.open		= gdog_open,
	.release	= gdog_release,
};

static struct miscdevice gdog_miscdev = {
	.minor		= WATCHDOG_MINOR,
	.name		= "watchdog",
	.fops		= &gdog_fops,
};

static int __init gdog_init(void)
{
	int ret;

	ret = misc_register(&gdog_miscdev);
	if (ret == 0) {
		printk(KERN_INFO "generic Watchdog Timer: timer margin %d sec\n", remaining_cycles);

	}
	return ret;
}

static int __init gdog_thread_init(void)
{
	sw_timeout = ((SW_INIT_TIMEOUT*1000)/HW_TIMOUT_CYCLES);	 
	remaining_cycles = sw_timeout;
	gdog_task_installed = 1;

	kernel_thread(gdog_thread, NULL, CLONE_VM | CLONE_FS);
	
	return(0);
}

device_initcall(gdog_init);
arch_initcall(gdog_thread_init);



MODULE_AUTHOR("Klaus Steinhammer <kst@tttech.com>");
MODULE_DESCRIPTION("generic watchdog driver");

MODULE_LICENSE("GPL");
MODULE_ALIAS_MISCDEV(WATCHDOG_MINOR);
