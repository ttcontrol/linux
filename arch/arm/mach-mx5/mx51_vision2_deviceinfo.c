#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/proc_fs.h>

extern int vision2_deviceid;
extern int vision2_sw_release, vision2_hw_revision;
extern char vision2_mode;
extern char vision2_deviceserial[32];
extern char vision2_boardserial[32];

static struct proc_dir_entry *vision2_dir, *serial_dir, *id_file,
        *deviceserial_file, *boardserial_file, *mode_file, *release_file,
	*revision_file;

static int proc_read_id(char *page, char **start,
	off_t off, int count, int *eof, void *data)
{
	return scnprintf(page, count, "%.4x", vision2_deviceid);
}

static int proc_read_serial_device(char *page, char **start,
	off_t off, int count, int *eof, void *data)
{
	return scnprintf(page, count, "%s", vision2_deviceserial);
}

static int proc_read_serial_board(char *page, char **start,
	off_t off, int count, int *eof, void *data)
{
	return scnprintf(page, count, "%s", vision2_boardserial);
}

static int proc_read_mode(char *page, char **start,
	off_t off, int count, int *eof, void *data)
{
	return scnprintf(page, count, "%u", vision2_mode);
}

static int proc_read_release(char *page, char **start,
	off_t off, int count, int *eof, void *data)
{
	return scnprintf(page, count, "%.8x", vision2_sw_release);
}

static int proc_read_revision(char *page, char **start,
	off_t off, int count, int *eof, void *data)
{
	return scnprintf(page, count, "%u", vision2_hw_revision);
}

static int __init init_vision2_deviceinfo(void)
{
        int rv = 0;

        vision2_dir = proc_mkdir("vision2", NULL);
        if(vision2_dir == NULL) {
                rv = -ENOMEM;
                goto out;
        }
	serial_dir = proc_mkdir("serial", vision2_dir);
        
        id_file = create_proc_read_entry("id", 0444,
		vision2_dir, proc_read_id, NULL);
        if(id_file == NULL) {
                rv  = -ENOMEM;
                goto no_id;
        }

        deviceserial_file = create_proc_read_entry("device", 0444,
		serial_dir, proc_read_serial_device, NULL);
	boardserial_file = create_proc_read_entry("board", 0444,
		serial_dir, proc_read_serial_board, NULL);
        if ((deviceserial_file == NULL) || (boardserial_file == NULL)) {
                rv = -ENOMEM;
                goto no_serial;
        }

	mode_file = create_proc_read_entry("mode", 0444,
		vision2_dir, proc_read_mode, NULL);
	if (mode_file == NULL) {
		rv = -ENOMEM;
		goto no_mode;
	}

	release_file = create_proc_read_entry("release", 0444,
		vision2_dir, proc_read_release, NULL);
	if (release_file == NULL) {
		rv = -ENOMEM;
		goto no_release;
	}

	revision_file = create_proc_read_entry("revision", 0444,
		vision2_dir, proc_read_revision, NULL);
	if (revision_file == NULL)
	{
		rv = -ENOMEM;
		goto no_revision;
	}

        return 0;

no_revision:
	remove_proc_entry("revision", vision2_dir);
no_release:
	remove_proc_entry("mode", vision2_dir);
no_mode:
	remove_proc_entry("board", serial_dir);
	remove_proc_entry("device", serial_dir);
no_serial:
        remove_proc_entry("id", vision2_dir);
no_id:                           
	remove_proc_entry("serial", vision2_dir);
        remove_proc_entry("vision2", NULL);
out:
        return rv;
}

static void __exit cleanup_vision2_deviceinfo(void)
{
	remove_proc_entry("revision", vision2_dir);
	remove_proc_entry("release", vision2_dir);
        remove_proc_entry("id", vision2_dir);
	remove_proc_entry("board", serial_dir);
	remove_proc_entry("device", serial_dir);
        remove_proc_entry("serial", vision2_dir);
	remove_proc_entry("mode", vision2_dir);
        remove_proc_entry("vision2", NULL);
}

module_init(init_vision2_deviceinfo);
module_exit(cleanup_vision2_deviceinfo);

MODULE_AUTHOR("Oliver Dillinger");
MODULE_DESCRIPTION("Vision2 device info driver");

