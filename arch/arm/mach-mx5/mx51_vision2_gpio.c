/*
 * Copyright 2007-2009 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

#include <linux/errno.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/delay.h>
#include <linux/reboot.h>
#include <mach/hardware.h>
#include <mach/gpio.h>
#include <asm/io.h>

#include "iomux.h"
#include "crm_regs.h"
#include "mx51_pins.h"

/*!
 * @file mach-mx51/mx51_vision2_gpio.c
 *
 * @brief This file contains all the GPIO setup functions for the board.
 *
 * @ingroup GPIO
 */

static int keyb_backlight_value;

static struct mxc_iomux_pin_cfg __initdata mxc_iomux_pins[] = {
/* NOR Flash start */
/* NOR Flash is configured per default - no reconfiguration necessary */
#if 0
	/* EIM_AD0-AD15 is configured to EIM per default */
	/* EIM_EB0-EB1 is configured to EIM per default */
	
//	{ MX51_PIN_EIM_CS0, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_EIM_CS1, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_EIM_OE, 				IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_EIM_LBA, 			IOMUX_CONFIG_ALT0, },
/*	{ MX51_PIN_EIM_RW, 				IOMUX_CONFIG_ALT0, }, initializing this pin does not work because of NON_MUX pin*/
/* NOR Flash end */
#endif

/* USB Host 2 start */
	{ MX51_PIN_EIM_D16, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_EIM_D17, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_EIM_D18, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_EIM_D19, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_EIM_D20, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_EIM_D21, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_EIM_D22, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_EIM_D23, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_EIM_A24, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_EIM_A25, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_EIM_A26, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_EIM_A27, 			IOMUX_CONFIG_ALT2, },
/* USB Host 2 end */

/* I2C start */
#if defined(CONFIG_I2C_GPIO) || defined(CONFIG_I2C_GPIO_MODULE)

#else
	{ MX51_PIN_EIM_D24, 			IOMUX_CONFIG_ALT4, (PAD_CTL_SRE_SLOW | PAD_CTL_DRV_HIGH | PAD_CTL_ODE_OPENDRAIN_ENABLE | PAD_CTL_22K_PU | PAD_CTL_HYS_ENABLE | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL), },	/* I2C SDA */
	{ MX51_PIN_EIM_D27, 			IOMUX_CONFIG_ALT4, (PAD_CTL_SRE_SLOW | PAD_CTL_DRV_HIGH | PAD_CTL_ODE_OPENDRAIN_ENABLE | PAD_CTL_22K_PU | PAD_CTL_HYS_ENABLE | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL), },	/* I2C SCK */
#endif
/* I2C end */

/* Audio start */
	{ MX51_PIN_EIM_D28, 			IOMUX_CONFIG_ALT5, },
	{ MX51_PIN_EIM_D29, 			IOMUX_CONFIG_ALT5, },
	{ MX51_PIN_EIM_D30, 			IOMUX_CONFIG_ALT5, },
	{ MX51_PIN_EIM_D31, 			IOMUX_CONFIG_ALT5, },
/* Audio end */

/* Ethernet start */
	{ MX51_PIN_NANDF_D8, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_NANDF_D9, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_NANDF_D11, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_NANDF_RB2, 			IOMUX_CONFIG_ALT1, },
	{ MX51_PIN_NANDF_RB3, 			IOMUX_CONFIG_ALT1, },
	{ MX51_PIN_NANDF_CS2, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_NANDF_CS3, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_NANDF_CS4, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_NANDF_CS5, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_NANDF_CS6, 			IOMUX_CONFIG_ALT2, },
	{ MX51_PIN_NANDF_CS7, 			IOMUX_CONFIG_ALT1, }, /* configured per default */
	{ MX51_PIN_NANDF_RDY_INT, 		IOMUX_CONFIG_ALT1, },
	{ MX51_PIN_EIM_EB2, 			IOMUX_CONFIG_ALT3, },
	{ MX51_PIN_EIM_EB3, 			IOMUX_CONFIG_ALT3, },
	{ MX51_PIN_EIM_CS2, 			IOMUX_CONFIG_ALT3, },
	{ MX51_PIN_EIM_CS3, 			IOMUX_CONFIG_ALT3, },
	{ MX51_PIN_EIM_CS4, 			IOMUX_CONFIG_ALT3, },
	{ MX51_PIN_EIM_CS5, 			IOMUX_CONFIG_ALT3, },
/* Ethernet end */

/* NAND Flash start */
/* NAND Flash is configured per default - no reconfiguration necessary */
#if 0
//	{ MX51_PIN_NANDF_WE_B, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_NANDF_RE_B, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_NANDF_ALE, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_NANDF_CLE, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_NANDF_WP_B, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_NANDF_RB0, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_NANDF_RB1, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_NANDF_CS0, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_NANDF_CS1, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_NANDF_D7, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_NANDF_D6, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_NANDF_D5, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_NANDF_D4, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_NANDF_D3, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_NANDF_D2, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_NANDF_D1, 			IOMUX_CONFIG_ALT0, },
//t	{ MX51_PIN_NANDF_D0, 			IOMUX_CONFIG_ALT0, },
/* NAND Flash end */
#endif

/* Video IN 1 start */
/* Video IN 1 is configured per default - no reconfiguration necessary */
#if 0
//	{ MX51_PIN_CSI1_D12, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI1_D13, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI1_D14, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI1_D15, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI1_D16, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI1_D17, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI1_D18, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI1_D19, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI1_VSYNC, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI1_HSYNC, 			IOMUX_CONFIG_ALT0, },
/*	{ MX51_PIN_CSI1_PIXCLK, 		IOMUX_CONFIG_ALT0, }, initializing this pin does not work because of NON_MUX pin*/
/* Video IN 1 end */
#endif

/* Video IN 2 start */
/* Video IN 2 is configured per default - no reconfiguration necessary */
#if 0
//	{ MX51_PIN_CSI2_D12, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI2_D13, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI2_D14, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI2_D15, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI2_D16, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI2_D17, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI2_D18, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI2_D19, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI2_VSYNC, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI2_HSYNC, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_CSI2_PIXCLK, 		IOMUX_CONFIG_ALT0, },
/* Video IN 2 end */
#endif

/* PMIc SPI start */
	{ MX51_PIN_CSPI1_MOSI, 			IOMUX_CONFIG_ALT0, },
	{ MX51_PIN_CSPI1_MISO, 			IOMUX_CONFIG_ALT0, },
	{ MX51_PIN_CSPI1_SS0, 			IOMUX_CONFIG_ALT0, },
        /*
         * Configure SS1 as GPIO because of long uninterrupted SPI
         * transmissions -- CS must be high during these
         */
	{ MX51_PIN_CSPI1_SS1, 			IOMUX_CONFIG_ALT3, (PAD_CTL_SRE_FAST | PAD_CTL_DRV_MAX | PAD_CTL_PKE_ENABLE | PAD_CTL_HYS_ENABLE)},
	{ MX51_PIN_CSPI1_SCLK, 			IOMUX_CONFIG_ALT0, },
	{ MX51_PIN_DI1_PIN11, 			IOMUX_CONFIG_ALT7, (PAD_CTL_SRE_FAST | PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: PMIC_SPI_CS2n */
/* PMIc SPI end */

/* UART 1 start */
	{ MX51_PIN_UART1_RXD, 			IOMUX_CONFIG_ALT0, },
	{ MX51_PIN_UART1_TXD, 			IOMUX_CONFIG_ALT0, },
/* UART 1 end */
	
/* UART 2 start */
	{ MX51_PIN_UART2_RXD, 			IOMUX_CONFIG_ALT0, (PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_HYS_ENABLE), MUX_IN_UART2_IPP_UART_RXD_MUX_SELECT_INPUT, INPUT_CTL_PATH2},
	{ MX51_PIN_UART2_TXD, 			IOMUX_CONFIG_ALT0, },
/* UART 2 end */
	
/* UART 3 start */
	{ MX51_PIN_EIM_D25, 			IOMUX_CONFIG_ALT3, },
	{ MX51_PIN_EIM_D26, 			IOMUX_CONFIG_ALT3, },
/* UART 3 end */

/* Keyboard start */
	{ MX51_PIN_KEY_ROW0, 			IOMUX_CONFIG_ALT0, (PAD_CTL_SRE_FAST | PAD_CTL_DRV_HIGH | PAD_CTL_22K_PU | PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_HYS_ENABLE), }, /* enable 22k PullUP */
	{ MX51_PIN_KEY_ROW1, 			IOMUX_CONFIG_ALT0, (PAD_CTL_SRE_FAST | PAD_CTL_DRV_HIGH | PAD_CTL_22K_PU | PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_HYS_ENABLE), }, /* enable 22k PullUP */
	{ MX51_PIN_KEY_COL0, 			IOMUX_CONFIG_ALT0, (PAD_CTL_SRE_FAST | PAD_CTL_DRV_HIGH | PAD_CTL_22K_PU | PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_HYS_ENABLE), }, /* enable 22k PullUP */
	{ MX51_PIN_KEY_COL1, 			IOMUX_CONFIG_ALT0, (PAD_CTL_SRE_FAST | PAD_CTL_DRV_HIGH | PAD_CTL_22K_PU | PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_HYS_ENABLE), }, /* enable 22k PullUP */
	{ MX51_PIN_KEY_COL2, 			IOMUX_CONFIG_ALT0, (PAD_CTL_SRE_FAST | PAD_CTL_DRV_HIGH | PAD_CTL_22K_PU | PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_HYS_ENABLE), }, /* enable 22k PullUP */
	{ MX51_PIN_KEY_COL3, 			IOMUX_CONFIG_ALT0, (PAD_CTL_SRE_FAST | PAD_CTL_DRV_HIGH | PAD_CTL_22K_PU | PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_HYS_ENABLE), }, /* enable 22k PullUP */
	{ MX51_PIN_KEY_COL4, 			IOMUX_CONFIG_ALT0, (PAD_CTL_SRE_FAST | PAD_CTL_DRV_HIGH | PAD_CTL_22K_PU | PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_HYS_ENABLE), }, /* enable 22k PullUP */
/* Keyboard end */

/* USB Host 1 start */
	{ MX51_PIN_USBH1_CLK, 			IOMUX_CONFIG_ALT0, },
	{ MX51_PIN_USBH1_DIR, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_USBH1_STP, 			IOMUX_CONFIG_ALT0, }, /* configured by USB driver */
	{ MX51_PIN_USBH1_NXT, 			IOMUX_CONFIG_ALT0, },
	{ MX51_PIN_USBH1_DATA0, 		IOMUX_CONFIG_ALT0, },
	{ MX51_PIN_USBH1_DATA1, 		IOMUX_CONFIG_ALT0, },
	{ MX51_PIN_USBH1_DATA2, 		IOMUX_CONFIG_ALT0, },
	{ MX51_PIN_USBH1_DATA3, 		IOMUX_CONFIG_ALT0, },
	{ MX51_PIN_USBH1_DATA4, 		IOMUX_CONFIG_ALT0, },
	{ MX51_PIN_USBH1_DATA5, 		IOMUX_CONFIG_ALT0, },
	{ MX51_PIN_USBH1_DATA6, 		IOMUX_CONFIG_ALT0, },
	{ MX51_PIN_USBH1_DATA7, 		IOMUX_CONFIG_ALT0, },
/* USB Host 1 end */

/* Display 1 start */
/* Display 1 is configured per default - no reconfiguration necessary */
#if 0
//	{ MX51_PIN_DISP1_DAT0, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT1, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT2, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT3, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT4, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT5, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT6, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT7, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT8, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT9, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT10, 		IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT11, 		IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT12, 		IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT13, 		IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT14, 		IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT15, 		IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT16, 		IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP1_DAT17, 		IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DI1_PIN3, 			IOMUX_CONFIG_ALT0, },
/*	{ MX51_PIN_DI1_DISP_CLK, 		IOMUX_CONFIG_ALT0, }, initializing this pin does not work because of NON_MUX pin*/
//	{ MX51_PIN_DI1_PIN2, 			IOMUX_CONFIG_ALT0, },
/*	{ MX51_PIN_DI1_PIN15, 			IOMUX_CONFIG_ALT0, }, initializing this pin does not work because of NON_MUX pin*/
/* Display 1 end */
#endif

/* Display 2 start */
//	{ MX51_PIN_DISP2_DAT0, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP2_DAT1, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP2_DAT2, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP2_DAT3, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP2_DAT4, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP2_DAT5, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP2_DAT6, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP2_DAT7, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP2_DAT8, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP2_DAT9, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP2_DAT10, 		IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP2_DAT11, 		IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP2_DAT12, 		IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP2_DAT13, 		IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP2_DAT14, 		IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DISP2_DAT15, 		IOMUX_CONFIG_ALT0, },
	{ MX51_PIN_DISP1_DAT22, 		IOMUX_CONFIG_ALT5, },
	{ MX51_PIN_DISP1_DAT23, 		IOMUX_CONFIG_ALT5, },
//	{ MX51_PIN_DI2_PIN2, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DI2_PIN3, 			IOMUX_CONFIG_ALT0, },
//	{ MX51_PIN_DI2_DISP_CLK, 		IOMUX_CONFIG_ALT0, },
	{ MX51_PIN_DI_GP4, 				IOMUX_CONFIG_ALT4, },
/* Display 2 end */

/* XC SPI start */
//	{ MX51_PIN_SD2_DATA0, 			IOMUX_CONFIG_ALT2, (PAD_CTL_DRV_VOT_HIGH | PAD_CTL_100K_PU | PAD_CTL_HYS_ENABLE | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL ), MUX_IN_CSPI_IPP_IND_MISO_SELECT_INPUT, INPUT_CTL_PATH3}, /* XC2287 SPI MISO */
//	{ MX51_PIN_SD2_DATA3, 			IOMUX_CONFIG_ALT2, (PAD_CTL_DRV_VOT_HIGH | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST ), MUX_IN_CSPI_IPP_IND_SS_B_2_SELECT_INPUT, INPUT_CTL_PATH1}, /* XC2287 SPI SS2 */
/* XC SPI end */

/* Special Purpose */
//	{ MX51_PIN_GPIO1_2, 			IOMUX_CONFIG_ALT1, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: CPU_PWM_1 */
/* todo: enable watchdog output */
/*	{ MX51_PIN_GPIO1_4, 			IOMUX_CONFIG_ALT2, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), },*/ /* output: WDOG_RESETn */ 
	{ MX51_PIN_GPIO1_8, 			IOMUX_CONFIG_ALT4, (PAD_CTL_SRE_FAST | PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: CLK_24MHz_CPU */


//	{ MX51_PIN_PMIC_INT_REQ, 		IOMUX_CONFIG_ALT0, },


#if !(defined(CONFIG_I2C_GPIO) || defined(CONFIG_I2C_GPIO_MODULE))

/* DAB_I2C (GPIO) */
	{ MX51_PIN_UART1_RTS, 			IOMUX_CONFIG_ALT3, (PAD_CTL_SRE_SLOW | PAD_CTL_DRV_HIGH | PAD_CTL_ODE_OPENDRAIN_ENABLE | PAD_CTL_22K_PU | PAD_CTL_HYS_ENABLE | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL)}, // DAB_I2C_SDA GPIO (GPIO4_30)
	{ MX51_PIN_UART1_CTS, 			IOMUX_CONFIG_ALT3, (PAD_CTL_SRE_SLOW | PAD_CTL_DRV_HIGH | PAD_CTL_ODE_OPENDRAIN_ENABLE | PAD_CTL_22K_PU | PAD_CTL_HYS_ENABLE | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL)}, // DAB_I2C_SCL GPIO (GPIO4_31)

#endif
	
/* DAB_I2C (HW) */
//	{ MX51_PIN_SD2_CMD, 			(IOMUX_CONFIG_ALT2|IOMUX_CONFIG_SION), (PAD_CTL_DRV_VOT_HIGH | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST ), MUX_IN_CSPI_IPP_IND_MOSI_SELECT_INPUT, INPUT_CTL_PATH3}, /* DAB_I2C_SCL HW */
//	{ MX51_PIN_SD2_CLK, 			(IOMUX_CONFIG_ALT2|IOMUX_CONFIG_SION), (PAD_CTL_DRV_VOT_HIGH | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST ), MUX_IN_CSPI_IPP_CSPI_CLK_IN_SELECT_INPUT, INPUT_CTL_PATH3}, /* DAB_I2C_SDA HW */


/* GPIOs */
//	{ MX51_PIN_GPIO1_3, 			IOMUX_CONFIG_ALT0, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* input: VBAT_EN_KL_30 */ // init in u-boot
 	{ MX51_PIN_GPIO1_9, 			IOMUX_CONFIG_ALT0, (PAD_CTL_HYS_ENABLE | PAD_CTL_100K_PU | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL), }, /* input: EXPANSION_IRQ */ 

 	{ MX51_PIN_AUD3_BB_TXD, 		IOMUX_CONFIG_ALT3, (PAD_CTL_HYS_ENABLE | PAD_CTL_100K_PU | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL), }, /* input: FRONT_IRQ */ 
 	{ MX51_PIN_AUD3_BB_RXD, 		IOMUX_CONFIG_ALT3, (PAD_CTL_HYS_ENABLE | PAD_CTL_100K_PU | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL), }, /* input: DAB_IRQ */ 

//	{ MX51_PIN_DI1_PIN12, 			IOMUX_CONFIG_ALT4, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: DAB_DISP_EN */ // init in u-boot
//	{ MX51_PIN_DI1_PIN13, 			IOMUX_CONFIG_ALT4, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: WDOG_TRIGGER */ // init in u-boot
//	{ MX51_PIN_DI1_D0_CS, 			IOMUX_CONFIG_ALT4, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: DISP2_TX_EN */ // init in u-boot
//	{ MX51_PIN_DI1_D1_CS, 			IOMUX_CONFIG_ALT4, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: DAB_BACKLIGHT_EN */ // init in u-boot
//	{ MX51_PIN_DISPB2_SER_DIN, 		IOMUX_CONFIG_ALT4, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: OUTPUT 0 */ // init in u-boot
//	{ MX51_PIN_DISPB2_SER_DIO, 		IOMUX_CONFIG_ALT4, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: OUTPUT 1 */ // init in u-boot
//	{ MX51_PIN_DISPB2_SER_CLK, 		IOMUX_CONFIG_ALT4, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: DISP2_GPIO */ // init in u-boot
//	{ MX51_PIN_DISPB2_SER_RS, 		IOMUX_CONFIG_ALT4, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: CAM_EN */ // init in u-boot

	{ MX51_PIN_GPIO1_5, 			IOMUX_CONFIG_ALT0, (PAD_CTL_HYS_ENABLE | PAD_CTL_100K_PU | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL), },	/* input: PMIC_IRQ */
//	{ MX51_PIN_GPIO1_6, 			IOMUX_CONFIG_ALT0, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: DEBUG_LED_iMX */ // init in u-boot
//	{ MX51_PIN_GPIO1_7, 			IOMUX_CONFIG_ALT0, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: 12V_SUP_DISn */ // init in u-boot

//	{ MX51_PIN_NANDF_D15, 			IOMUX_CONFIG_ALT3, (PAD_CTL_DRV_HIGH | PAD_CTL_22K_PU | PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_HYS_ENABLE | PAD_CTL_DRV_VOT_HIGH), }, /* tristate output: RESETn_p_2V775*/ // init in u-boot
//	{ MX51_PIN_NANDF_D14, 			IOMUX_CONFIG_ALT3, (PAD_CTL_DRV_HIGH | PAD_CTL_DRV_VOT_HIGH), }, /* output BEEPER_EN */ // init in u-boot
//	{ MX51_PIN_NANDF_D13, 			IOMUX_CONFIG_ALT3, (PAD_CTL_DRV_HIGH | PAD_CTL_DRV_VOT_HIGH), }, /* output POWER_OFF */ // init in u-boot
//	{ MX51_PIN_NANDF_D12, 			IOMUX_CONFIG_ALT3, (PAD_CTL_PKE_NONE | PAD_CTL_HYS_ENABLE | PAD_CTL_DRV_VOT_HIGH), }, /* input INPORT 2 */ // init in u-boot
//	{ MX51_PIN_NANDF_D10, 			IOMUX_CONFIG_ALT3, (PAD_CTL_DRV_HIGH | PAD_CTL_DRV_VOT_HIGH), }, /* output FRAM_WE */ // init in u-boot

	{ MX51_PIN_CSI1_D9, 			IOMUX_CONFIG_ALT3, (PAD_CTL_HYS_ENABLE | PAD_CTL_100K_PU | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL ), }, /* input: VIDEO2_IRQn */
	{ MX51_PIN_EIM_A22, 			IOMUX_CONFIG_ALT1, (PAD_CTL_HYS_ENABLE | PAD_CTL_100K_PU | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL), }, /* input VIDEO1_IRQn */
	{ MX51_PIN_EIM_A23, 			IOMUX_CONFIG_ALT1, (PAD_CTL_HYS_ENABLE | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL), }, /* input: XC_IRQ */

//	{ MX51_PIN_CSPI1_RDY, 			IOMUX_CONFIG_ALT3, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: EXPANSION_EN) */ // init in u-boot


//	{ MX51_PIN_OWIRE_LINE, 			IOMUX_CONFIG_ALT3, (PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_HYS_ENABLE ), }, /* output: KEYB_BACKLIGHT_EN */ // init in u-boot

/* These pins are configured to GPIO per default*/
//	{ MX51_PIN_I2C1_CLK, 			IOMUX_CONFIG_ALT3, }, /* todo: expansion port */
//	{ MX51_PIN_I2C1_DAT, 			IOMUX_CONFIG_ALT3, }, /* todo: expansion port */
//	{ MX51_PIN_UART3_RXD, 			IOMUX_CONFIG_ALT3, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: XC_BOOTLOADER */
//	{ MX51_PIN_UART3_TXD, 			IOMUX_CONFIG_ALT3, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE ), }, /* output: XC_RESET */

/*
	//:  nicht notwendig - default config
	//t  st�rtzt beim aktivieren ab
*/

};

/*
 * This gets called every 1ms when we panic.
 */

static long vision2_panic_blink(long count)
{
	long delay = 0;
	static long last_blink;
	static char led;

	/* Fast blink: 200ms period. */
	if (count - last_blink < 100)
		return 0;

	led ^= 1;
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_6), led);
			
	last_blink = count;

	return delay;
}

extern void machine_restart(char *cmd);

static void vision2_pm_power_off(void)
{
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_NANDF_D13), 1);
	// If K15 is high now, we should reboot. In this case, setting
	// the POWER_OFF pin won't switch off power, so we can do
	// a reset now. Bootloader will take care of deasserting
	// POWER_OFF.
	machine_restart(NULL);
}

int gpio_wdt_init(void)
{
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_DI1_PIN13), "WDOG_TRIGGER");		/* MX51_PIN_GPIO3_2 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_DI1_PIN13),0);
	return(0);
}

int gpio_wdt_toggle(void)
{
	static unsigned int wdog_val = 0;
	wdog_val ^= 1;
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_DI1_PIN13), wdog_val);
	return(0);
}

EXPORT_SYMBOL(gpio_wdt_toggle);
EXPORT_SYMBOL(gpio_wdt_init);


static int __initdata enable_w1 = { 0 };
static int __init w1_setup(char *__unused)
{
	enable_w1 = 1;
	return 1;
}

__setup("w1", w1_setup);

void __init mx51_vision2_io_init(void)
{
	u32 reg;
	int i;

#if defined(CONFIG_I2C_GPIO) || defined(CONFIG_I2C_GPIO_MODULE)
/* use I2C via GPIO Pins */
	mxc_request_iomux(MX51_PIN_EIM_D24, IOMUX_CONFIG_ALT1);	/* I2C GPIO SDA */
	mxc_iomux_set_pad(MX51_PIN_EIM_D24, (PAD_CTL_SRE_SLOW | PAD_CTL_DRV_HIGH | PAD_CTL_ODE_OPENDRAIN_ENABLE | PAD_CTL_22K_PU | PAD_CTL_HYS_ENABLE | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL));
	mxc_free_iomux   (MX51_PIN_EIM_D24, IOMUX_CONFIG_ALT1);
	mxc_request_iomux(MX51_PIN_EIM_D27, IOMUX_CONFIG_ALT1);	/* I2C GPIO SCK */
	mxc_iomux_set_pad(MX51_PIN_EIM_D27, (PAD_CTL_SRE_SLOW | PAD_CTL_DRV_HIGH | PAD_CTL_ODE_OPENDRAIN_ENABLE | PAD_CTL_22K_PU | PAD_CTL_HYS_ENABLE | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL));
	mxc_free_iomux   (MX51_PIN_EIM_D27, IOMUX_CONFIG_ALT1);

	mxc_request_iomux(MX51_PIN_UART1_RTS, IOMUX_CONFIG_ALT3);	/* DAB_I2C_SDA_GPIO */
	mxc_iomux_set_pad(MX51_PIN_UART1_RTS, (PAD_CTL_SRE_SLOW | PAD_CTL_DRV_LOW | PAD_CTL_ODE_OPENDRAIN_ENABLE | PAD_CTL_HYS_ENABLE | PAD_CTL_PUE_PULL));
	mxc_free_iomux   (MX51_PIN_UART1_RTS, IOMUX_CONFIG_ALT3);
	
	mxc_request_iomux(MX51_PIN_UART1_CTS, IOMUX_CONFIG_ALT3);	/* DAB_I2C_SCL_GPIO */
	mxc_iomux_set_pad(MX51_PIN_UART1_CTS, (PAD_CTL_SRE_SLOW | PAD_CTL_DRV_LOW | PAD_CTL_ODE_OPENDRAIN_ENABLE | PAD_CTL_HYS_ENABLE | PAD_CTL_PUE_PULL));
	mxc_free_iomux   (MX51_PIN_UART1_CTS, IOMUX_CONFIG_ALT3);
#endif
/* GPIO 1 */

/* configure display backlight PWM pin to drive low in GPIO mode -> backlight is off if GPIO is selected */
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_GPIO1_2), "DAB_PWM");				/* MX51_PIN_GPIO1_2 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_GPIO1_2),0);
        /* set GPIO output to 100% backlight */
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_2), 1);
	
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_GPIO1_3), "VBAT_EN_KL30");			/* MX51_PIN_GPIO1_3 */
	gpio_direction_input(IOMUX_TO_GPIO(MX51_PIN_GPIO1_3));
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_GPIO1_3),0);							/* export to userspace */

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_GPIO1_5), "PMIC_IRQ");				/* MX51_PIN_GPIO1_5 */
	gpio_direction_input(IOMUX_TO_GPIO(MX51_PIN_GPIO1_5));

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_GPIO1_6), "DEBUG_LED_iMX");			/* MX51_PIN_GPIO1_6 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_GPIO1_6),0);
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_GPIO1_6),0);				         	/* export to userspace */

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_GPIO1_7), "12V_SUP_DIS");			/* MX51_PIN_GPIO1_7 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_GPIO1_7),0);
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_7),1);
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_GPIO1_7),0);
	
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_GPIO1_9), "EXPANSION_IRQ");			/* MX51_PIN_GPIO1_9 */
	gpio_direction_input(IOMUX_TO_GPIO(MX51_PIN_GPIO1_9));
	
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_DISPB2_SER_RS), "CAM_EN");			/* MX51_PIN_DISPB2_SER_RS */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_DISPB2_SER_RS),0);
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_DISPB2_SER_RS),0);					/* export to userspace */

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_OWIRE_LINE), "KEYB_BACKLIGHT_EN");	/* MX51_PIN_GPIO1_24 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_OWIRE_LINE),0);
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_OWIRE_LINE),0);

/* GPIO 2 */

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_EIM_A22), "VIDEO1_IRQn"); 			/* MX51_PIN_GPIO2_16 */
	gpio_direction_input(IOMUX_TO_GPIO(MX51_PIN_EIM_A22));

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_EIM_A23), "XC_IRQ");				/* MX51_PIN_GPIO2_17 */
	gpio_direction_input(IOMUX_TO_GPIO(MX51_PIN_EIM_A23));
	/* GPIO2_31 is reserved for future use */ 

/* GPIO 3 */

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_DI1_PIN12), "DAB_DISPLAY_EN");		/* MX51_PIN_GPIO3_1 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_DI1_PIN12),0);
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_DI1_PIN12), 1);
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_DI1_PIN12), 0);
	
//	gpio_request(IOMUX_TO_GPIO(MX51_PIN_DI1_PIN13), "WDOG_TRIGGER");		/* MX51_PIN_GPIO3_2 */
//	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_DI1_PIN13),0);

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_DI1_D0_CS), "DISP2_TX_EN");			/* MX51_PIN_GPIO3_3 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_DI1_D0_CS),0);
	
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_DI1_D1_CS), "DAB_LIGHT_EN");		/* MX51_PIN_GPIO3_4 */

	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_DI1_D1_CS),
				gpio_get_value(IOMUX_TO_GPIO(MX51_PIN_DI1_D1_CS)));
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_DI1_D1_CS), 0);
	
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_DISPB2_SER_DIN), "OUTPORT 0");		/* MX51_PIN_GPIO3_5 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_DISPB2_SER_DIN),0);
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_DISPB2_SER_DIN),0);					/* export to userspace */

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_DISPB2_SER_DIO), "OUTPORT 1");		/* MX51_PIN_GPIO3_6 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_DISPB2_SER_DIO),0);
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_DISPB2_SER_DIO),0);					/* export to userspace */

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_GPIO_NAND), "OUTPORT 2");			/* MX51_PIN_GPIO3_12 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_GPIO_NAND),0);
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_GPIO_NAND),0);						/* export to userspace */

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_AUD3_BB_FS), "INPORT 0");			/* MX51_PIN_GPIO4_21 */
	gpio_direction_input(IOMUX_TO_GPIO(MX51_PIN_AUD3_BB_FS));
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_AUD3_BB_FS),0);						/* export to userspace */

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_AUD3_BB_CK), "INPORT 1");			/* MX51_PIN_GPIO4_20 */
	gpio_direction_input(IOMUX_TO_GPIO(MX51_PIN_AUD3_BB_CK));
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_AUD3_BB_CK),0);						/* export to userspace */

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_NANDF_D12), "INPORT 2");			/* MX51_PIN_GPIO3_28 */
	gpio_direction_input(IOMUX_TO_GPIO(MX51_PIN_NANDF_D12));
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_NANDF_D12),0);						/* export to userspace */

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_DISPB2_SER_CLK), "DISP2_GPIO");		/* MX51_PIN_GPIO3_7 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_DISPB2_SER_CLK),0);
		
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_CSI1_D9), "VIDEO2_IRQn");			/* MX51_PIN_GPIO3_13 */
	gpio_direction_input(IOMUX_TO_GPIO(MX51_PIN_CSI1_D9));
	
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_NANDF_D15), "Peripheral RESETn"); 	/* MX51_PIN_GPIO3_25 */
	gpio_direction_input(IOMUX_TO_GPIO(MX51_PIN_NANDF_D15));				/* tristate output */
	
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_NANDF_D14), "BEEPER");				/* MX51_PIN_GPIO3_26 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_NANDF_D14),0); 				/* beeper OFF - set PWM before use of beeper */

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_NANDF_D13), "POWER_OFF");			/* MX51_PIN_GPIO3_27 */
	// [issue37308] Not necessary to do this (already done in the bootloader)
	//		Will change the value to "0" if called, but we want to keep it "1"
	//gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_NANDF_D13),0);	 				/* set to 1 to power off */

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_GPIO1_4), "WDOG_RESETn");
//        gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_4), 1);
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_GPIO1_4), 1);

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_NANDF_D10), "FRAM_WE");				/* MX51_PIN_GPIO3_30 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_NANDF_D10),0);				/* set to 1 before writing FRAM */
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_NANDF_D10),0);						/* export to userspace */

/* GPIO 4 */

//	gpio_request(IOMUX_TO_GPIO(MX51_PIN_I2C1_CLK), "EXP_GPIO_1");			/* MX51_PIN_GPIO4_16 */
//	gpio_direction_input(IOMUX_TO_GPIO(MX51_PIN_I2C1_CLK));
//	gpio_request(IOMUX_TO_GPIO(MX51_PIN_I2C1_DAT), "EXP_GPIO_2");			/* MX51_PIN_GPIO4_17 */
//	gpio_direction_input(IOMUX_TO_GPIO(MX51_PIN_I2C1_DAT));

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_AUD3_BB_TXD), "FRONT_IRQ");			/* MX51_PIN_GPIO4_18 */
	gpio_direction_input(IOMUX_TO_GPIO(MX51_PIN_AUD3_BB_TXD));

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_AUD3_BB_RXD), "DAB_IRQ");			/* MX51_PIN_GPIO4_19 */
	gpio_direction_input(IOMUX_TO_GPIO(MX51_PIN_AUD3_BB_RXD));

	gpio_request(IOMUX_TO_GPIO(MX51_PIN_UART3_RXD), "XC_BOOTLOADER");		/* MX51_PIN_GPIO4_22 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_UART3_RXD),0);
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_UART3_RXD),0);						/* export to userspace */
	
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_UART3_TXD), "XC_RESET");			/* MX51_PIN_GPIO4_23 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_UART3_TXD),0);				/* set to 1 to reset XC CPU */
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_UART3_TXD),0);						/* export to userspace */
	
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_CSPI1_RDY), "EXPANSION_EN");		/* MX51_PIN_GPIO4_26 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_CSPI1_RDY),1);				/* set to 1 to enable expansion */
	gpio_export(IOMUX_TO_GPIO(MX51_PIN_CSPI1_RDY),0);						/* export to userspace */
	
/* todo: s�mtliche IRQs initialisieren */


/* route 24MHz OSC to GPIO1_8 */
	reg = __raw_readl(MXC_CCM_CCOSR);
	reg = 0x010e0000;
	__raw_writel(reg, MXC_CCM_CCOSR);

#if 0
/* configure RESETn_p_2V775 as tristate pin */
	mxc_request_iomux(MX51_PIN_NANDF_D15, IOMUX_CONFIG_ALT3);
	mxc_iomux_set_pad(MX51_PIN_NANDF_D15, (PAD_CTL_DRV_HIGH | PAD_CTL_22K_PU | PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_HYS_ENABLE | PAD_CTL_DRV_VOT_HIGH));
	
/* set muxer of FEC config lines to GPIO */
	mxc_request_iomux(MX51_PIN_EIM_EB3, IOMUX_CONFIG_ALT1);		/* FEC_RXD1 - sel GPIO (2-23) for configuration -> 1 */
	mxc_request_iomux(MX51_PIN_EIM_CS2, IOMUX_CONFIG_ALT1);		/* FEC_RXD2 - sel GPIO (2-27) for configuration -> 0 */
	mxc_request_iomux(MX51_PIN_EIM_CS3, IOMUX_CONFIG_ALT1);		/* FEC_RXD3 - sel GPIO (2-28) for configuration -> 0 */
	mxc_request_iomux(MX51_PIN_EIM_CS4, IOMUX_CONFIG_ALT1);		/* FEC_RXER - sel GPIO (2-29) for configuration -> 0 */
	mxc_request_iomux(MX51_PIN_NANDF_RB2, IOMUX_CONFIG_ALT3);	/* FEC_COL  - sel GPIO (3-10) for configuration -> 1 */
	mxc_request_iomux(MX51_PIN_NANDF_RB3, IOMUX_CONFIG_ALT3);	/* FEC_RCLK - sel GPIO (3-11) for configuration -> 0 */
	mxc_request_iomux(MX51_PIN_NANDF_D9, IOMUX_CONFIG_ALT3);	/* FEC_RXD0 - sel GPIO (3-31) for configuration -> 1 */	

/* config FEC config lines */
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_EIM_EB3), "FEC_RXD1_CONFIG");			/* MX51_PIN_GPIO2_23 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_EIM_EB3),1);
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_EIM_CS2), "FEC_RXD2_CONFIG");			/* MX51_PIN_GPIO2_27 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_EIM_CS2),0);
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_EIM_CS3), "FEC_RXD3_CONFIG");			/* MX51_PIN_GPIO2_28 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_EIM_CS3),0);
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_EIM_CS4), "FEC_RXER_CONFIG");			/* MX51_PIN_GPIO2_29 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_EIM_CS4),0);
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_NANDF_RB2), "FEC_COL_CONFIG");			/* MX51_PIN_GPIO3_10 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_NANDF_RB2),1);
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_NANDF_RB3), "FEC_RCLK_CONFIG");			/* MX51_PIN_GPIO3_11 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_NANDF_RB3),0);
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_NANDF_D9), "FEC_RXD0_CONFIG");			/* MX51_PIN_GPIO3_31 */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_NANDF_D9),1);

/* reset Peripherals */
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_NANDF_D15), 0);
	msleep(100);
	gpio_direction_input(IOMUX_TO_GPIO(MX51_PIN_NANDF_D15));

/* release GPIO pins*/
	mxc_free_iomux(MX51_PIN_EIM_EB3, IOMUX_CONFIG_ALT1);
	mxc_free_iomux(MX51_PIN_EIM_CS2, IOMUX_CONFIG_ALT1);
	mxc_free_iomux(MX51_PIN_EIM_CS3, IOMUX_CONFIG_ALT1);
	mxc_free_iomux(MX51_PIN_EIM_CS4, IOMUX_CONFIG_ALT1);
	mxc_free_iomux(MX51_PIN_NANDF_RB2, IOMUX_CONFIG_ALT3);
	mxc_free_iomux(MX51_PIN_NANDF_RB3, IOMUX_CONFIG_ALT3);
	mxc_free_iomux(MX51_PIN_NANDF_D9, IOMUX_CONFIG_ALT3);
#endif

	for (i = 0; i < ARRAY_SIZE(mxc_iomux_pins); i++) 
	{
		mxc_request_iomux(mxc_iomux_pins[i].pin, mxc_iomux_pins[i].mux_mode);
		if (mxc_iomux_pins[i].pad_cfg)
			mxc_iomux_set_pad(mxc_iomux_pins[i].pin, mxc_iomux_pins[i].pad_cfg);
		if (mxc_iomux_pins[i].in_select)
			mxc_iomux_set_input(mxc_iomux_pins[i].in_select, mxc_iomux_pins[i].in_mode);
	}

	panic_blink = vision2_panic_blink;
	pm_power_off = vision2_pm_power_off;
        /* Configure SS1 as GPIO because of long uninterrupted SPI
         * transmissions -- CS must be high during these
         */
        {
            u32 gpio = IOMUX_TO_GPIO(MX51_PIN_CSPI1_SS1);
            gpio_request(gpio, "cspi1_ss1");
            gpio_direction_output(gpio, 0);
            gpio_set_value(gpio, 1);
        }

/* todo: enable watchdog service */

}

/* workaround for ecspi chipselect pin may not keep correct level when idle
 * The cspi_mode is set to spi->master->bus_num by the caller
 * Note: The chipselect is incremented by 1 by the caller, so
 * the first is 1, not 0
 * Note: Configure SS1 as GPIO because of long uninterrupted SPI
 *       transmissions -- CS must be high during these
 */
void mx51_vision2_gpio_spi_chipselect_active(int cspi_mode, int status,
					     int chipselect)
{
	u32 gpio;

	switch (cspi_mode) {
	case 1:
		gpio = IOMUX_TO_GPIO(MX51_PIN_CSPI1_SS1);
		switch (chipselect) {
		case 0x2: /* cs = 1 */
#ifdef DEBUG                        
			pr_debug ("SPI+CS: bus: %d cs: %d (%d) %d\n"
			         , cspi_mode, chipselect, status, gpio);
#endif
			gpio_set_value(gpio, !!status);
			break;
		default:
			gpio_set_value(gpio, 1);
			break;
		}
		break;
	case 2:
		break;
	case 3:
		break;
	default:
		break;
	}
}
EXPORT_SYMBOL(mx51_vision2_gpio_spi_chipselect_active);

void mx51_vision2_io_suspend()
{
	// disable keyboard backlight and remember current state
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_OWIRE_LINE), "KEYB_BACKLIGHT_EN");
	keyb_backlight_value = gpio_get_value(IOMUX_TO_GPIO(MX51_PIN_OWIRE_LINE));
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_OWIRE_LINE), 0);

	// disable 12V power supply
	gpio_request(IOMUX_TO_GPIO(MX51_PIN_GPIO1_7), "12V_SUP_DIS");        
        gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_7),0);
}

EXPORT_SYMBOL(mx51_vision2_io_suspend);


void mx51_vision2_io_resume()
{
	// restore keyboard backlight value
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_OWIRE_LINE), keyb_backlight_value);

	// enable 12V power supply
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_7),1);
}

EXPORT_SYMBOL(mx51_vision2_io_resume);
