/*
 * Copyright 2009-2010 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

#include <linux/types.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/init.h>
#include <linux/clk.h>
#include <linux/platform_device.h>
#include <linux/fsl_devices.h>
#include <linux/spi/spi.h>
#include <linux/i2c.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <linux/regulator/consumer.h>
#include <linux/pmic_external.h>
#include <linux/pmic_status.h>
#include <linux/ipu.h>
#include <linux/mxcfb.h>
#include <linux/mmzone.h>
#include <linux/pfn.h>
#include <linux/bootmem.h>
#include <linux/page-flags.h>
#include <linux/swap.h>
#include <linux/pwm_backlight.h>
#include <mach/common.h>
#include <mach/hardware.h>
#include <asm/setup.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/time.h>
#include <asm/mach/keypad.h>
#include <asm/mach/flash.h>
#include <mach/gpio.h>
#include <mach/mmc.h>
#include <mach/mxc_dvfs.h>
#include <mach/mxc_edid.h>
#include <linux/i2c-gpio.h>
#include <linux/i2c/at24.h>
#include <linux/i2c/max9526.h>
#include <linux/input/smtc/sx86xx_reg_struct.h>
#include <linux/input/smtc/sx86_545556747576_Registers.h>
#include <linux/input/smtc/sx8654_platform_data.h>

#include "devices.h"
#include "iomux.h"
#include "mx51_pins.h"
#include "crm_regs.h"
#include "usb.h"

/*!
 * @file mach-mx51/mx51_vision2.c
 *
 * @brief This file contains the board specific initialization routines.
 *
 * @ingroup MSL_MX51
 */
extern int __init mx51_vision2_init_mc13892(void);
extern void __init mx51_vision2_io_init(void);
extern void mx51_vision2_io_suspend(void);
extern void mx51_vision2_io_resume(void);

extern struct cpu_wp *(*get_cpu_wp)(int *wp);
extern void (*set_num_cpu_wp)(int num);
static int num_cpu_wp = 3;

#ifdef CONFIG_FB_KEEP_BOOTSPLASH
static struct vision2_bootsplash_info 
    {
	unsigned long start;
	unsigned long length;
	unsigned long min_pfn;
	unsigned long max_pfn;
	pg_data_t     *pgdat;
    } vision2_bootsplash_info;
#endif

/* working point(wp): 0 - 800MHz; 1 - 166.25MHz; */
static struct cpu_wp cpu_wp_auto[] = {
	{
	 .pll_rate = 1000000000,
	 .cpu_rate = 1000000000,
	 .pdf = 0,
	 .mfi = 10,
	 .mfd = 11,
	 .mfn = 5,
	 .cpu_podf = 0,
	 .cpu_voltage = 1175000,},
	{
	 .pll_rate = 800000000,
	 .cpu_rate = 800000000,
	 .pdf = 0,
	 .mfi = 8,
	 .mfd = 2,
	 .mfn = 1,
	 .cpu_podf = 0,
	 .cpu_voltage = 1100000,},
	{
	 .pll_rate = 800000000,
	 .cpu_rate = 166250000,
	 .pdf = 4,
	 .mfi = 8,
	 .mfd = 2,
	 .mfn = 1,
	 .cpu_podf = 4,
	 .cpu_voltage = 850000,},
};


struct cpu_wp *mx51_vision2_get_cpu_wp(int *wp)
{
	*wp = num_cpu_wp;
	return cpu_wp_auto;
}

void mx51_vision2_set_num_cpu_wp(int num)
{
	num_cpu_wp = num;
	return;
}

/******************************************************************/
/*  oneWire                                                       */
/******************************************************************/

#if 0
static struct mxc_w1_config mxc_w1_data = {
	.search_rom_accelerator = 1,
};
#endif

/******************************************************************/
/*  Keyboard                                                      */
/******************************************************************/

/* map keypad to F1...F10 to ease simultaneous use with encoder. */
static u16 keymapping[10] = {
	KEY_F1, KEY_F2, KEY_F3, KEY_F4, KEY_F5,
	KEY_F6, KEY_F7, KEY_F8, KEY_F9, KEY_F10, 
};

static struct keypad_data keypad_plat_data = {
	.rowmax = 2,
	.colmax = 5,
	.irq = MXC_INT_KPP,
	.learning = 0,
	.delay = 2,
	.matrix = keymapping,
};

/******************************************************************/
/*  PWM Backlight                                                 */
/******************************************************************/

static int vision2_backlight_init(struct device *dev)
{
    mxc_request_iomux(MX51_PIN_GPIO1_2, IOMUX_CONFIG_ALT1); // pwm output
    /* pad settings from mx51_vision2_gpio.c: */
    mxc_iomux_set_pad(MX51_PIN_GPIO1_2, (PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE));
    return 0;
}


static struct platform_pwm_backlight_data mxc_pwm_backlight_data = {
	.pwm_id = 0,
	.max_brightness = 255,
	.dft_brightness = 255,
	.pwm_period_ns = (5*1000*1000), /* 200 Hz */
	.fade_steps_per_sec = 1000,
        .init = vision2_backlight_init,
};

/******************************************************************/
/*  IPU                                                           */
/******************************************************************/


extern void mx5_ipu_reset(void);
static struct mxc_ipu_config mxc_ipu_data = {
	.rev = 2,
	.reset = mx5_ipu_reset,
};

/******************************************************************/
/*  VPU                                                           */
/******************************************************************/

extern void mx5_vpu_reset(void);
static struct mxc_vpu_platform_data mxc_vpu_data = {
	.reset = mx5_vpu_reset,
};

/******************************************************************/
/*  SPI                                                           */
/******************************************************************/

extern void mx51_vision2_gpio_spi_chipselect_active(int cspi_mode, int status,
						    int chipselect);
static struct mxc_spi_master mxcspi1_data = {
	.maxchipselect = 4,
	.spi_version = 23,
	.chipselect_active   = mx51_vision2_gpio_spi_chipselect_active,
	.chipselect_inactive = mx51_vision2_gpio_spi_chipselect_active,
};

#if 0
static struct mxc_spi_master mxcspi2_data = {
	.maxchipselect = 4,
	.spi_version = 23,
};

static struct mxc_spi_master mxcspi3_data = {
	.maxchipselect = 4,
	.spi_version = 7,
};
#endif


/******************************************************************/
/*  I2C                                                           */
/******************************************************************/

#ifdef CONFIG_I2C_GPIO

static struct i2c_gpio_platform_data i2c_gpio_pdata_1 = {
	.sda_pin		= IOMUX_TO_GPIO(MX51_PIN_EIM_D24),
	.sda_is_open_drain	= 0,
	.scl_pin		= IOMUX_TO_GPIO(MX51_PIN_EIM_D27),
	.scl_is_open_drain	= 0,
	.udelay			= 4,		/* ~100 kHz */
};

static struct platform_device gpio_twi_device_1 = {
	.name			= "i2c-gpio",
	.id			= 1,
	.dev.platform_data	= &i2c_gpio_pdata_1,
};

static struct at24_platform_data board_eeprom = {
	.byte_len = 4096,
	.page_size = 32,
	.flags = AT24_FLAG_ADDR16,
};

static struct max9526_platform_data max9526_pdata[2] = {
	[0] = {
		.csi	= 0,
	},
	[1] = {
		.csi	= 1,
	},
};

static struct i2c_board_info mxc_i2c1_board_info_1[] __initdata = {
	{
		I2C_BOARD_INFO("max9526", 0x20),
		.platform_data = &max9526_pdata[0],
	},
	{
		I2C_BOARD_INFO("max9526", 0x22),
		.platform_data = &max9526_pdata[1],
	},
	{
		I2C_BOARD_INFO("fm24cl", 0x50),
		.platform_data = &board_eeprom,
	},
/*	{
		I2C_BOARD_INFO("sgtl5000-i2c", 0x0a),
	},
	{
		I2C_BOARD_INFO("bh1715", 0x23),
	},*/
	{
		I2C_BOARD_INFO("bh1780", 0x29),
	},
	{
		I2C_BOARD_INFO("pcf8563", 0x51),
	},

};

/* I2C @ extension board */

static struct i2c_gpio_platform_data i2c_gpio_pdata_2 = {
	.sda_pin		= IOMUX_TO_GPIO(MX51_PIN_I2C1_DAT),
	.sda_is_open_drain	= 0,
	.scl_pin		= IOMUX_TO_GPIO(MX51_PIN_I2C1_CLK),
	.scl_is_open_drain	= 0,
	.udelay			= 4,		/* ~100 kHz */
};

static struct platform_device gpio_twi_device_2 = {
	.name			= "i2c-gpio",
	.id			= 2,
	.dev.platform_data	= &i2c_gpio_pdata_2,
};

/*static u16 keymap[] = {
	KEY_A, KEY_B, KEY_C,
	KEY_D, KEY_E, KEY_F,
	KEY_G, KEY_H,
};

static struct mxc_keyp_platform_data keypad_data = {
	.matrix = keymap,
	.active = NULL,
	.inactive = NULL,
	.vdd_reg = NULL,
};*/

static struct at24_platform_data testboard_eeprom_i2c2 = {
        .byte_len = 2048,
        .page_size = 8,
};

static struct i2c_board_info mxc_i2c1_board_info_2[] __initdata = {
	{
/*		I2C_BOARD_INFO("cap1066i2c", 0x28),
		.platform_data = &keypad_data,
		.irq = IOMUX_TO_IRQ(MX51_PIN_GPIO1_9),*/
//              I2C_BOARD_INFO("sht21", 0x40)
                I2C_BOARD_INFO("24c02", 0x50),
                .platform_data = &testboard_eeprom_i2c2,
	},

};

/* I2C @ DAB */

static struct i2c_gpio_platform_data i2c_gpio_pdata_3 = {
	.sda_pin		= IOMUX_TO_GPIO(MX51_PIN_UART1_RTS),
	.sda_is_open_drain	= 0,
	.scl_pin		= IOMUX_TO_GPIO(MX51_PIN_UART1_CTS),
	.scl_is_open_drain	= 0,
	.udelay			= 4,		/* ~100 kHz */
};

static struct platform_device gpio_twi_device_3 = {
	.name			= "i2c-gpio",
	.id			= 3,
	.dev.platform_data	= &i2c_gpio_pdata_3,
};

/*static u16 dab_keymap[] = {
	KEY_A, KEY_B, KEY_C,
	KEY_D, KEY_E, KEY_F,
	KEY_G, KEY_H,
};

static struct mxc_keyp_platform_data dab_keypad_data = {
	.matrix = dab_keymap,
	.active = NULL,
	.inactive = NULL,
	.vdd_reg = NULL,
};*/

static struct at24_platform_data testboard_eeprom_i2c3 = {
        .byte_len = 2048,
        .page_size = 8,
};

static inline int sx8654_get_pendown_state(void)
{
	return !gpio_get_value(IOMUX_TO_GPIO(MX51_PIN_AUD3_BB_RXD));
}

static struct sx86xx_reg sx8654_reg_init[] = {
	{
		.reg = SMTC_REGTOUCH0,
		.data = SMTC_TOUCHRATE_100CPS | SMTC_POWDLY_284US,
	},
	{ 
		.reg = SMTC_REGTOUCH1,
		.data = SMTC_TSTYPE_5WIRE | SMTC_RPNDT_114KOHM | SMTC_FILT_7SAMPLE,
	},
	{ 
		.reg = SMTC_REGTOUCH2, 
		.data = SMTC_SETDLY_71_0US,
	},
/*	{
		.reg = SMTC_REGTOUCH3,
		.data = 0x01,
	},*/
	{ 
		.reg = SMTC_REGCHANMSK, 
		.data = SMTC_CONV_X | SMTC_CONV_Y, // | SMTC_CONV_Z1 | SMTC_CONV_Z2,
	},
	{ 
		.reg = SMTC_REGIRQMSK, 
		.data = SMTC_PENTOUCHIRQ_TOUCHCONVDONEIRQ | SMTC_PENRELEASEIRQ,
	},
};

static int sx8654_hw_init(void)
{
	return 0;
}

static void sx8654_hw_exit(void)
{
}

static struct sx8654_platform_data sx8654_config = {
	.touchPlatData = {
		.rt_max = 400,
		.x_axis = {
			.threshold = 4095,
			.max = 0x0fff,
			.min = 0,
			.plate_ohms = 618,
		},
		.y_axis = {
			.threshold = 4095,
			.max = 0x0fff,
			.min = 0,
			.plate_ohms = 297,
		},
	},
	.get_pendown_state = sx8654_get_pendown_state,
	.init_platform_hw = sx8654_hw_init,
	.exit_platform_hw = sx8654_hw_exit,
	.init_array = sx8654_reg_init,
	.size_init_array = ARRAY_SIZE(sx8654_reg_init),
	.numTouchChannelsEnabled = 2,
};

static struct i2c_board_info mxc_i2c1_board_info_3[] __initdata = {
	{
/*		I2C_BOARD_INFO("cap1066i2c", 0x28),
		.platform_data = &dab_keypad_data,
		.irq = IOMUX_TO_IRQ(MX51_PIN_AUD3_BB_RXD),*/
		I2C_BOARD_INFO("24c02", 0x57),
		.platform_data = &testboard_eeprom_i2c3,
	},
	{
		I2C_BOARD_INFO("sx8654", 0x48),
		.flags = I2C_CLIENT_WAKE,
		.irq = IOMUX_TO_IRQ(MX51_PIN_AUD3_BB_RXD),
		.platform_data = &sx8654_config,
	},
};

#endif

/******************************************************************/
/*  RTC                                                           */
/******************************************************************/

static struct mxc_srtc_platform_data srtc_data = {
	.srtc_sec_mode_addr = 0x83F98840,
};

/******************************************************************/
/*  Dynamic Voltage & Frequency Scaling                           */
/******************************************************************/

static struct mxc_dvfs_platform_data dvfs_core_data = {
	.reg_id = "SW1",
	.clk1_id = "cpu_clk",
	.clk2_id = "gpc_dvfs_clk",
	.gpc_cntr_offset = MXC_GPC_CNTR_OFFSET,
	.gpc_vcr_offset = MXC_GPC_VCR_OFFSET,
	.ccm_cdcr_offset = MXC_CCM_CDCR_OFFSET,
	.ccm_cacrr_offset = MXC_CCM_CACRR_OFFSET,
	.ccm_cdhipr_offset = MXC_CCM_CDHIPR_OFFSET,
	.prediv_mask = 0x1F800,
	.prediv_offset = 11,
	.prediv_val = 3,
	.div3ck_mask = 0xE0000000,
	.div3ck_offset = 29,
	.div3ck_val = 2,
	.emac_val = 0x08,
	.upthr_val = 25,
	.dnthr_val = 9,
	.pncthr_val = 33,
	.upcnt_val = 10,
	.dncnt_val = 10,
	.delay_time = 30,
	.num_wp = 3,
};

static struct mxc_dvfsper_data dvfs_per_data = {
	.reg_id = "SW2",
	.clk_id = "gpc_dvfs_clk",
	.gpc_cntr_reg_addr = MXC_GPC_CNTR,
	.gpc_vcr_reg_addr = MXC_GPC_VCR,
	.gpc_adu = 0x0,
	.vai_mask = MXC_DVFSPMCR0_FSVAI_MASK,
	.vai_offset = MXC_DVFSPMCR0_FSVAI_OFFSET,
	.dvfs_enable_bit = MXC_DVFSPMCR0_DVFEN,
	.irq_mask = MXC_DVFSPMCR0_FSVAIM,
	.div3_offset = 0,
	.div3_mask = 0x7,
	.div3_div = 2,
	.lp_high = 1200000,
	.lp_low = 1200000,
};

/******************************************************************/
/*  SPDIF                                                         */
/******************************************************************/


static struct mxc_spdif_platform_data mxc_spdif_data = {
	.spdif_tx = 1,
	.spdif_rx = 0,
	.spdif_clk_44100 = 0,	/* spdif_ext_clk source for 44.1KHz */
	.spdif_clk_48000 = 7,	/* audio osc source */
	.spdif_clkid = 0,
	.spdif_clk = NULL,	/* spdif bus clk */
};


/******************************************************************/
/*  Framebuffer                                                   */
/******************************************************************/

#if defined(CONFIG_FB_MXC_SYNC_PANEL) || \
	defined(CONFIG_FB_MXC_SYNC_PANEL_MODULE)

extern struct fb_videomode video_mode;

static void mxc_nop_release(struct device *dev)
{
	/* Nothing */
}
	
/*static struct resource mxcfb_resources[] = {
	[0] = {
	       .flags = IORESOURCE_MEM,
	       },
};*/

static struct mxc_fb_platform_data fb_data;
static char mode_str[32];

static struct platform_device mxc_fb_device[] = {
	{
	 .name = "mxc_sdc_fb",
	 .id = 0, /* DI0 port */
	 .dev = {
		 .release = mxc_nop_release,
		 .coherent_dma_mask = 0xFFFFFFFF,
		 .platform_data = &fb_data,
		 },
	 },
	{
	 .name = "mxc_sdc_fb",
	 .id = 1, /* DI1 port */
	 .dev = {
		 .release = mxc_nop_release,
		 .coherent_dma_mask = 0xFFFFFFFF,
//		 .platform_data = &fb_data[1],
		 },
	 },
	{
	 .name = "mxc_sdc_fb",
	 .id = 2, /* overlay */
	 .dev = {
		 .release = mxc_nop_release,
		 .coherent_dma_mask = 0xFFFFFFFF,
		 },
	 },
};

static void display_reset(void)
{

}

static struct mxc_lcd_platform_data lcd_data =
{
	.reset = display_reset
};

static struct platform_device lcd_device =
{
	.name = "mxcfb_vision2",
	.dev = {
		.release = mxc_nop_release,
		.coherent_dma_mask = 0xFFFFFFFF,
		.platform_data = &lcd_data,
	}
};

static int __init mxc_init_fb(void)
{
	if (!machine_is_ttc_vision2())
		return 0;

        fb_data.interface_pix_fmt = IPU_PIX_FMT_RGB666;
        fb_data.mode = &video_mode;
	sprintf(mode_str, "%dx%dM-16@60", video_mode.xres, video_mode.yres);
	fb_data.mode_str = mode_str;
        fb_data.num_modes = 1;

	platform_device_register(&lcd_device);
        mxc_register_device(&mxc_fb_device[0], &fb_data);
        /* Overlay */
        mxc_register_device(&mxc_fb_device[2], NULL);

	return 0;
}
device_initcall(mxc_init_fb);

#else

static inline void mxc_init_fb(void)
{
}

#endif


/******************************************************************/
/*  SPI Flash                                                     */
/******************************************************************/

#if defined(CONFIG_MTD_M25P80) || defined(CONFIG_MTD_M25P80_MODULE)

#if defined(CONFIG_MTD) || defined(CONFIG_MTD_MODULE)
/* Splash image used to be at 0x200000 */
static struct mtd_partition mxc_spi_nor_partitions[] = {
	{
	 .name = "bootloader",
	 .offset = 0,
	 .size = 0x003E0000,
	},
	{
         .name = "sys_cfg",
	 .offset = MTDPART_OFS_APPEND,
	 .size = 0x00010000,
	},
	{
	 .name = "bl_cfg",
         .offset = MTDPART_OFS_APPEND,
         .size = MTDPART_SIZ_FULL,
	},
};

static struct flash_platform_data mxc_spi_flash_data[] = {
	{
	 .name = "mxc_spi_nor",
	 .parts = mxc_spi_nor_partitions,
	 .nr_parts = ARRAY_SIZE(mxc_spi_nor_partitions),
         .type = "m25p32",
	},
};
#endif /* #if defined(CONFIG_MTD) || defined(CONFIG_MTD_MODULE) */

static struct spi_board_info mxc_spi_nor_device[] __initdata = {
#if defined(CONFIG_MTD) || defined(CONFIG_MTD_MODULE)
	{
	 .modalias = "m25p80",
	 .max_speed_hz = 25000000,	/* max spi clock (SCK) speed in HZ */
	 .bus_num = 1,
	 .mode = SPI_MODE_0,            /* CPOL=0, CPHA=0 */
	 .chip_select = 1,
	 .platform_data = &mxc_spi_flash_data[0],},
#endif
};

#endif



/******************************************************************/
/*  NAND Flash                                                    */
/******************************************************************/

#ifdef CONFIG_MTD_PARTITIONS
static struct mtd_partition nand_flash_partitions[] = {
	{
	 .name = "system",
	 .offset = 0,
	 .size = 0x1400000},	/* 20 MByte */
	{
	 .name = "rootfs",
	 .offset = MTDPART_OFS_APPEND,
	 .size = MTDPART_SIZ_FULL},
};
#endif

static int nand_init(void)
{
	return 0;
}

static void nand_exit(void)
{

}

static struct flash_platform_data mxc_nand_data = {
	#ifdef CONFIG_MTD_PARTITIONS
		.parts = nand_flash_partitions,
		.nr_parts = ARRAY_SIZE(nand_flash_partitions),
	#endif
	.width = 1,
	.init = nand_init,
	.exit = nand_exit,
};


/******************************************************************/
/*  SD Interface                                                  */
/******************************************************************/


static int sdhc_write_protect(struct device *dev)
{
	unsigned short rc = 0;

	rc = gpio_get_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_1));
	return rc;
}

static unsigned int sdhc_get_card_det_status(struct device *dev)
{
	int ret;

	ret = gpio_get_value(IOMUX_TO_GPIO(MX51_PIN_GPIO1_0));
	return ret;
}

static struct mxc_mmc_platform_data mmc1_data = {
	.ocr_mask = MMC_VDD_27_28 | MMC_VDD_28_29 | MMC_VDD_29_30 | MMC_VDD_31_32,
	.caps = MMC_CAP_4_BIT_DATA,
	.min_clk = 150000,
	.max_clk = 52000000,
	.card_inserted_state = 0,
	.status = sdhc_get_card_det_status,
	.wp_status = sdhc_write_protect,
	.clock_mmc = "esdhc_clk",
	.power_mmc = NULL,
};


/******************************************************************/
/*  Audio                                                         */
/******************************************************************/

#if defined(CONFIG_SND_SOC_IMX_3STACK_SGTL5000) || defined(CONFIG_SND_SOC_IMX_3STACK_SGTL5000_MODULE)

static struct mxc_audio_platform_data sgtl5000_data = {
	.ssi_num = 1,
	.src_port = 2,
	.ext_port = 6,
	.hp_irq = 0,
	.hp_status = NULL,
	.amp_enable = NULL,
	.sysclk = 24000000,
};

static struct platform_device mxc_sgtl5000_device = {
	.name = "imx-3stack-sgtl5000",
};
#endif

/******************************************************************/
/*  XC2000                                                       */
/******************************************************************/

#if (defined(CONFIG_SPI_MXC) || defined(CONFIG_SPI_MXC_MODULE))
static struct spi_board_info mxc_xc2000_device[] __initdata = {
    {
     .modalias = "spi_xc",
     .max_speed_hz = 100000,  // Speed is NOT the value specified here, but some strange other value
     .bus_num = 1,
     .mode = SPI_MODE_3,      // MODE3 => CPOL = 0 (clock starts at LOW with rising edge): CPOL<->mode relation is opposite to kernel-documentation of spi
                              // MODE3 => CPHA = 1 (sample with leading (falling) edge) 
     .chip_select = 2,},
};
#endif

/******************************************************************/
/*  Sensors                                                       */
/******************************************************************/

static struct platform_device vision2_temperature_device = {
	.name			= "vision2_sensors",
	.id			= -1,
};

/******************************************************************/
/*  PWR Buttons                                                   */
/******************************************************************/


static struct platform_device vision2_power_buttons_device = {
	.name			= "pmic_power_buttons",
	.id			= -1,
};

/******************************************************************/
/*  Board specific fixup                                          */
/******************************************************************/

#ifdef CONFIG_FB_KEEP_BOOTSPLASH
/*!
 * Reserve memory for the boot splash image generated by the bootloader
 */
void __init reserve_bootsplash(void)
{
	int ret;
	pg_data_t *pgdat;
	struct vision2_bootsplash_info *bsi = &vision2_bootsplash_info;
	if (!bsi->length) {
		printk (KERN_INFO "No bootsplash allocation\n");
		return;
	}
	bsi->min_pfn = PFN_DOWN (bsi->start);
	bsi->max_pfn = PFN_UP   (bsi->start + bsi->length);

	for_each_online_pgdat(pgdat) {
		if (  bsi->min_pfn >= pgdat->bdata->node_min_pfn
		   && bsi->max_pfn <= pgdat->bdata->node_low_pfn
		   ) {
			bsi->pgdat = pgdat;
			break;
		}
	}
	if (!bsi->pgdat) {
		printk (KERN_INFO "No memory region found for bootsplash\n");
		return;
	}
	ret = reserve_bootmem_node
	    (bsi->pgdat, bsi->start, bsi->length, BOOTMEM_EXCLUSIVE);
        if (ret) {
                printk (KERN_INFO "Bootsplash reservation failed\n");
        } else {
                printk (KERN_INFO "Bootsplash reserved: %lX-%lX\n"
                       , bsi->start, bsi->start + bsi->length);
        }
}

/*!
 * Free memory of the boot splash image generated by the bootloader.
 * This is called after the bootmem allocator has been retired, so we
 * return the memory to the buddy allocator.
 */
void free_bootsplash (void)
{
	struct vision2_bootsplash_info *bsi = &vision2_bootsplash_info;
	unsigned long pfn = 0;
	for (pfn=bsi->min_pfn; pfn<bsi->max_pfn; pfn++) {
                struct page *page = pfn_to_page(pfn);
                ClearPageReserved(page);
                atomic_set(&page->_count, 1);
                __free_page(page);
	}
        totalram_pages += bsi->max_pfn - bsi->min_pfn;
        printk (KERN_INFO "Bootsplash memory freed\n");
}

#endif /* CONFIG_FB_KEEP_BOOTSPLASH */

/*!
 * Board specific fixup function. It is called by \b setup_arch() in
 * setup.c file very early on during kernel starts. It allows the user to
 * statically fill in the proper values for the passed-in parameters. None of
 * the parameters is used currently.
 *
 * @param  desc         pointer to \b struct \b machine_desc
 * @param  tags         pointer to \b struct \b tag
 * @param  cmdline      pointer to the command line
 * @param  mi           pointer to \b struct \b meminfo
 */
static void __init fixup_mxc_board(struct machine_desc *desc, struct tag *tags,
				   char **cmdline, struct meminfo *mi)
{
	char *str;
	struct tag *t;
	struct tag *mem_tag = 0;
	int total_mem = SZ_512M;
	int left_mem = 0;
	int gpu_mem = SZ_64M + SZ_1M;
	//int fb_mem = SZ_32M;
	int fb_mem = 0;

	mxc_set_cpu_type(MXC_CPU_MX51);

	get_cpu_wp = mx51_vision2_get_cpu_wp;
	set_num_cpu_wp = mx51_vision2_set_num_cpu_wp;

	for_each_tag(mem_tag, tags) {
		if (mem_tag->hdr.tag == ATAG_MEM) {
			total_mem = mem_tag->u.mem.size;
			break;
		}
	}

	for_each_tag(t, tags) {
		if (t->hdr.tag == ATAG_CMDLINE) {
			str = t->u.cmdline.cmdline;
			str = strstr(str, "mem=");
			if (str != NULL) {
				str += 4;
				left_mem = memparse(str, &str);
			}

			str = t->u.cmdline.cmdline;
			str = strstr(str, "gpu_nommu");
			if (str != NULL)
			{
				gpu_data.enable_mmu = 0;
			}
			
			str = t->u.cmdline.cmdline;
                        str = strstr(str, "gpu_mmu");
                        if (str != NULL)
			{
                                gpu_data.enable_mmu = 1;
			}


			str = t->u.cmdline.cmdline;
			str = strstr(str, "gpu_memory=");
			if (str != NULL) {
				str += 11;
				gpu_mem = memparse(str, &str);
			}

			break;
		}
	}

	if (gpu_data.enable_mmu)
		gpu_mem = 0;

	if (left_mem == 0 || left_mem > total_mem)
		left_mem = total_mem - gpu_mem - fb_mem;


	if (mem_tag) {
		fb_mem = total_mem - left_mem - gpu_mem;
		if (fb_mem < 0) {
			gpu_mem = total_mem - left_mem;
			fb_mem = 0;
		}
		mem_tag->u.mem.size = left_mem;

		/*reserve memory for gpu*/
		if (!gpu_data.enable_mmu) {		
			gpu_device.resource[5].start =
				mem_tag->u.mem.start + left_mem;
			gpu_device.resource[5].end =
				gpu_device.resource[5].start + gpu_mem - 1;
		}
#if defined(CONFIG_FB_MXC_SYNC_PANEL) || defined(CONFIG_FB_MXC_SYNC_PANEL_MODULE)
/*		if (fb_mem) {
			mxcfb_resources[0].start = gpu_device.resource[5].end + 1;
			mxcfb_resources[0].end = mxcfb_resources[0].start + fb_mem - 1;
		} else {
			mxcfb_resources[0].start = 0;
			mxcfb_resources[0].end = 0;
		}*/
#endif
	}
}

static void mx51_vision2_suspend_enter()
{
	mx51_vision2_io_suspend();
}

static void mx51_vision2_suspend_exit()
{
	mx51_vision2_io_resume();
}

static struct mxc_pm_platform_data mx51_vision2_pm_data = {
        .suspend_enter = mx51_vision2_suspend_enter,
        .suspend_exit = mx51_vision2_suspend_exit,
};


/*!
 * Board specific initialization.
 */
static void __init mxc_board_init(void)
{
	mxc_ipu_data.di_clk[0] = clk_get(NULL, "ipu_di0_clk");
	mxc_ipu_data.di_clk[1] = clk_get(NULL, "ipu_di1_clk");

	mxc_spdif_data.spdif_core_clk = clk_get(NULL, "spdif_xtal_clk");
	clk_put(mxc_spdif_data.spdif_core_clk);
	
	/* SD card detect irqs */
	mxcsdhc1_device.resource[2].start = IOMUX_TO_IRQ(MX51_PIN_GPIO1_0);
	mxcsdhc1_device.resource[2].end = IOMUX_TO_IRQ(MX51_PIN_GPIO1_0);

	mxc_cpu_common_init();
	mx51_vision2_io_init();

	mxc_register_device(&mxc_dma_device, NULL);
	mxc_register_device(&mxc_wdt_device, NULL);
	mxc_register_device(&mxcspi1_device, &mxcspi1_data);
//	mxc_register_device(&mxcspi2_device, &mxcspi2_data);
//	mxc_register_device(&mxcspi3_device, &mxcspi3_data);

	mx51_vision2_init_mc13892();

#if (defined(CONFIG_MTD_M25P80) || defined(CONFIG_MTD_M25P80_MODULE)) && (defined(CONFIG_MTD) || defined(CONFIG_MTD_MODULE))
	spi_register_board_info(mxc_spi_nor_device, ARRAY_SIZE(mxc_spi_nor_device));
#endif

#if (defined(CONFIG_SPI_MXC) || defined(CONFIG_SPI_MXC_MODULE))
	spi_register_board_info(mxc_xc2000_device, ARRAY_SIZE(mxc_xc2000_device));
#endif

	mxc_register_device(&mxc_nandv2_mtd_device, &mxc_nand_data);
//	mxc_register_device(&mxc_w1_master_device, &mxc_w1_data);

#if defined(CONFIG_I2C_GPIO)
	i2c_register_board_info(1, mxc_i2c1_board_info_1,	ARRAY_SIZE(mxc_i2c1_board_info_1));
	platform_device_register(&gpio_twi_device_1);

	i2c_register_board_info(2, mxc_i2c1_board_info_2,	ARRAY_SIZE(mxc_i2c1_board_info_2));
	platform_device_register(&gpio_twi_device_2);

	i2c_register_board_info(3, mxc_i2c1_board_info_3,	ARRAY_SIZE(mxc_i2c1_board_info_3));
	platform_device_register(&gpio_twi_device_3);

#endif

	mxc_register_device(&mxc_dvfs_core_device, &dvfs_core_data);
	mxc_register_device(&mxc_dvfs_per_device, &dvfs_per_data);
	mxc_register_device(&mxc_alsa_spdif_device, &mxc_spdif_data);
	mxc_register_device(&mxc_keypad_device, &keypad_plat_data);
#if defined(CONFIG_SND_SOC_IMX_3STACK_SGTL5000) || defined(CONFIG_SND_SOC_IMX_3STACK_SGTL5000_MODULE)
	mxc_register_device(&mxc_sgtl5000_device, &sgtl5000_data);
#endif
	mxc_register_device(&mxc_rtc_device, &srtc_data);
	mxc_register_device(&mxc_ipu_device, &mxc_ipu_data);
	mxc_register_device(&mxcvpu_device, &mxc_vpu_data);
        mxc_register_device(&gpu_device, &gpu_data);
	mxc_register_device(&mxcscc_device, NULL);
	mxc_register_device(&mx51_lpmode_device, NULL);
	mxc_register_device(&busfreq_device, NULL);
	mxc_register_device(&sdram_autogating_device, NULL);
	mxc_register_device(&mxc_iim_device, NULL);
	mxc_register_device(&mxc_pwm1_device, NULL);	
	mxc_register_device(&mxc_pwm1_backlight_device,
                &mxc_pwm_backlight_data);
	mxc_register_device(&pm_device, &mx51_vision2_pm_data);
	mxc_register_device(&mxcsdhc1_device, &mmc1_data);
	mxc_register_device(&mxc_ssi1_device, NULL);
	mxc_register_device(&mxc_ssi2_device, NULL);
	mxc_register_device(&mxc_fec_device, NULL);
	mxc_register_device(&mxc_v4l2_device[0], NULL);
	mxc_register_device(&mxc_v4l2_device[1], NULL);
	mxc_register_device(&mxc_v4l2out_device, NULL);

	platform_device_register(&vision2_temperature_device);
	platform_device_register(&vision2_power_buttons_device);

	mx5_usb_dr_init();
	mx5_usbh1_init();
#if 0
	mx5_usbh2_init();
#endif

}

static void __init mx51_vision2_timer_init(void)
{
	struct clk *uart_clk;

	/* Change the CPU voltages for TO2*/
	if (cpu_is_mx51_rev(CHIP_REV_2_0) <= 1) {
		cpu_wp_auto[0].cpu_voltage = 1175000;
		cpu_wp_auto[1].cpu_voltage = 1100000;
		cpu_wp_auto[2].cpu_voltage = 1000000;
	}

	mx51_clocks_init(32768, 24000000, 22579200, 24576000);

	uart_clk = clk_get_sys("mxcintuart.2", NULL);
	early_console_setup(UART3_BASE_ADDR, uart_clk);
}

static struct sys_timer mxc_timer = {
	.init	= mx51_vision2_timer_init,
};

int vision2_deviceid;
char vision2_mode;
int vision2_sw_release, vision2_hw_revision;
char vision2_deviceserial[32];
char vision2_boardserial[32];

static int __init parse_tag_vision2_deviceinfo(const struct tag *tag)
{
	printk("serial_device=%s, serial_board=%s\n", tag->u.vision2_deviceinfo.serial_device,
		tag->u.vision2_deviceinfo.serial_board);

//	vision2_deviceserial[14] = '0';
//	vision2_deviceserial[15] = '0';
	
	vision2_deviceid = tag->u.vision2_deviceinfo.id;
	vision2_mode = tag->u.vision2_deviceinfo.mode;
	vision2_sw_release = tag->u.vision2_deviceinfo.sw_release;
	vision2_hw_revision = tag->u.vision2_deviceinfo.hw_revision;
	strncpy(vision2_deviceserial, tag->u.vision2_deviceinfo.serial_device, 32);
	strncpy(vision2_boardserial, tag->u.vision2_deviceinfo.serial_board, 32);

        return 0;
}

__tagtable(ATAG_VISION2_DEVICEINFO, parse_tag_vision2_deviceinfo);

#ifdef CONFIG_FB_KEEP_BOOTSPLASH
static int __init parse_tag_vision2_bootsplash(const struct tag *tag)
{
	printk("Bootsplash framebuffer@%X, length=%X\n"
              , tag->u.vision2_bootsplash.start
              , tag->u.vision2_bootsplash.length);

	vision2_bootsplash_info.start  = tag->u.vision2_bootsplash.start;
	vision2_bootsplash_info.length = tag->u.vision2_bootsplash.length;
        return 0;
}

__tagtable(ATAG_VISION2_BOOTSPLASH, parse_tag_vision2_bootsplash);
#endif

/*
 * The following uses standard kernel macros define in arch.h in order to
 * initialize __mach_desc_MX51_VISION2 data structure.
 */
/* *INDENT-OFF* */
MACHINE_START(TTC_VISION2, "TTControl Vision2")
	/* Maintainer: TTControl GmbH. */
	.fixup = fixup_mxc_board,
	.map_io = mx5_map_io,
	.init_irq = mx5_init_irq,
	.init_machine = mxc_board_init,
	.timer = &mxc_timer,
MACHINE_END
